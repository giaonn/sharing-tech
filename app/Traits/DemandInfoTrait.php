<?php
/**
 * Created by PhpStorm.
 * User: dungphamv
 * Date: 6/7/2018
 * Time: 1:05 PM
 */

namespace App\Traits;

use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\DemandAttachedFileRepositoryInterface;
use App\Repositories\AuctionInfoRepositoryInterface;
use App\Repositories\AutoCommissionCorpRepositoryInterface;
use App\Repositories\DemandCorrespondsRepositoryInterface;
use App\Repositories\MAnswerRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Repositories\MSiteCategoryRepositoryInterface;
use App\Repositories\MSiteGenresRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use App\Models\MCorp;

trait DemandInfoTrait
{
    /**
     * @var DemandCorrespondsRepositoryInterface
     */
    protected $demandCorrespondsRepository;
    /**
     * @var MUserRepositoryInterface
     */
    protected $mUserRepository;
    /**
     * @var AuctionInfoRepositoryInterface
     */
    protected $auctionInfoRepo;
    /**
     * @var MSiteService
     */
    protected $mSiteService;
    /**
     * @var MPostService
     */
    protected $mPostService;
    /**
     * @var AuctionService
     */
    protected $auctionService;
    /**
     * @var CommissionService
     */
    protected $commissionService;
    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSite;
    /**
     * @var MItemRepositoryInterface
     */
    protected $mItemRepository;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepo;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCateRepo;

    /**
     * @var mixed
     */
    protected $mCorp;
    /**
     * @var mixed
     */
    protected $demandInfoRepo;
    /**
     * @var mixed
     */
    protected $demandAttachedFileRepo;
    /**
     * @var mixed
     */
    protected $visitTimeRepo;
    /**
     * @var mixed
     */
    protected $autoCommissionCorp;
    /**
     * @var mixed
     */
    protected $mAnswerRepository;
    /**
     * @var mixed
     */
    protected $mSiteGenresRepository;
    /**
     * @var mixed
     */
    protected $mSiteCategoryRepo;
    /**
     * @var mixed
     */
    protected $mGenresRepository;

    /**
     * @des get Error
     * @return array
     */
    public function getErrors()
    {
        $errs = [];
        $sessionErrors = is_array(session('errors')) ? session('errors') : [session('errors')];
        $errs = session('errors') ? array_merge($errs, $sessionErrors) : $errs;
        $errs = session('demand_errors') ? array_merge($errs, session('demand_errors')) : $errs;
        $errs['error_msg_input'] = session('error_msg_input') ? session('error_msg_input') : null;

        return $errs;
    }

    /**
     * @des init repository
     */
    private function initRepository()
    {
        $this->mCorpRepo = app()->make(MCorpRepositoryInterface::class);
        $this->demandInfoRepo = app()->make(DemandInfoRepositoryInterface::class);
        $this->demandAttachedFileRepo = app()->make(DemandAttachedFileRepositoryInterface::class);
        $this->visitTimeRepo = app()->make(VisitTimeRepositoryInterface::class);
        $this->autoCommissionCorp = app()->make(AutoCommissionCorpRepositoryInterface::class);
        $this->mSiteGenresRepository = app()->make(MSiteGenresRepositoryInterface::class);
        $this->mAnswerRepository = app()->make(MAnswerRepositoryInterface::class);
        $this->mCorp = new MCorp();
        $this->mSiteCategoryRepo = app()->make(MSiteCategoryRepositoryInterface::class);
        $this->mGenresRepository = app()->make(MGenresRepositoryInterface::class);
        $this->demandCorrespondsRepository = app()->make(DemandCorrespondsRepositoryInterface::class);
        $this->mUserRepository = app()->make(MUserRepositoryInterface::class);
        $this->auctionInfoRepo = app()->make(AuctionInfoRepositoryInterface::class);
        $this->mSite = app()->make(MSiteRepositoryInterface::class);
        $this->mItemRepository = app()->make(MItemRepositoryInterface::class);
        $this->mCateRepo = app()->make(MCategoryRepositoryInterface::class);
    }

    /**
     * @param $data
     * @param $errFlg
     * @return bool
     */
    public function validateAndDebug($data, &$errFlg)
    {
        /* validate demand info */
        if (!$this->demandInfoService->validateDemandInfo($data)) {
            \Log::debug('validateDemandInfo');
            $errFlg = true;
        }
        /* validate visit time */
        if ($this->demandInfoService->validateDemandInquiryAnswer($data)
            || !$this->visitTimeService->processValidateVisitTime($data)
        ) {
            \Log::debug('validateDemandInquiryAnswer, processValidateVisitTime');
            $errFlg = true;
        }
        if ($this->demandInfoService->checkErrSendCommissionInfo($data)) {
            \Log::debug('checkErrSendCommissionInfo');
            $errFlg = true;
        }
        /* validate commission info */
        if (isset($data['commissionInfo'])) {
            if (!$this->commissionService->validate($data['commissionInfo'], $data['demandInfo']['demand_status'])) {
                \Log::debug('commissionService->validate');
                $errFlg = true;
            }
            if (!$this->commissionService->validateCheckLimitCommitFlg($data)) {
                \Log::debug('commissionService->validateCheckLimitCommitFlg');
                session()->flash('error_msg_input', __('demand.commitFlgLimit'));
                session()->flash('error_commit_flg', __('demand.commitFlgLimit'));
                $errFlg = true;
            }
        }
        /* validate demand correspond */
        if (isset($data['demandCorrespond']) && $data['send_commission_info'] == 0) {
            if (!$this->demandCorrespondService->validate($data['demandCorrespond'])) {
                \Log::debug('demandCorrespondService->validate');
                $errFlg = true;
            }
        }

        return $errFlg;
    }

    /**
     * @param $data
     * @param $errFlg
     */
    public function validateAndBackToDetail($data, &$errFlg)
    {
        /*validate commission type div*/
        $cTypeDiv = $data['demandInfo']['commission_type_div'];
        $commissionExits = array_key_exists('commissionInfo', $data);
        $demandStatus = $data['demandInfo']['demand_status'];
        if (!$this->demandInfoService->validateCommissionTypeDiv($cTypeDiv, $commissionExits, $demandStatus)) {
            session()->flash('error_msg_input', __('demand.notEmptyIntroduceInfo'));
            \Log::debug('validateCommissionTypeDiv');
            return $this->backToDetail($data);
        }

        if (!$this->demandInfoService->checkModifiedDemand($data['demandInfo'])) {
            session()->flash('error_msg_input', __('demand.modifiedNotCheck'));
            \Log::debug('checkModifiedDemand');
            return $this->backToDetail($data);
        }

        if (!$this->demandInfoService->validateSelectSystemType($data)) {
            \Log::debug('validateSelectSystemType');
            return $this->backToDetail($data);
        }

        return $errFlg;
    }

    /**
     * @param $hasStartTimeErr
     * @param $auctionNoneFlg
     * @param $auctionFlg
     */
    public function setFlashError($hasStartTimeErr, $auctionNoneFlg, $auctionFlg)
    {
        if ($hasStartTimeErr) {
            session()->flash('error_msg_input', __('demand.start_time_error'));
        } elseif (!$auctionNoneFlg) {
            session()->flash('error_msg_input', __('demand.aff_nothing'));
        } elseif (!$auctionFlg) {
            session()->flash('error_msg_input', __('demand.auction_ng_update'));
        }
        return;
    }

    /**
     * @param $data
     */
    public function sendNotify($data)
    {
        $demandId = $data['demandInfo']['id'];
        if (!$data['demandInfo']['id']) {
            $demandId = $this->demandInfoRepo->getMaxIdInsert();
        }
        if (isset($data['send_commission_info']) && $data['send_commission_info'] == 1) {
            $this->commissionService->sendNotify($demandId);
        }
    }

    /**
     * @des transaction for update
     * @param $data
     * @return mixed
     */
    public function updateData(&$data)
    {
        $data['demandInfo'] = $this->demandInfoService->updateDemand($data['demandInfo']);
        $this->demandInquiryAnswerService->updateDemandInquiryAnswer($data);
        $this->visitTimeService->updateVisitTime($data);
        $errorNo = $this->commissionService->updateCommission($data);
        $this->commissionService->updateIntroduce($data);
        $this->auctionService->updateAuctionInfos($data['demandInfo']['id'], $data);
        $this->demandCorrespondService->updateDemandCorrespond($data);

        return $errorNo;
    }

    /**
     * @param $data
     * @param $commissionInfo
     * @param $sendCommissionInfo
     * @param $errFlg
     * @return bool
     */
    public function checkCommissionFlgCount($data, $commissionInfo, $sendCommissionInfo, &$errFlg)
    {
        if (!empty($commissionInfo) && $sendCommissionInfo == 1) {
            if (!$this->commissionService->checkCommissionFlgCount($data)) {
                \Log::debug('checkCommissionFlgCount');
                session()->flash('error_commission_flg_count', __('demand.check_input_confirmation'));
                $errFlg = true;
            }
        }
        return $errFlg;
    }

    /**
     * @param $data
     */
    public function updateCommissionSendMailFax($data)
    {
        if ($data['send_commission_info'] == 1) {
            $this->commissionService->updateCommissionSendMailFax($data);
        }

        return;
    }

    /**
     * @param $id
     * @return \App\Models\Base|null
     */
    public function findDemandCorrespond($id)
    {
        return $this->demandCorrespondsRepository->find($id);
    }
}
