<?php

namespace App\Validators;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Models\MCorp;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        $this->app['validator']->extend(
            'globalrule',
            function ($attribute, $value, $parameters) {
                //TODO: implement global rule
                return true;
            }
        );

        Validator::extend(
            'AffiliationAddValidateMailRequired',
            function ($attribute, $value, $parameters, $validator) {
                $coordinationMethod = [
                MCorp::METHOD_NUM_1,
                MCorp::METHOD_NUM_2,
                MCorp::METHOD_NUM_6,
                MCorp::METHOD_NUM_7,
                ];
                if (in_array($parameters[1], $coordinationMethod) && !$parameters[0] && !$value) {
                    return false;
                }
                return true;
            }
        );
        Validator::extend(
            'AffiliationAddValidateMailFormat',
            function ($attribute, $value, $parameters, $validator) {
                if ($value) {
                    return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
                }
                return true;
            }
        );
    }

    /**
     * @return void
     */
    public function register()
    {
        //
    }
}
