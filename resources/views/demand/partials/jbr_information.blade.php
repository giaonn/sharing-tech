<div class="form-category mb-4" id="jbrdemandinfo">
    @include('demand.create.anchor_top')
    <label class="form-category__label">@lang('demand_detail.proposal_information')</label>
    <div class="form-category__body clearfix">
        <div class="form-table mb-4">
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.reception_no')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::text('demandInfo[jbr_order_no]', $demand->jbr_order_no, ['class' => 'form-control w-100']) !!}

                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.work_content_jbr')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                            {!! Form::select('demandInfo[jbr_work_contents]', $jbrWorkContentDropDownList, $demand->jbr_work_contents, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.content_of_the_email')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::textarea('demandInfo[mail]', $demand->mail, ['class' => 'form-control', 'rows' => 5]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
