<div class="row table-result-search">
    <span class="count-total">@lang('mcorp_list.total') {{ !empty($results) ? $results->total() : 0 }}@lang('mcorp_list.matter')</span>
    <div class="table-responsive">
        <table class="table table-bordered" id="tbBillSearch">
            <thead>
            <tr>
                <th class="text-center w-15">@lang('mcorp_list.id_link')</th>
                <th class="text-center w-85">@lang('mcorp_list.link')</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($results))
                @foreach($results as $result)
                <tr>
                    <td class="text-center w-15">{{ $result['id'] }}</td>
                    <td class="text-center w-85">
                        <a target="_blank" href="{{route('bill.getBillList',$result['id'])}}">{{ $result['official_corp_name'] }}</a>
                    </td>
                </tr>
            @endforeach
            @endif
            </tbody>
        </table>
        @if(!empty($results))
            {{ $results->links('bill.component.mcorp_pagination') }}
        @endif
    </div>
</div>
