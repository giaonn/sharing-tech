<div class="form-table mb-4 commission-table">
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 bg-primary-lighter p-0">
            <div class="col-12 col-lg-3 px-0">
                <div class="form__label form__label--primary p-3 h-100 border-bottom">
                    <label class="m-0">

                        <strong>@lang('demand_detail.supplier')<span class="max-index">{{ $key + 1 }}</span></strong>
                    </label>
                    <button data-url_data="{{ route('ajax.demand.load_m_corp', [$commissionInfo['corp_id']]) }}"
                            data-toggle="modal" type="button"
                            class="btn btn-sm btn--gradient-default m-corps-detail">
                        @lang('demand_detail.information_reference')
                    </button>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-3">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text('commissionInfo['. $key .'][mCorp][corp_name]', $commissionInfo['mCorp']['corp_name'], ['class' => 'form-control', 'id' => 'corp_name' . $key]) !!}
                </div>
            </div>
            {{--<div class="col-12 col-lg-3 py-3 text-right"><a target='_blank' class="text--orange" href="{{ route('commission.detail', ['id' => $commissionInfo->id ?? '']) }}">詳細</a></div>--}}
        </div>

    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.procedure-dial')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">

                <div class="form-group h-100 d-flex justify-content-start align-items-center mb-lg-">
                    @if($commissionInfo['mCorp']['commission_dial'])
                        <a href="callto:{{ $commissionInfo['mCorp']['commission_dial'] }}" class="text--orange ml-2 w-50 text--underline">
                            {{ $commissionInfo['mCorp']['commission_dial'] }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for="del_flg{{ $key}}">
                        <strong> @lang('demand_detail.procedure-dial')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][del_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][del_flg]', 1, false, ['class' => 'custom-control-input', 'id' => 'del_flg'.$key]) !!}
                    <label class="custom-control-label" for='commissionInfo[del_flg]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.appointers')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                    {!! Form::select('commissionInfo['. $key .'][appointers]', $userDropDownList, $commissionInfo['appointers'], ['class' => 'form-control']) !!}

                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='first_commission{{ $key }}'>
                        <strong>@lang('demand_detail.initial-check')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][first_commission]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][first_commission]', 1, false, ['class' => 'custom-control-input', 'id' => 'first_commission' . $key]) !!}
                    <label class="custom-control-label" for='commissionInfo[first_commission]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.mail-order-sender')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around  mb-lg-0">
                    {!! Form::select('commissionInfo['. $key .'][commission_note_sender]', $userDropDownList, $commissionInfo['commission_note_sender'], ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='unit_price_calc_exclude{{ $key }}'>
                        <strong>@lang('demand_detail.not-covered-by-contract-price')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    {!! Form::hidden('commissionInfo[' . $key . '][corp_id]', $commissionInfo['corp_id'], ['class' => 'corp_id']) !!}
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][unit_price_calc_exclude]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][unit_price_calc_exclude]', 1, false, ['class' => 'custom-control-input', 'id' => 'unit_price_calc_exclude' . $key]) !!}
                    <label class="custom-control-label" for='commissionInfo[unit_price_calc_exclude]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.date-time-of-agency-sent')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">

                    {!! Form::text('commissionInfo['. $key .'][commission_note_send_datetime]', $commissionInfo['commission_note_send_datetime_format'] ?? '', ['class' => 'form-control datetimepicker']) !!}
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='commit_flg{{ $key }}'>
                        <strong>@lang('demand_detail.confirm')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="d-inline-block align-middle custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][commit_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][commit_flg]', 1, false, ['class' => 'custom-control-input chk-commit-flg']) !!}
                    <label class="custom-control-label" for='commissionInfo["commit_flg]'></label>
                </div>

            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.no_mail')@lang('demand_detail.fax-transmission')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex  mb-lg-0">
                    <p>
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax]', $commissionInfo['send_mail_fax']) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax_datetime]', $commissionInfo['send_mail_fax_datetime']) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax_sender]', $commissionInfo['send_mail_fax_sender']) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax_othersend]', $commissionInfo['send_mail_fax_othersend']) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][demand_id]', $demand['id'] ?? '') !!}
                        @if($commissionInfo['send_mail_fax'] == 1)
                            @lang('demand_detail.送信済み')　{{$commissionInfo['send_mail_fax_datetime']}}
                            {{ $userDropDownList[$commissionInfo['send_mail_fax_sender']] }}
                        @endif
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='lost_flg{{ $key }}'>
                        <strong>@lang('demand_detail.prior-to-ordering')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][lost_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][lost_flg]', 1, false, ['class' => 'custom-control-input', 'id' => 'lost_flg' . $key]) !!}
                    <label class="custom-control-label" for='commissionInfor["lost_flg]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        {!! Form::hidden('commissionInfo['. $key .'][order_fee_unit]', $commissionInfo['order_fee_unit'] ?? null) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][complete_date]', $commissionIn['complete_date'] ?? '') !!}
                        {!! Form::hidden('commissionInfo['. $key .'][commission_status]', $commissionInfo['commission_status'] ?? '') !!}
                        {!! Form::hidden('commissionInfo['. $key .'][commission_string]', $commissionInfo['commission_string'] ?? '') !!}
                        <strong>{{ $commissionInfo['commission_string'] ?? '' }} </strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex mb-lg-0">
                    {!! Form::hidden('commissionInfo['. $key .'][commission_fee_rate]', $commissionInfo['commission_fee_rate'] ?? '') !!}
                    {!! Form::hidden('commissionInfo['. $key .'][commission_type]', $commissionInfo['commission_type']) !!}
                    {!! Form::hidden('commissionInfo['. $key .'][corp_commission_type_disp]', $commissionInfo['corp_commission_type_disp'] ?? '') !!}
                    {!! Form::hidden('commissionInfo['. $key .'][id]', $commissionInfo['id'] ?? '') !!}
                    <p class="get-fee-data">{{ $commissionInfo['corp_commission_type_disp'] ?? '' }} {{ $commissionInfo['commission_fee_dis'] ?? '' }}</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='corp_claim_flg{{ $key }}'>
                        <strong>@lang('demand_detail.merchant-claim')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][corp_claim_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][corp_claim_flg]', 1, false, ['class' => 'custom-control-input', 'id' => 'corp_claim_flg'. $key]) !!}
                    <label class="custom-control-label" for='commissionInfor["corp_claim_flg]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.unit-price-rank') </strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex  mb-lg-0">
                    {!! Form::hidden('commissionInfo['. $key .'][select_commission_unit_price_rank]', $commissionInfo['select_commission_unit_price_rank']) !!}
                    <p>{{ $commissionInfo['select_commission_unit_price_rank'] ?? '' }}@lang('demand_detail.unit-price-per-contract')   {{ yenFormat2($commissionInfo['select_commission_unit_price'] ?? '') }}</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong></strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">

            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.notes')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                {!! nl2br($commissionInfo['attention'] ?? '')  !!}
            </div>
        </div>

    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.long-term-holidays-situation')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        @foreach($commissionInfo['mCorpNewYear'] as $k => $label)
                            @if(strpos($k, 'label') !== false)
                                {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][' . $k . ']', $label) !!}
                                <th>{{ $label }}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        @foreach($commissionInfo['mCorpNewYear'] as $index => $status)
                            @if(strpos($index, 'status') !== false)
                                <td>
                                    {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][' . $index . ']', $status) !!}

                                    {{ $status }}
                                </td>
                            @endif

                        @endforeach
                    </tr>

                    <tr>
                        <td>@lang('demand_detail.remarks')</td>
                        <td colspan="{{ count($commissionInfo['mCorpNewYear']) }}">

                            {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][note]', $commissionInfo['mCorpNewYear']['note'] ?? '') !!}
                            {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][long_vacation_note]', $commissionInfo['mCorpNewYear']['long_vacation_note'] ?? '') !!}
                            {{ $commissionInfo['mCorpNewYear']['note'] ?? ''}}

                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
    @if(isset($commissionInfo['corp_fee']))
        {!! Form::hidden('commissionInfo['. $key .'][corp_fee]', $commissionInfo['corp_fee'] ?? 0) !!}
    @endif

    {!! Form::hidden('commissionInfo['. $key .'][select_commission_unit_price]', $commissionInfo['select_commission_unit_price'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][introduction_not]', $commissionInfo['introduction_not'] ?? 0) !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][fax]', $commissionInfo['mCorp']['fax'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][mailaddress_pc]', $commissionInfo['mCorp']['mailaddress_pc'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][coordination_method]', $commissionInfo['mCorp']['coordination_method'] ?? 0) !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][contactable_time]', $commissionInfo['mCorp']['contactable_time'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][holiday]', $commissionInfo['mCorp']['holiday'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][commission_dial]', $commissionInfo['mCorp']['commission_dial'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][demand_id]', $commissionInfo['demand_id'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][id]', $commissionInfo['id']) !!}

    {!! Form::hidden('commissionInfo['. $key .'][affiliationInfo ][attention]', isset($commissionInfo['affiliationInfo']) ? $commissionInfo['affiliationInfo']['attention'] : '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][attention]', $commissionInfo['attention'] ?? '') !!}

    {!! Form::hidden('commissionInfo['. $key .'][mCorpCategory][order_fee]', $commissionInfo['mCorpCategory']['order_fee'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorpCategory][order_fee_unit]', $commissionInfo['mCorpCategory']['order_fee_unit'] ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorpCategory][note]', $commissionInfo['mCorpCategory']['note'] ?? '') !!}
</div>

