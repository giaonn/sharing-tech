<?php

namespace App\Http\Controllers\Auth;

use App\Repositories\MUserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\LoginService;
use App\Services\UserService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    /**
     * @var LoginService
     */
    public $loginService;

    /**
     * @var MUserRepositoryInterface
     */
    public $userRepository;

    /**
     * Create a new controller instance.
     *
     * @param LoginService             $loginService
     * @param MUserRepositoryInterface $userRepository
     */
    public function __construct(LoginService $loginService, MUserRepositoryInterface $userRepository)
    {
        $this->loginService = $loginService;
        $this->userRepository = $userRepository;
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    protected function authenticated(Request $request)
    {
        $dataRequest = $request->all();
        if (!$this->loginService->checkGuideline($dataRequest)) {
            $this->guard()->logout();
            return redirect('/login')->withErrors(['GuideLineError' => trans('auth.guide_line_error')]);
        } elseif (UserService::checkRole('affiliation')) {
            return redirect('auction');
        } else {
            $data['last_login_date'] = Carbon::now();
            $this->userRepository->updateLastLogin($request->user_id, $data);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->session()->invalidate();
        $this->guard()->logout();

        return redirect('/');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'user_id';
    }
}
