<?php
$invalidFlowDate = false;
$orderFailDate = old('demandInfo')['order_fail_date'];
$orderFailDateInvalid = false;
$timeInvalid[1] = false;
$timeInvalid[2] = false;
$timeInvalid[3] = false;
if (old('visitTime')) {
    if (strtotime(old('visitTime')[1]['visit_time']) === false ) {}
    if (strtotime(old('visitTime')[2]['visit_time']) === false && !empty(old('visitTime')[2]['visit_time'])) {
        $timeInvalid[2] = true;
    }
    if (strtotime(old('visitTime')[3]['visit_time']) === false && !empty(old('visitTime')[3]['visit_time'])) {
        $timeInvalid[3] = true;
    }
}
if (old('demandInfo')['order_fail_date'] && strtotime(old('demandInfo')['order_fail_date']) === false ) {
    $orderFailDateInvalid = true;
    $orderFailDate = '';
}
if (old('demandInfo')['follow_date'] && strtotime(old('demandInfo')['follow_date']) === false ) {
    $invalidFlowDate = true;
}
?>
<div class="form-table">
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.customer_name')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text("demandInfo[customer_name]", $demand->customer_name, ['class' => 'form-control is-required', 'data-rules' => 'not-empty', 'maxlength' => 50]) !!}
                    @if ($errors->has('demandInfo.customer_name'))
                        <label class="invalid-feedback d-block">{{ $errors->first('demandInfo.customer_name') }}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>  @lang('demand_detail.corporate_name')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text("demandInfo[customer_corp_name]", $demand->customer_corp_name, ['class' => 'form-control', 'id' => 'customer_corp_name']) !!}
                    @if ($errors->has('demandInfo.customer_corp_name'))
                        <label class="invalid-feedback d-block">{{ $errors->first('demandInfo.customer_corp_name') }}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.customer_tel')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-inline-block justify-content-between align-items-center mb-lg-0">
                    @php
                        $tel = '';
                        if(!empty(old('demandInfo')['customer_tel'])) {
                            $tel = old('demandInfo')['customer_tel'];
                        }
                    @endphp
                    {!! Form::text("demandInfo[customer_tel]", $demand->customer_tel, ['class' => 'd-inline-block form-control w-50 is-required', 'data-rules'=> "not-empty,valid-number", 'maxlength' => '11']) !!}
                    @if(!empty(old('demandInfo')['customer_tel']))
                        <a href="callto:{{ $tel }}"  class="text--orange w-50">{{ $tel }}</a>
                    @else
                        <a href="callto:{{ $demand->customer_tel }}" class="text--orange ml-2 w-50">{{ $demand->customer_tel }}</a>
                    @endif

                </div>
                @if (Session::has('demand_errors.check_customer_tel'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_customer_tel')}}</label>
                @elseif ($errors->has('demandInfo.customer_tel'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.customer_tel')}}</label>
                @elseif ($errors->has('check_customer_tel'))
                    <label class="invalid-feedback d-block">
                        {{$errors->first('check_customer_tel')}}
                    </label>
                @endif
                <div id="err-demandInfo_customer_tel"></div>
            </div>

        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.contact_first')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-inline-block justify-content-between align-items-center mb-lg-0">
                    {!! Form::text("demandInfo[tel1]", $demand->tel1, ['class' => 'd-inline-block form-control w-50 is-required', 'data-rules' => 'not-empty,valid-number', 'maxlength' => 11]) !!}
                    <a href="callto:{{ old('demandInfo')['tel1'] ?? $demand->tel1 }}" class="text--orange ml-2 w-50">{{ old('demandInfo')['tel1'] ?? $demand->tel1 }}</a>
                </div>
                @if (Session::has('demand_errors.check_tel1'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_tel1')}}</label>
                @elseif ($errors->has('demandInfo.tel1'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.tel1')}}</label>
                @endif
                <div id="err-demandInfo_tel"></div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.contact_second')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                    {!! Form::text("demandInfo[tel2]", $demand->tel2, ['class' => 'form-control w-50']) !!}
                    @if(!empty(old('demandInfo')['tel2']))
                        <a href="callto:{{ old('demandInfo')['tel2'] }}" class="text--orange">{{ old('demandInfo')['tel2'] }}</a>
                    @endif
                    @if ($errors->has('demandInfo.tel2'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.tel2')}}</label>
                    @endif
                    <a href="callto:{{ $demand->tel2 }}" class="text--orange ml-2 w-50">{{ $demand->tel2 }}</a>
                </div>

            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.customer_email_address')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between flex-column align-items-center mb-lg-0">
                    {!! Form::text("demandInfo[customer_mailaddress]", $demand->customer_mailaddress, ['class' => 'form-control is-required', 'data-rules' => 'valid-email']) !!}
                    <br/>
                    @if ($errors->has('demandInfo.customer_mailaddress'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.customer_mailaddress')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.postal_code')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-group d-flex flex-column flex-lg-row  align-items-lg-center mb-lg-0">
                    {!! Form::text("demandInfo[postcode]", $demand->postcode, ['class' => 'form-control w-auto mb-2 mb-lg-0', 'id' => 'postcode', 'size' => 7, 'maxlength' => 7]) !!}
                    <button class="btn btn--gradient-default ml-lg-2 mb-2 mb-lg-0" id="search-address-by-zip">@lang('demand_detail.address_search')</button>
                    <p class="text-muted mb-2 mb-lg-0 ml-lg-2">@lang('demand_detail.entering_postal_codes')@lang('demand_detail.hyphen')</p>
                </div>
                <div id="err-postcode">
                    @if ($errors->has('demandInfo.postcode'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.postcode')}}</label>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.prefectures')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around flex-column align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[address1]', $prefectureDiv, $demand->address1, ['class' => 'form-control is-required', 'id' => 'address1', 'data-rules' => 'not-empty', 'maxlength' => 10]) !!}
                    {{--                    {!! Form::select('demandInfo[address1]', $prefectureDiv, '', ['class' => 'form-control', 'id' => 'address1']) !!}--}}
                    @if ($errors->has('demandInfo.address1'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.address1')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.municipality')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text('demandInfo[address2]', $demand->address2, ['class' => 'form-control is-required', 'id' => 'address2', 'data-rules' => 'not-empty', 'maxlength' => 20]) !!}
                    @if ($errors->has('demandInfo.address2'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.address2')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.later_address')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text('demandInfo[address3]', $demand->address3, ['class' => 'form-control', 'id' => 'address3']) !!}
                    @if ($errors->has('demandInfo.address3'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.address3')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.building_type')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around flex-column align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[construction_class]', $mItemsDropDownList, $demand->construction_class, ['class' => 'form-control is-required', 'data-rules' => 'not-empty']) !!}
                    @if ($errors->has('demandInfo.construction_class'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.construction_class')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.selection_method')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[selection_system]', $selectionSystemList, $demand->selection_system, ['class' => 'form-control', 'id' => 'selection_system']) !!}
                    {!! Form::hidden('demandInfo[selection_system_before]', $demand->selection_system, ['id' => 'selection_system_before']) !!}
                </div>
                @if (Session::has('demand_errors.check_selection_system'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_selection_system')}}</label>
                @elseif (Session::has('demand_errors.check_auction_setting_genre'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_auction_setting_genre')}}</label>
                @elseif ($errors->has('demandInfo.selection_system'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.selection_system')}}</label>
                @endif
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.content_of_consultation')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::textarea('demandInfo[contents]', $demand->getOriginal('contents'), ['class' => 'form-control', 'rows' => 10, 'id' => 'demand-content']) !!}
                    @if (Session::has('demand_errors.check_contents_string'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contents_string')}}</label>
                    @elseif ($errors->has('demandInfo.contents'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contents')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.sharing')
                            <br>
                            @lang('demand_detail.technology_side_notes')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                    <div id="attention" class="clearfix"></div>
                    {{--<textarea class="form-control-plaintext" id="attention" disabled></textarea>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.contact_deadline')</strong>
                    </label>
                    <button class="btn btn--gradient-default d-block" id="plus-15-minus"> @lang('demand_detail.15_minutes')</button>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2 visitTimeDiv">
                <div class="form-row align-items-center mb-2">
                    <div class="custom-control custom-radio mr-2">
                        <input @if($demand->is_contact_time_range_flg == 0 || old('demandInfo.is_contact_time_range_flg') == 0) checked @endif type="radio" name = 'demandInfo[is_contact_time_range_flg]' class="custom-control-input range-time-0 absolute_time" value="0" id="demandInfo[is_contact_time_range_flg_0]" />
                        <label class="custom-control-label"
                               for="demandInfo[is_contact_time_range_flg_0]">@lang('demand_detail.specify_time')</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input @if($demand->is_contact_time_range_flg == 1 || old('demandInfo.is_contact_time_range_flg') == 1) checked @endif class="custom-control-input range-time-1 range_time" id="demandInfo[is_contact_time_range_flg_1]" name="demandInfo[is_contact_time_range_flg]" type="radio" value="1" aria-invalid="false">

                        <label class="custom-control-label"
                               for="demandInfo[is_contact_time_range_flg_1]"> @lang('demand_detail.time_adjustment_required')</label>
                    </div>
                </div>
                <div class="form-row align-items-center mb-2">
                    @php
                        $desertTime = dateTimeFormat($demand->contact_desired_time);
                        $disabled = false;
                        if (old('demandInfo.is_contact_time_range_flg')) {
                            $desertTime = dateTimeFormat($demand->contact_desired_time);
                        }
                        if ($demand->is_contact_time_range_flg == 1 || old('demandInfo.is_contact_time_range_flg') == 1) {
                            $desertTime = '';
                            $disabled = true;
                        }
                    @endphp
                    <div class="col-12 col-lg-3 mb-2">
                        {!! Form::hidden('demandInfo[contact_desired_time]', $desertTime) !!}
                        {!! Form::text('demandInfo[contact_desired_time]', $desertTime,
                        ['class' => 'form-control datetimepicker txt_absolute_time is-required', 'id' => 'contact_desired_time', 'data-rules' => 'valid-date',
                            'disabled' => $disabled
                        ]) !!}
                        {!! Form::hidden('demandInfo[contact_desired_time_before]', '') !!}

                        @if (Session::has('demand_errors.check_contact_desired_time2'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time2')}}</label>
                        @elseif (Session::has('demand_errors.check_contact_desired_time3'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time3')}}</label>
                        @elseif ($errors->has('demandInfo.contact_desired_time'))
                            <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contact_desired_time')}}</label>
                        @endif

                    </div>
                </div>
                <div class="form-row align-items-center mb-2">
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::hidden('demandInfo[contact_desired_time_from]', $demand->contact_desired_time_from_format) !!}
                        {!! Form::text('demandInfo[contact_desired_time_from]', $demand->contact_desired_time_from_format, ['class' => 'form-control datetimepicker txt_range_time',
                         'disabled' => !$disabled]) !!}
                        @if (Session::has('demand_errors.check_contact_desired_time4'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time4')}}</label>
                        @elseif ($errors->has('demandInfo.contact_desired_time_from'))
                            <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contact_desired_time_from')}}</label>
                        @elseif($errors->has('contact_desired_time_from'))
                            <label class="invalid-feedback d-block">{{$errors->first('contact_desired_time_from')}}</label>
                        @endif
                    </div>
                    <span class="col-auto mb-2 text-center">〜</span>
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::hidden('demandInfo[contact_desired_time_to]', $demand->contact_desired_time_to_format) !!}
                        {!! Form::text('demandInfo[contact_desired_time_to]', $demand->contact_desired_time_to_format, ['class' => 'form-control datetimepicker txt_range_time',
                        'disabled' => !$disabled]) !!}
                        @if (Session::has('demand_errors.check_contact_desired_time5'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time5')}}</label>
                        @elseif (Session::has('demand_errors.check_require_to'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_require_to')}}</label>
                        @elseif (Session::has('demand_errors.check_contact_desired_time6'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time6')}}</label>
                        @elseif ($errors->has('demandInfo.contact_desired_time_to'))
                            <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contact_desired_time_to')}}</label>
                        @endif
                    </div>
                </div>
                <p class="text--info">@lang('demand_detail.visit_time_text')</p>
            </div>
        </div>
    </div>

    @php
        $label = [1 => '①', 2 => '②', 3 => '③'];
    @endphp
    @for($i = 1; $i <= 3; $i++)
        @php $j = $i - 1 @endphp
        {!! Form::hidden('visitTime[' . $i . '][visit_time_before]', isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->visit_time_before : '') !!}
        {!! Form::hidden('visitTime[' . $i . '][commit_flg]', isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->commit_flg : '') !!}
        {!! Form::hidden('visitTime[' . $i . '][id]', isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->id : '') !!}
        {!! Form::hidden('visitTime[' . $i . '][visit_time_from]', isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->visit_time_from : '') !!}
        {!! Form::hidden('visitTime[' . $i . '][visit_time_to]', isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->visit_time_to : '') !!}
        {!! Form::hidden('visitTime[' . $i . '][visit_adjust_time]', isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->visit_adjust_time : '') !!}
        <div class="row mx-0 border visit-time-div normal_info" style="display: @if($demand->visitTimes) block @else none @endif;">
            <div class="col-12 row m-0 p-0">
                <div class="col-12 col-lg-3  px-0">
                    <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                        <label class="m-0">
                            <strong>@lang('demand_detail.desired_visit_date'){{ $label[$i] }}</strong>
                        </label>
                    </div>
                </div>
                <div class="col-12 col-lg-9 py-2 visitTimeDiv">
                    <div class="form-row align-items-center mb-2">
                        <div class="custom-control custom-radio mr-2">
                            <input @if((isset(old('visitTime')[$i]) && old('visitTime')[$i]['is_visit_time_range_flg'] == 0)
                            || (!isset($demand->visitTimes[$j])
                            || $demand->visitTimes[$j]->is_visit_time_range_flg == 0)) checked @endif
                            name='visitTime[{{ $i }}][is_visit_time_range_flg]' type="radio" value="0" id="visitTime[{{ $i }}][is_visit_time_range_flg_0]" class="custom-control-input absolute_time" />


                            <label class="custom-control-label"
                                   for="{{'visitTime['. $i .'][is_visit_time_range_flg_0]'}}">@lang('demand_detail.specify_time')</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input
                                    @if((isset(old('visitTime')[$i]) && old('visitTime')[$i]['is_visit_time_range_flg'] == 1) || (isset($demand->visitTimes[$j])) && $demand->visitTimes[$j]->is_visit_time_range_flg == 1))
                                    checked
                                    @endif name='visitTime[{{ $i }}][is_visit_time_range_flg]' type="radio" value="1" id="visitTime[{{ $i }}][is_visit_time_range_flg_1]" class="custom-control-input range_time" />
                            <label class="custom-control-label"
                                   for="{{ 'visitTime[' . $i . '][is_visit_time_range_flg_1]' }}">@lang('demand_detail.time_adjustment_required')</label>
                        </div>
                    </div>
                    @php
                        $disabledVisitTime = false;
                        $disabledRangeTime = false;

                        if(old('visitTime')[$i] && old('visitTime')[$i]['is_visit_time_range_flg'] == 1){
                            $disabledVisitTime = true;
                            $disabledRangeTime = false;
                        }elseif(!isset(old('visitTime')[$i]) && isset($demand->visitTimes[$j]) && $demand->visitTimes[$j]->is_visit_time_range_flg == 1){
                            $disabledVisitTime = true;
                            $disabledRangeTime = false;
                        }elseif(!isset(old('visitTime')[$i]) && !isset($demand->visitTimes[$j])){
                            $disabledVisitTime = false;
                            $disabledRangeTime = true;
                        }elseif(!isset(old('visitTime')[$i]) && isset($demand->visitTimes[$j]) && $demand->visitTimes[$j]->is_visit_time_range_flg == 1){
                            $disabledVisitTime = true;
                            $disabledRangeTime = false;
                        }else{
                            $disabledVisitTime = false;
                            $disabledRangeTime = true;
                        }

                    @endphp
                    <div class="form-row align-items-center mb-2">
                        <div class="col-12 col-lg-3 mb-2">
                            {!! Form::hidden('visitTime[' . $i . '][visit_time]', '') !!}
                            <input class="form-control datetimepicker txt_absolute_time" name="visitTime[{{ $i }}][visit_time]"
                                   type="text"
                                   @if($disabledVisitTime) disabled @endif

                                   value="{{ isset($demand->visitTimes[$j]) ? $demand->visitTimes[$j]->visit_time_format : '' }}" aria-invalid="true" />
                            @if(!empty($timeInvalid[$i]))
                                <label class="invalid-feedback d-block">{{ trans('demand.validation_error.date_error') }}</label>
                            @elseif($errors->first('visitTime.'.$i.'.visit_time'))
                                <label class="invalid-feedback d-block">{{ $errors->first('visitTime.'.$i.'.visit_time') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-row align-items-center mb-2">
                        <div class="col-5 col-lg-3 mb-2">
                            <input class="form-control datetimepicker txt_range_time" name="visitTime[{{ $i }}][visit_time_from]"
                                   type="text"
                                   @if($disabledRangeTime) disabled @endif
                                   value="{{ $disabledRangeTime ? '' : $demand->buildVisittime(old('visitTime'), $j, 'visit_time_from') }}" aria-invalid="true" />

                        </div>

                        <span class="col-auto mb-2 text-center">〜</span>
                        <div class="col-5 col-lg-3 mb-2">
                            <input class="form-control datetimepicker txt_range_time" name="visitTime[{{ $i }}][visit_time_to]"
                                   type="text"
                                   @if($disabledRangeTime) disabled @endif
                                   value="{{ $disabledRangeTime ? '' : $demand->buildVisittime(old('visitTime'), $j, 'visit_time_to') }}" aria-invalid="true" />
                        </div>
                        <span class="col-auto mb-2">@lang('demand_detail.visit_adjustment') </span>
                        <div class="col-12 col-lg-3 mb-2">
                            <input class="form-control datetimepicker txt_range_time" name="visitTime[{{ $i }}][visit_adjust_time]"
                                   type="text"
                                   @if($disabledRangeTime) disabled @endif
                                   value="{{ $demand->buildVisittime(old('visitTime'), $j, 'visit_adjust_time') }}" aria-invalid="true" />
                        </div>


                    </div>
                    <div class="form-row align-items-center mb-2">
                        <div class="col-5 col-lg-3 mb-2">
                            @if($errors->first('visitTime.'.$i.'.visit_time_from'))
                                <label class="invalid-feedback d-block">{{ $errors->first('visitTime.'.$i.'.visit_time_from') }}</label>
                            @elseif(!empty(old('visitTime')[$i]['visit_time_to']) && $errors->first('visitTimeFromRequired'))
                                <label class="invalid-feedback d-block">{{ $errors->first('visitTimeFromRequired') }}</label>
                            @endif
                        </div>
                        <div class="col-auto mb-2 text-center">&nbsp;&nbsp;</div>
                        <div class="col-5 col-lg-3 mb-2">
                            @if(!empty(old('visitTime')[$i]['visit_time_from']) && $errors->first('visitimeToRquired'))
                                <label class="invalid-feedback d-block">{{ $errors->first('visitimeToRquired') }}</label>
                            @elseif($errors->first('visitTime.'.$i.'.visit_time_to'))
                                <label class="invalid-feedback d-block">{{ $errors->first('visitTime.'.$i.'.visit_time_to') }}</label>
                            @endif
                        </div>
                        <div class="col-2">&nbsp;</div>
                        <div class="col-5 col-lg-3 mb-2">
                            @if(!empty(old('visitTime')[$i]['visit_time_from']) && $errors->first('adjustTimeRequire'))
                                <label class="invalid-feedback d-block">{{ $errors->first('adjustTimeRequire') }}</label>
                            @endif
                            @if(!empty(old('visitTime')[$i]['visit_time_from']) && !empty(old('visitTime')[$i]['visit_time_to']) && $errors->first('adjust_time'))
                                <label class="invalid-feedback d-block">{{ $errors->first('adjust_time') }}</label>
                            @endif
                        </div>
                    </div>
                    @if($i != 0) <p class="text--info">@lang('demand_detail.visit_possible_time')@lang('demand_detail.induce_to_complete')</p>@endif
                </div>
            </div>
        </div>
    @endfor


    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.business_trip_cost')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2 visitTimeDiv">
                <div class="form-row align-items-center mb-2">
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::text('demandInfo[business_trip_amount]', $demand->business_trip_amount,
                        ['class' => 'form-control is-required', 'id' => 'business-trip-mount', 'data-rules' => 'valid-number']) !!}
                    </div>
                    <span class="col-auto mb-2 text-center"> @lang('demand_detail.circle') </span>
                    @if ($errors->has('demandInfo.business_trip_amount'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.business_trip_amount')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.service_price_offer')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-row align-items-center mb-2">
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::text('demandInfo[cost_from]', $demand->cost_from, ['class' => 'form-control is-required', 'data-rules' => 'valid-number']) !!}
                        @if ($errors->has('demandInfo.cost_from'))
                            <label class="invalid-feedback d-block">{{$errors->first('demandInfo.cost_from')}}</label>
                        @endif
                    </div>
                    <span class="col-auto mb-2 text-center"> @lang('demand_detail.circle') </span>
                    <span class="col-auto mb-2 text-center"> 〜</span>
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::text('demandInfo[cost_to]', $demand->cost_to, ['class' => 'form-control is-required', 'data-rules' => 'valid-number']) !!}
                        @if ($errors->has('demandInfo.cost_to'))
                            <label class="invalid-feedback d-block">{{$errors->first('demandInfo.cost_to')}}</label>
                        @endif
                    </div>
                    <span class="col-auto mb-2 text-center"> @lang('demand_detail.circle')</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.follow_up_date')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[follow_date]', $demand->follow_date, ['class' => 'form-control datetimepicker']) !!}
                </div>
                @if (Session::has('demand_errors.follow_date'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.follow_date')}}</label>
                @elseif($invalidFlowDate == true)
                    <label class="invalid-feedback d-block">{{ trans('demand.validation_error.date_error') }}</label>
                @endif
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.reception_date_time')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[receive_datetime]', isset($demand->receive_datetime_format) ? $demand->receive_datetime_format : '', ['class' => 'form-control datetimepicker']) !!}
                    @if ($errors->has('demandInfo.receive_datetime'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.receive_datetime')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.priority')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[priority]', $priorityDropDownList, $demand->priority, ['class' => 'form-control', 'disabled' => Auth::user()->priority == 'system']) !!}
                    {!! Form::hidden('demandInfo[priority_before]', $demand->priority_before ?? '') !!}
                </div>
            </div>
        </div>
    </div>


    <div class="row mx-0 border normal_info" style="display: none">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.bid_deadline')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[auction_deadline_time]', isset($demand->auction_deadline_time_format) ? $demand->auction_deadline_time_format : '', ['class' => 'form-control-plaintext ignore', 'readonly']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row mx-0 border normal_info" style="display: none">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.follow_up_date_time')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[follow_tel_date_format]', isset($demand->follow_tel_date_format) ? $demand->follow_tel_date_format : '', ['class' => 'form-control-plaintext ignore', 'readonly']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border normal_info" style="display: none">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.followed')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    {!! Form::checkbox('demandInfo[follow]', 1, $demand->follow == 1, ['class' => 'custom-control-input']) !!}
                    <label class="custom-control-label" for="customControlAutosizing"></label>
                </div>
            </div>
        </div>
    </div>
    @if(isset($demand->auctionInfo) && count($demand->auctionInfo) > 0)
        <div class="row mx-0">
            <div class="col-12 p-20 text-right" style="padding: 20px">
                <button data-url_data="{{ route('demand.auction_detail', $demand->id) }}" class="btn btn--gradient-red" type="button" data-toggle="modal" id="get-auction-detail">@lang('demand_detail.bidding_situation')</button>
            </div>

        </div>
    @endif
</div>
