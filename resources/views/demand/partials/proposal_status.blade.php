<div class="form-category mb-4" id="demandstatus">
    @include('demand.create.anchor_top')
    <label class="form-category__label">@lang('demand_detail.proposal_status')</label>
    <div class="form-category__body clearfix">
        <div class="form-table mb-4">
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.proposal_status')</strong>
                            </label>
                            <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>

                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::select('demandInfo[demand_status]', $demandStatusDropDownList, $demand->demand_status, ['class' => 'form-control', 'id' => 'demand_status', 'data-rule-required' => 'true']) !!}
                        @if (Session::has('demand_errors.check_demand_status'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status')}}</label>
                        @elseif (Session::has('demand_errors.check_demand_status_advance'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status_advance')}}</label>
                        @elseif (Session::has('demand_errors.check_demand_status_introduce'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status_introduce')}}</label>
                        @elseif (Session::has('demand_errors.check_demand_status_introduce_email'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status_introduce_email')}}</label>
                        @elseif (Session::has('demand_errors.check_demand_status_selection_type'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status_selection_type')}}</label>
                        @elseif (Session::has('demand_errors.check_demand_status_confirm'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status_confirm')}}</label>
                        @elseif (Session::has('demand_errors.check_demand_status_introduce_email2'))
                            <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_demand_status_introduce_email2')}}</label>
                        @elseif ($errors->has('demandInfo.demand_status'))
                            <label class="invalid-feedback d-block">{{$errors->first('demandInfo.demand_status')}}</label>
                        @elseif ($errors->has('check_demand_status_confirm'))
                            <label class="invalid-feedback d-block">{{$errors->first('check_demand_status_confirm')}}</label>
                        @endif
                    </div>

                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.lost_date')</strong>
                            </label>

                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::text('demandInfo[order_fail_date]', $demand->order_fail_date, ['class' => 'form-control datepicker order-fail-date']) !!}
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.reason_for_losing')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::select('demandInfo[order_fail_reason]', $orderFailReasonDropDownList, $demand->order_fail_reason,
                        ['class' => 'form-control', 'disabled' => old('demandInfo')['demand_status'] && old('demandInfo')['demand_status'] == 6 ? false : true, 'id' => 'order-fail-reason']) !!}
                        @if(session('demand_errors.check_order_fail_reason'))
                            <label class="invalid-feedback d-block">{{ session('demand_errors.check_order_fail_reason') }}</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.reception_status')</strong>
                            </label>
                            <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>

                        </div>
                    </div>
                    <div class="col-9 col-lg-3 py-2">
                        {!! Form::select('demandInfo[acceptance_status]', $acceptanceStatusDropDownList, $demand->acceptance_status, ['class' => 'form-control is-required', 'data-rules' => 'not-empty']) !!}
                    </div>
                    <div class="col-3 py-2">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            {!! Form::checkbox('demandInfo[nitoryu_flg]', 1, $demand->nitoryu_flg == 1, ['class' => 'custom-control-input']) !!}
                            <label class="custom-control-label" for='demandInfo[nitoryu_flg]'>@lang('demand_detail.double_sword')</label>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
