<?php

namespace App\Repositories;

interface AdditionInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param array $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param string $fields
     * @param string $orderBy
     * @param array $conditions
     * @return mixed
     */
    public function getReportAdditionList($fields, $orderBy, $conditions);
}
