<div class="form-table">
    <div class="row mx-0 border">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 py-2">
                <div class="form-row align-items-center">
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[nighttime_takeover]', 1, $demand->nighttime_takeover == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[nighttime_takeover]']) !!}
                        <label class="custom-control-label"
                               for='demandInfo[nighttime_takeover]'>@lang('demand_detail.night_work')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[mail_demand]', 1, $demand->mail_demand == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[mail_demand]']) !!}
                        <label class="custom-control-label"
                               for='demandInfo[mail_demand]'>@lang('demand_detail.mail_demand')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[cross_sell_implement]', 1, isset($demand->cross_sell_implement) && $demand->cross_sell_implement == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[cross_sell_implement]']) !!}
                        <label class="custom-control-label" for='demandInfo[cross_sell_implement]'>@lang('demand_detail.cross_cell_acquisition')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[cross_sell_call]', 1, isset($demand->cross_sell_call) && $demand->cross_sell_call == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[cross_sell_call]']) !!}
                        <label class="custom-control-label" for='demandInfo[cross_sell_call]'>@lang('demand_detail.cross_sell_call')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">

                        {!! Form::checkbox('demandInfo[riro_kureka]', 1, isset($demand->riro_kureka) && $demand->riro_kureka == 1, ['class' => 'custom-control-input', 'disabled' => true, 'id' => 'demandInfo[riro_kureka]']) !!}
                        <label class="custom-control-label"
                               for='demandInfo[riro_kureka]'>@lang('demand_detail.cleka_case')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[remand]', 1, isset($demand->remand) && $demand->remand == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[remand]']) !!}
                        <label class="custom-control-label"
                               for='demandInfo[remand]'>@lang('demand_detail.reversed_case')</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 py-2">
                <div class="form-row align-items-center">
                    <div class="custom-control custom-checkbox mr-sm-4">

                        {!! Form::checkbox('demandInfo[auction]', 1, isset($demand->auction) && $demand->auction == 1, ['class' => 'custom-control-input', 'disabled' => true]) !!}
                        <label class="custom-control-label"
                               for='demandInfo[auction]'>@lang('demand_detail.bidding_flow_case')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[do_auction]', 2, isset($demand->do_auction) && $demand->do_auction == 2, ['disabled' => false, 'class' => 'custom-control-input', 'id' => 'demandInfo[do_auction]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[do_auction]">@lang('demand_detail.re_bidding')</label>
                    </div>
                    @if($errors->has('check_do_auction'))
                        <label class="invalid-feedback d-block">{{ $errors->first('check_do_auction') }}</label>
                    @endif
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[low_accuracy]', 1, $demand->low_accuracy == 1, ['class'=> 'custom-control-input', 'id' => 'demandInfo[low_accuracy]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[low_accuracy]">@lang('demand_detail.low_accuracy_projects')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[corp_change]', 1, $demand->corp_change == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[corp_change]']) !!}
                        <label class="custom-control-label" for="demandInfo[corp_change]">@lang('demand_detail.change_of_merchant_store_request')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[sms_reorder]', 1, $demand->sms_reorder == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[sms_reorder]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[sms_reorder]">@lang('demand_detail.re_order_sms')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[calendar_flg]', 1, $demand->calendar_flg == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[calendar_flg]']) !!}
                        <label class="custom-control-label" for="demandInfo[calendar_flg]">@lang('demand_detail.calendar_flg')
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.special_measures')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[special_measures]', $specialMeasureDropDownList, $demand->special_measures, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.guide_pet_tombstones')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-group d-flex flex-column flex-lg-row  align-items-lg-center mb-lg-0 group-radio-pet-tomb">
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[pet_tombstone_demand]', 1, $demand->pet_tombstone_demand == 1, ['class' => 'custom-control-input', 'id' => 'demandInfo[pet_tombstone_demand_1]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[pet_tombstone_demand_1]">@lang('demand_detail.guided')</label>
                    </div>
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[pet_tombstone_demand]', 2, $demand->pet_tombstone_demand == 2, ['class' => 'custom-control-input', 'id' => 'demandInfo[pet_tombstone_demand_2]']) !!}

                        <label class="custom-control-label"
                               for="demandInfo[pet_tombstone_demand_2]">@lang('demand_detail.not_covered')</label>
                    </div>
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[pet_tombstone_demand]', 3, $demand->pet_tombstone_demand == 3, ['class' => 'custom-control-input', 'id' => 'demandInfo[pet_tombstone_demand_3]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[pet_tombstone_demand_3]">@lang('demand_detail.forget_the_guide')</label>
                    </div>
                    <button class="btn btn--gradient-default btn--w-normal ml-lg-2 mb-2 mb-lg-0" id="reset-radio">@lang('demand_detail.release')</button>
                    <p class="text-muted mb-2 mb-lg-0 ml-lg-2"> @lang('demand_detail.genre_is_pet')@lang('demand_detail.essential_items')</p>
                </div>
                <div>
                    @if(session('demand_errors.pet_tombstone_demand'))
                        <label class="invalid-feedback d-block">{{ session('demand_errors.pet_tombstone_demand') }}</label>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.order_number')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[order_no_marriage]', $demand->order_no_marriage, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.st_claim')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[st_claim]', $stClaimDropDownList, $demand->st_claim, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
