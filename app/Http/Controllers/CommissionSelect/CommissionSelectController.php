<?php

namespace App\Http\Controllers\CommissionSelect;

use App\Http\Controllers\Controller;
use App\Repositories\AffiliationAreaStatRepositoryInterface;
use App\Repositories\Eloquent\MItemRepository;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Services\Commission\CommissionSelectService;
use Illuminate\Http\Request;

use App\Services\MItemService;

class CommissionSelectController extends Controller
{
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;
    /**
     * @var AffiliationAreaStatRepositoryInterface
     */
    protected $affiliationAreaRepo;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepository;
    /**
     * @var MPostRepositoryInterface
     */
    protected $mPostRepository;
    /**
     * @var MItemRepositoryInterface
     */
    protected $mItemRepository;
    /**
     * @var MItemService
     */
    protected $mItemService;
    /**
     * @var CommissionSelectService
     */
    protected $commissionSelectService;

    /**
     * CommissionSelectController constructor.
     *
     * @param AffiliationAreaStatRepositoryInterface           $affiliationAreaRepo
     * @param MCategoryRepositoryInterface                     $mCategoryRepository
     * @param MCorpRepositoryInterface                         $mCorpRepository
     * @param MItemRepositoryInterface                         $mItemRepository
     * @param MPostRepositoryInterface                         $mPostRepository
     * @param MItemService                                     $mItemService
     * @param \App\Services\Commission\CommissionSelectService $commissionSelectService
     */
    public function __construct(
        AffiliationAreaStatRepositoryInterface $affiliationAreaRepo,
        MCategoryRepositoryInterface $mCategoryRepository,
        MCorpRepositoryInterface $mCorpRepository,
        MItemRepositoryInterface $mItemRepository,
        MPostRepositoryInterface $mPostRepository,
        MItemService $mItemService,
        CommissionSelectService $commissionSelectService
    ) {
        parent::__construct();
        $this->mCorpRepository = $mCorpRepository;
        $this->affiliationAreaRepo = $affiliationAreaRepo;
        $this->mCategoryRepository = $mCategoryRepository;
        $this->mPostRepository = $mPostRepository;
        $this->mItemRepository = $mItemRepository;
        $this->mItemService = $mItemService;
        $this->commissionSelectService = $commissionSelectService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getButton()
    {
        return view('commission.getButton');
    }

    /**
     * Display page commission_select/display
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function display(Request $request)
    {
        $data = $request['data'];
        $data = $this->commissionSelectService->validateDataAndAddParams($data);
        /* Get max price form affiliation_area_stat */
        $maxPrice = $this->affiliationAreaRepo->getMaxCommissionUnitPriceCategory($data);
        /* Add params in $data */
        $data['category_name'] = '';
        $data['category_default_fee'] = '';
        $data['category_default_fee_unit'] = '';
        $data['category_default_commission_type'] = '';

        if (!empty($data['category_id'])) {
            $data['category_name'] = $this->mCategoryRepository->getListText($data['category_id']);
            $categoryDefaultFee = $this->mCategoryRepository->getDefaultFee($data['category_id']);
            $data['category_default_fee'] = $categoryDefaultFee['category_default_fee'];
            $data['category_default_fee_unit'] = $categoryDefaultFee['category_default_fee_unit'];
            $data['category_default_commission_type'] = $this->mCategoryRepository->getCommissionType($data['category_id']);
        }

        $vacation = $this->mItemService->prepareDataList($this->mItemRepository->getListByCategoryItem(MItemRepository::LONG_HOLIDAYS));
        $corpList = $this->mCorpRepository->searchCorpForPopup($data, false, 0);

        if (!is_array($corpList)) {
            return view(
                'commission.display',
                ['data' => $data, 'maxPrice' => $maxPrice,
                'list' => [], 'vacation' => $vacation, 'new_list' => [],
                'errorCommission' => trans('commissionselect.noneCategoryOrJis')]
            );
        }

        $count = count($corpList);
        $corpList2 = $this->commissionSelectService->getNewCorpList($corpList);

        $newCorpList = $this->mCorpRepository->searchCorpForPopup($data, true, $count);

        $newCorpList2 = [];
        for ($i = 0; $i < count($newCorpList); $i++) {
            $obj = $newCorpList[$i];
            $obj['commission_unit_price_rank_1'] = 'z';
            $obj['commission_unit_price_rank_2'] = 'z';
            $newCorpList2[] = $obj;
        }

        if ($request->ajax() && !isset($data['view'])) {
            return view(
                'commission.component.commission_select_popup',
                ['data' => $data, 'maxPrice' => $maxPrice,
                'list' => $corpList2, 'vacation' => $vacation, 'new_list' => $newCorpList2,
                'errorCommission' => null ]
            );
        }

        return view(
            'commission.display',
            ['data' => $data, 'maxPrice' => $maxPrice,
            'list' => $corpList2, 'vacation' => $vacation, 'new_list' => $newCorpList2,
            'errorCommission' => null ]
        );
    }

    /**
     * @param Request $request
     * @return string
     */
    public function mCorpDisplay(Request $request)
    {
        return $this->mCorpSearch($request);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function mCorpSearch(Request $request)
    {
        $dataRequest = $request->all();
        $mCorps = $this->mCorpRepository->getListForCommissionSelect($dataRequest);
        return json_encode($mCorps, true);
    }
}
