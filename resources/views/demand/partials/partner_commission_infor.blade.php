<div class="form-table mb-4 commission-table">
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 bg-primary-lighter p-0">
            <div class="col-12 col-lg-3 px-0">
                <div class="form__label form__label--primary p-3 h-100 border-bottom">
                    <label class="m-0">

                        <strong>@lang('demand_detail.supplier')<span class="max-index">{{ $key + 1 }}</span></strong>
                    </label>
                    <button data-url_data="{{ route('ajax.demand.load_m_corp', [$commissionInfor->mCorpAndMCorpNewYear['id'], $demand->category_id]) }}"
                            data-toggle="modal" type="button"
                            class="btn btn-sm btn--gradient-default m-corps-detail">
                            @lang('demand_detail.information_reference')
                    </button>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-3">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text('commissionInfo['. $key .'][mCorp][corp_name]',
                     $commissionInfor->mCorpAndMCorpNewYear['corp_name'], ['class' => 'form-control', 'id' => 'corp_name' . $key]) !!}
                </div>
            </div>
            <div class="col-12 col-lg-3 py-3 text-right"><a target='_blank' class="text--orange" href="{{ route('commission.detail', ['id' => $commissionInfor->id]) }}">詳細</a></div>
        </div>

    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong> @lang('demand_detail.procedure-dial')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">

                <div class="form-group h-100 d-flex justify-content-start align-items-center mb-lg-">
                    @if($commissionInfor->mCorpAndMCorpNewYear['commission_dial'])
                        <a href="callto:{{ $commissionInfor->mCorpAndMCorpNewYear['commission_dial'] }}" class="text--orange ml-2 w-50 text--underline">
                            {{ $commissionInfor->mCorpAndMCorpNewYear['commission_dial'] }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='del_flg{{ $key }}'>
                        <strong> @lang('demand_detail.delete')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    {!! Form::checkbox('commissionInfo['. $key .'][del_flg]', 1, $commissionInfor->del_flg == 1, ['class' => 'custom-control-input', 'id' => 'del_flg'.$key]) !!}
                    <label class="custom-control-label" for='commissionInfo[del_flg]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.appointers')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                    {!! Form::select('commissionInfo['. $key .'][appointers]', $userDropDownList, $commissionInfor->appointers, ['class' => 'form-control']) !!}

                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='first_commission{{ $key }}'>
                        <strong>@lang('demand_detail.initial-check')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][first_commission]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][first_commission]', 1, $commissionInfor->first_commission == 1, ['class' => 'custom-control-input', 'id' => 'first_commission' . $key]) !!}
                    <label class="custom-control-label" for='commissionInfo[first_commission]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.mail-order-sender')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around  mb-lg-0">
                    {!! Form::select('commissionInfo['. $key .'][commission_note_sender]', $userDropDownList, $commissionInfor->commission_note_sender, ['id' => 'commission_note_sender'.$key, 'class' => 'form-control now_date', 'data-url' => route('ajax.get.now.view'), 'data-key' => $key]) !!}
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for='unit_price_calc_exclude{{ $key }}'>
                        <strong>@lang('demand_detail.not-covered-by-contract-price')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    {!! Form::hidden('commissionInfo[' . $key . '][corp_id]', $commissionInfor->corp_id, ['class' => 'corp_id', 'id' => 'CommissionInfo' . $key . 'CorpId']) !!}
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][unit_price_calc_exclude]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][unit_price_calc_exclude]', 1, $commissionInfor->unit_price_calc_exclude == 1, ['class' => 'custom-control-input', 'id' => 'unit_price_calc_exclude'. $key]) !!}
                    <label class="custom-control-label" for='commissionInfo[unit_price_calc_exclude]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.date-time-of-agency-sent')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">

                    {!! Form::text('commissionInfo['. $key .'][commission_note_send_datetime]', $commissionInfor->commission_note_send_datetime_format, ['id' => 'commission_note_send_datetime'.$key,'class' => 'form-control datetimepicker']) !!}
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for="commit_flg{{ $key }}">
                        <strong>@lang('demand_detail.confirm')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="d-inline-block align-middle custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][commit_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][commit_flg]', 1, $commissionInfor->commit_flg == 1, 
                    ['class' => 'commission_commit_flg custom-control-input chk-commit-flg', 'id' => 'commit_flg' . $key, 'data-id' => $key]) !!}
                    <label class="custom-control-label" for='commissionInfo["commit_flg]'></label>
                </div>

            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.no_mail')@lang('demand_detail.fax-transmission')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex  mb-lg-0">
                    <p>
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax]', $commissionInfor->send_mail_fax) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax_datetime]', $commissionInfor->send_mail_fax_datetime) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax_sender]', $commissionInfor->send_mail_fax_sender) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][send_mail_fax_othersend]', $commissionInfor->send_mail_fax_othersend) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][demand_id]', $demand->id) !!}
                        @if($commissionInfor->send_mail_fax == 1)
                        送信済み　{{$commissionInfor->send_mail_fax_datetime}}
                        {{ $userDropDownList[$commissionInfor->send_mail_fax_sender] }}
                        @endif
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for="lost_flg{{ $key }}">
                        <strong>@lang('demand_detail.prior-to-ordering')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][lost_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][lost_flg]', 1, $commissionInfor->lost_flg == 1, ['class' => 'custom-control-input', 'id' => 'lost_flg' . $key]) !!}
                    <label class="custom-control-label" for='commissionInfor["lost_flg]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        {!! Form::hidden('commissionInfo['. $key .'][order_fee_unit]', $commissionInfor->order_fee_unit) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][complete_date]', $commissionInfor->complete_date) !!}
                        {!! Form::hidden('commissionInfo['. $key .'][commission_status]', $commissionInfor->commission_status) !!}
                        <strong>{{ $commissionInfor->order_fee_unit_label }} </strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex mb-lg-0">
                    {!! Form::hidden('commissionInfo['. $key .'][commission_fee_rate]', $commissionInfor->commission_fee_rate) !!}
                    {!! Form::hidden('commissionInfo['. $key .'][commission_type]', $commissionInfor->commission_type) !!}
                    {!! Form::hidden('commissionInfo['. $key .'][id]', $commissionInfor->id) !!}
                    <p class="get-fee-data">{{ $commissionInfor->corp_commission_type_disp }} {{ $commissionInfor->commission_fee_rate }} ％</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0" for="corp_claim_flg{{ $key}}">
                        <strong>@lang('demand_detail.merchant-claim')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <!-- {!! Form::hidden('commissionInfo['. $key .'][corp_claim_flg]', 0) !!} -->
                    {!! Form::checkbox('commissionInfo['. $key .'][corp_claim_flg]', 1, $commissionInfor->corp_claim_flg == 1, ['class' => 'custom-control-input', 'id' => 'corp_claim_flg' . $key]) !!}
                    <label class="custom-control-label" for='commissionInfor["corp_claim_flg]'></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.unit-price-rank') </strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex  mb-lg-0">
                    {!! Form::hidden('commissionInfo['. $key .'][select_commission_unit_price_rank]', $commissionInfor->select_commission_unit_price_rank) !!}
                    <p>{{ $commissionInfor->select_commission_unit_price_rank }}@lang('demand_detail.unit-price-per-contract')   {{ yenFormat2($commissionInfor->select_commission_unit_price) }}</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong></strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">

            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.notes')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                {!! $commissionInfor->mCorpAndMCorpNewYear && $commissionInfor->mCorpAndMCorpNewYear->affiliationInfo ? nl2br($commissionInfor->mCorpAndMCorpNewYear->affiliationInfo->attention) : ''  !!}
            </div>
        </div>

    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.long-term-holidays-situation')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="table-responsive">
                        <table class="table table-bordered table-list">
                                <thead>
                                @if(!empty($vacationLabels))
                                    <tr>
                                        @foreach($vacationLabels as $k => $label)
                                            {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][label_0' . $k . ']', $label) !!}
            
                                            <th>{{ $label }}</th>
            
                                        @endforeach
                                    </tr>
                                @endif
                                </thead>
                                <tbody>
                                    <tr>
                                        @foreach($vacationLabels as $index => $label)
                                                {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][status_0' . $index . ']', '') !!}
                                                <td>
                                                    @if($commissionInfor->mCorpAndMCorpNewYear && $commissionInfor->mCorpAndMCorpNewYear->mCorpNewYear)
                                                        {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][status_0' . $index . ']',
                                                            $commissionInfor->mCorpAndMCorpNewYear->mCorpNewYear->{'status_0' . $index}) !!}
            
                                                        {{ $commissionInfor->mCorpAndMCorpNewYear->mCorpNewYear->{'status_0' . $index} }}
                                                    @endif
                                                </td>
            
                                        @endforeach
                                    </tr>
                                <tr>
                                    <td>@lang('demand_detail.remarks')</td>
                                    <td colspan="{{ count($vacationLabels) }}">
                                        {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][note]', '') !!}
                                        @if($commissionInfor->mCorpAndMCorpNewYear && $commissionInfor->mCorpAndMCorpNewYear->mCorpNewYear)
                                            {!! Form::hidden('commissionInfo['. $key .'][mCorpNewYear][note]', $commissionInfor->mCorpAndMCorpNewYear->mCorpNewYear->note) !!}
                                            {{ $commissionInfor->mCorpAndMCorpNewYear->mCorpNewYear->note }}
                                        @endif
                                    </td>
            
                                </tr>
            
                                </tbody>
                            </table>
                </div>
               
            </div>
        </div>

    </div>
    {!! Form::hidden('commissionInfo['. $key .'][id]', $commissionInfor->id, ['id' => 'CommissionInfo' . $key . 'Id']) !!}
    {!! Form::hidden('commissionInfo['. $key .'][select_commission_unit_price]', $commissionInfor->select_commission_unit_price) !!}
    {!! Form::hidden('commissionInfo['. $key .'][introduction_not]', $commissionInfor->introduction_not) !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][fax]', $commissionInfor->mCorpAndMCorpNewYear->fax ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][mailaddress_pc]', $commissionInfor->mCorpAndMCorpNewYear->mailaddress_pc ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][coordination_method]', $commissionInfor->mCorpAndMCorpNewYear->coordination_method ?? 0) !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][contactable_time]', $commissionInfor->mCorpAndMCorpNewYear->contactable_time ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][holiday]', $commissionInfor->mCorpAndMCorpNewYear->holiday1 ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorp][commission_dial]', $commissionInfor->mCorpAndMCorpNewYear->commission_dial ?? '') !!}

    {!! Form::hidden('commissionInfo['. $key .'][auctionInfo][attention]', $demand->affiliationInfo ? $demand->affiliationInfo->attention : '') !!}

    {!! Form::hidden('commissionInfo['. $key .'][mCorpCategory][order_fee]', $commissionInfor->mCorpAndMCorpNewYear->m_corps_categories->order_fee ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorpCategory][order_fee_unit]', $commissionInfor->mCorpAndMCorpNewYear->m_corps_categories->order_fee_unit ?? '') !!}
    {!! Form::hidden('commissionInfo['. $key .'][mCorpCategory][note]', $commissionInfor->mCorpAndMCorpNewYear->m_corps_categories->note ?? '') !!}
</div>

