<?php

namespace App\Services\Credit;

use App\Helpers\MailHelper;
use App\Repositories\AffiliationInfoRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;

class CreditService
{
    const CREDIT_NORMAL = 'normal';
    const CREDIT_WARNING = 'warning';
    const CREDIT_DANGER = 'danger';
    const MAIL_DANGER_TEMPLATE = 'email_template.credit_danger';
    const MAIL_WARNING_TEMPLATE = 'email_template.credit_warning';

    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepository;

    /**
     * @var AffiliationInfoRepositoryInterface
     */
    protected $affiliationRepository;

    /**
     * CreditService constructor.
     *
     * @param CommissionInfoRepositoryInterface  $commissionInfoRepository
     * @param AffiliationInfoRepositoryInterface $affiliationInfoRepository
     */
    public function __construct(CommissionInfoRepositoryInterface $commissionInfoRepository, AffiliationInfoRepositoryInterface $affiliationInfoRepository)
    {
        $this->commissionInfoRepository = $commissionInfoRepository;
        $this->affiliationRepository = $affiliationInfoRepository;
    }

    /**
     * @var array
     */
    public static $exceptCorpId = [1751, 1755, 3539];

    /**
     * @param integer $corpId
     * @param integer $genreId
     * @param bool $displayPrice
     * @param bool $mailFlg
     * @return int|string
     */
    public function checkCredit($corpId = null, $genreId = null, $displayPrice = false, $mailFlg = false)
    {
        if (in_array($corpId, self::$exceptCorpId)) {
            return $displayPrice ? 0 : self::CREDIT_NORMAL;
        }
        $priceResult = $this->commissionInfoRepository->checkCreditSumPrice($corpId);

        if ($mailFlg && in_array($priceResult, [self::CREDIT_DANGER, self::CREDIT_WARNING])) {
            $price = $this->commissionInfoRepository->checkCreditSumPrice($corpId);
            $this->sendMailToAll($corpId, $priceResult, $price);
        }

        return $priceResult;
    }

    /**
     * @param integer $corpId
     * @param boolean $creditFlg
     * @param float $price
     */
    public function sendMailToAll($corpId = null, $creditFlg = null, $price = null)
    {
        $affiliation = $this->affiliationRepository->getOneMCorp($corpId);
        if (!$affiliation->credit_limit) {
            return;
        }

        $information = [
            'from_mail' => 'mailback@rits-c.jp',
            'from_name' => 'シェアリングテクノロジー株式会社'
        ];
        if ($creditFlg == self::CREDIT_WARNING && $affiliation->credit_mail_send_flg == 0) {
            $information = array_merge(
                $information,
                [
                'subject' => '【重要】与信限度額残高のお知らせ《' . $affiliation->mCorp->id . '》',
                'data' => ['price' => $affiliation->credit_limit + $affiliation->add_month_credit - $price],
                'template' => 'credit_warning',
                'credit_mail_send_flg' => 1
                ]
            );
        }
        if ($creditFlg == self::CREDIT_DANGER && $affiliation->credit_mail_send_flg != 2) {
            $information = array_merge(
                $information,
                [
                'subject' => '【重要】お取引が与信限度額残高に達している可能性がございます《' . $affiliation->mCorp->id . '》',
                'data' => ['price' => $affiliation->credit_limit + $affiliation->add_month_credit - $price],
                'template' => 'credit_danger',
                'credit_mail_send_flg' => 2
                ]
            );
        }


        $emailsForPC = $affiliation->mCorp->email_by_array;
        $emailForMobile = $affiliation->mCorp->email_mobile_by_array;

        $this->sendMail($emailsForPC, $information);
        $this->sendMail($emailForMobile, $information, 'Mobile');

        $this->affiliationRepository->updateById(
            $affiliation->id,
            [
            'credit_mail_send_flg' => $information['credit_mail_send_flg']
            ]
        );

        return;
    }

    /**
     * @param array $emails
     * @param array $data
     * @param string $type
     */
    private function sendMail($emails, $data, $type = 'PC')
    {
        if (!empty($emails)) {
            return;
        }

        foreach ($emails as $email) {
            MailHelper::sendTemplateMail('email_template.' . $data['template'], [], ['to' => $email]);
            if (!MailHelper::failures()) {
                continue;
            }

            $this->logging($type, $data);
        }
        return;
    }

    /**
     * @param string $type
     * @param array $data
     * @return bool
     */
    public function logging($type, $data)
    {
        return \Log::error(
            'MailSend FAILURE: AffiliationInfo_Id: '.$data['id'].
            ' ' . $type .'Address: '.$data['to_address_pc'].' Template: '.$data['template']
        );
    }
}
