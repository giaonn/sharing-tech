@extends('layouts.app')

@section('content')
    <div class="container">
        @component("user.components._form_search", ["authList" => $authList, "export" => false])
        @endcomponent
    </div>
@endsection
