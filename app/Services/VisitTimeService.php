<?php

namespace App\Services;

use App\Repositories\VisitTimeRepositoryInterface;
use App\Models\VisitTime;
use Illuminate\Support\Facades\Log;

class VisitTimeService
{

    /**
     *
     * @var VisitTimeRepositoryInterface
     */
    protected $visitTimeRepo;
    /**
     * build visit time data by is_visit_time_range_flg field
     * @param  array $data     visit time data
     * @param  int $demandId demand id
     * @return array          visit time data
     */
    private function buildVisitTimeData($data, $demandId)
    {
        $visitTimeData = [];
        $idDelete = [];
        foreach ($data as $key => $val) {
            $doAuction = null;
            if (!empty($data['demandInfo']['do_auction'])) {
                $doAuction = 1;
            }
            if ($val['is_visit_time_range_flg'] == 0) {
                if (!empty($val['visit_time'])) {
                    $visitTimeData[$key]['id'] = $val['id'];
                    $visitTimeData[$key]['demand_id'] = $demandId;
                    $visitTimeData[$key]['visit_time'] = $val['visit_time'];
                    $visitTimeData[$key]['is_visit_time_range_flg'] = $val['is_visit_time_range_flg'];
                    $visitTimeData[$key]['do_auction'] = $doAuction;
                } elseif (!empty($val['id'])) {
                    $idDelete[] = $val['id'];
                }
                continue;
            }
            if (!empty($val['visit_time_from']) && !empty($val['visit_time_to'])) {
                $visitTimeData[$key]['id'] = $val['id'];
                $visitTimeData[$key]['demand_id'] = $demandId;
                $visitTimeData[$key]['visit_time_from'] = $val['visit_time_from'];
                $visitTimeData[$key]['visit_time_to'] = $val['visit_time_to'];
                $visitTimeData[$key]['is_visit_time_range_flg'] = $val['is_visit_time_range_flg'];
                $visitTimeData[$key]['visit_adjust_time'] = $val['visit_adjust_time'];
                $visitTimeData[$key]['do_auction'] = $doAuction;
            } elseif (!empty($val['id'])) {
                $idDelete[] = $val['id'];
            }
        }
        return ['visitTimeData' => $visitTimeData, 'idDelete' => $idDelete];
    }


    /**
     * constructor
     *
     * @param VisitTimeRepositoryInterface $visitTimeRepo
     */
    public function __construct(
        VisitTimeRepositoryInterface $visitTimeRepo
    ) {
        $this->visitTimeRepo = $visitTimeRepo;
    }

    /**
     * Update visit time
     *
     * @param  mixed $data
     * @return boolean
     */
    public function updateVisitTime($data)
    {
        Log::debug('___ start udpate visit-time __________');
        // If the visit date is not entered, I do nothing
        if (!array_key_exists('visitTime', $data)) {
            Log::debug('___ empty visitTime __________');
            return true;
        }
        // Retrieve deal ID
        $demandId = (array_key_exists('id', $data['demandInfo'])) ? $data['demandInfo']['id'] : null;
        // Register visit date information
        $visitTimeData = [];
        $idDelete = [];
        $allData = $this->buildVisitTimeData($data['visitTime'], $demandId);
        $idDelete = $allData['idDelete'];
        $visitTimeData = $allData['visitTimeData'];
        $insertData = [];
        $updateData = [];
        $date = date('Y-m-d h:i:s');
        $userId = auth()->user()->user_id;
        $visitTimeFiels = VisitTime::getField();

        foreach ($visitTimeData as $value) {
            unset($value['do_auction']);
            if ($value['id']) {
                $value['modified'] = $date;
                $value['modified_user_id'] = $userId;
                $updateData[] = $value;
            } else {
                unset($value['id']);
                $value['modified'] = $date;
                $value['created'] = $date;
                $value['modified_user_id'] = $userId;
                $value['created_user_id'] = $userId;
                $insertData[] = array_merge($visitTimeFiels, $value);
            }
        }

        if (!empty($insertData)) {
            Log::debug('___ insert visitTime __________');
            $this->visitTimeRepo->saveMany($insertData);
        }

        if (!empty($updateData)) {
            Log::debug('___ update visitTime __________');
            $this->visitTimeRepo->multipleUpdate($updateData);
        }

        if (!empty($idDelete)) {
            Log::debug('___ delete visitTime __________');
            $this->visitTimeRepo->multipleDelete($idDelete);
        }

        // Even if there is no data to be registered, normal termination
        Log::debug('___ end update visitTime __________');
        return true;
    }

    /**
     * Validate
     *
     * @param  mixed $attributes
     * @return boolean
     */
    public function validate($attributes)
    {
        $validate = [
            'validateAdjustTime' => $this->validateVisitTime($attributes),
            'validateVisitTimeFrom' => $this->validateVisitTimeFrom($attributes),
            'validateVisitTimeTo' => $this->validateVisitTimeTo($attributes),
            'validateVisitAdjustTime' => $this->validateVisitAdjustTime($attributes)
        ];
        return in_array(false, $validate);
//        return $this->validateVisitTime($attributes)
//            && $this->validateVisitTimeFrom($attributes)
//            && $this->validateVisitTimeTo($attributes)
//            && $this->validateVisitAdjustTime($attributes);
    }

    /**
     * Validate visit time
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function validateVisitTime($attributes)
    {
        if (!empty($attributes['visit_time'])) {
            if (!empty($attributes['do_auction'])) {
                if (strtotime($attributes['visit_time']) < strtotime(date('Y/m/d H:i'))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check is datetime
     *
     * @param  mixed $attribute
     * @return boolean
     */
    private function isDateTime($attribute)
    {
        return strtotime($attribute) != false;
    }

    /**
     * Validate visit time from
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function validateVisitTimeFrom($attributes)
    {
        if (empty($attributes['visit_time_from'])) {
            return true;
        }
        return $this->checkVisitTimeFrom($attributes) && $this->checkRequireTo($attributes);
    }

    /**
     * Check visit time from
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function checkVisitTimeFrom($attributes)
    {
        if (!empty($attributes['visit_time_from'])) {
            if (!empty($attributes['do_auction'])) {
                if (strtotime($attributes['visit_time_from']) < strtotime(date('Y/m/d H:i'))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check require to
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function checkRequireTo($attributes)
    {
        if (!isset($attributes['visit_time_from'])) {
            return true;
        }

        if (strlen($attributes['visit_time_from']) == 0) {
            return true;
        }

        if (!isset($attributes['visit_time_to']) || empty($attributes['visit_time_to'])) {
            session()->flash('errors.visitimeToRquired', '開始日時入力時は終了日時も入力してください。');
            if (empty($attributes['visit_adjust_time'])) {
                session()->flash('errors.adjustTimeRequire', '要時間調整を選択して、期間を入力するときは訪問日時要調整時間も入力して下さい。');
            }
            return false;
        }

        return true;
    }

    /**
     * Validate visit time to
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function validateVisitTimeTo($attributes)
    {
        return !empty($attributes['visit_time_to']) ? $this->isDateTime($attributes['visit_time_to'])
            && $this->checkVisitTimeTo($attributes)
            && $this->checkVisitTimeTo2($attributes)
            && $this->checkRequireFrom($attributes) : true
            ;
    }

    /**
     * Check visit time to
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function checkVisitTimeTo($attributes)
    {
        if (!empty($attributes['visit_time_to'])) {
            if (!empty($attributes['do_auction'])) {
                if (strtotime($attributes['visit_time_to']) < strtotime(date('Y/m/d H:i'))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check visit time 2
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function checkVisitTimeTo2($attributes)
    {
        if (!empty($attributes['visit_time_to']) && !empty($attributes['visit_time_from'])) {
            if (strtotime($attributes['visit_time_to']) < strtotime($attributes['visit_time_from'])) {
                session()->flash('errors.visitTimeToGTVisitTimefrom', '開始日時より過去の日時が入力されています。');

                return false;
            }
        }

        return true;
    }

    /**
     * Check require from
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function checkRequireFrom($attributes)
    {
        if (!isset($attributes['visit_time_to']) || empty($attributes['visit_time_to'])) {
            return true;
        }

        if (!isset($attributes['visit_time_from']) || empty($attributes['visit_time_from'])) {
            session()->flash('errors.visitTimeFromRequired', '終了日時入力時は開始日時も入力してください。');

            return false;
        };

        return true;
    }

    /**
     * Validate visit adjust time
     *
     * @param  mixed $attributes
     * @return boolean
     */
    private function validateVisitAdjustTime($attributes)
    {
        if ($attributes['is_visit_time_range_flg'] != 1) {
            return true;
        }
        if (!isset($attributes['visit_time_from']) || empty($attributes['visit_time_from'])) {
            return true;
        }

        if (empty($attributes['visit_adjust_time'])) {
            session()->flash('errors.adjust_time', __('demand.adjust_visit_time'));
            return false;
        }

        return true;
    }

    /**
     * Process validate visit time
     *
     * @param  mixed $data
     * @return boolean
     */
    public function processValidateVisitTime($data)
    {
        $errFlg = false;

        if (isset($data['visitTime'])) {
            $vData = [];

            foreach ($data['visitTime'] as $v) {
                array_forget($v, 'commit_flg');
                array_forget($v, 'visit_time_before');
                array_forget($v, 'id');
                $addArray = [];
                $addArray['demand_id'] = $data['demandInfo']['id'] ?? 0;

                if (!empty($data['demandInfo']['do_auction'])) {
                    $addArray['do_auction'] = 1;
                }

                $vData[] = $v + $addArray;
            }

            foreach ($vData as $viData) {
                if (!$this->validate($viData)) {
                    $errFlg = true;

                    break;
                }
            }
        }

        return $errFlg;
    }
}
