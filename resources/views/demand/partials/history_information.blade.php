<div class="form-category mb-4" id="correspondsinfo">
    @include('demand.create.anchor_top')
    <label class="form-category__label"> @lang('demand_detail.corresponding_history_information')</label>
    <div class="form-category__body clearfix">
        <div class="form-table mb-4">
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.corresponding_person')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::select('demandCorrespond[responders]', $userDropDownList, $demand->demandCorrespond->responders ?? Auth::user()->id ?? '', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.corresponding_date_time')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                            {!! Form::text('demandCorrespond[correspond_datetime]', dateTimeNowFormat(), ['class' => 'form-control datetimepicker']) !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>@lang('demand_detail.correspondence_contents')</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::textarea('demandCorrespond[corresponding_contens]', isset($demand->corresponding_contens) ? $demand->corresponding_contens : '', ['class' => 'form-control is-required', 'data-rules' => 'not-empty', 'id' => 'demandCorrespondContent']) !!}
                        @if ($errors->has('corresponding_contens'))
                            <label class="invalid-feedback d-block">{{$errors->first('corresponding_contens')}}</label>
                        @elseif ($errors->has('demandCorrespond.corresponding_contens'))
                            <label class="invalid-feedback d-block">
                                {{$errors->first('demandCorrespond.corresponding_contens') == __('demand.validation_error.not_empty') ? __('demand.validation_error.corresponding_contens') : $errors->first('demandCorrespond.corresponding_contens')
                                }}
                            </label>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">

            <table class="table table-list table-bordered" >
                <thead>

                <th align="center" width="50px">No</th>
                <th align="center" width="100px">@lang('demand_detail.person_in_charge')</th>
                <th align="center" width="120px">@lang('demand_detail.corresponding_date_time')</th>
                <th align="left">@lang('demand_detail.correspondence_contents')</th>

                </thead>
                <tbody>
                @if(isset($demand->demandCorresponds))
                    @forelse($demand->demandCorresponds as $key => $demandCorrespond)
                        <tr>
                            <td align="center"><a class="popupHistory"
                                                  style="cursor: pointer;text-decoration: underline; color: #f27b07"
                                                  data-toggle="modal"
                                                  data-url_data="{{ route('demand.history_input', $demandCorrespond->id) }}">
                                    {{ count($demand->demandCorresponds) - $key }}
                                </a>
                            </td>
                            <td align="center" id="user-{{ $demandCorrespond->id }}">{{ $demandCorrespond->user_name }}</td>
                            <td align="center" id="date-time-{{ $demandCorrespond->id }}">{{ $demandCorrespond->correspond_date_time_format }}</td>
                            <td align="left" id="content-{{ $demandCorrespond->id}}">{!! nl2br($demandCorrespond->corresponding_contens) !!}</td>
                        </tr>

                    @empty
                        <tr>
                            <td align="center" colspan="4">@lang('demand_detail.there_is_no_record')</td>
                        </tr>
                    @endforelse
                @endif
                </tbody>
            </table>

        </div>
    </div>
</div>
