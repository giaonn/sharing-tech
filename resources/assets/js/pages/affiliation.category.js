var AffiliationCategory = function () {

    function holidayInit() {
        $('.category-holiday').click(function () {
            $('.category-holiday-no-rest').prop('checked', false);
            $(this).prop('checked');
        });

        $('.category-holiday-no-rest').click(function () {
            $('.category-holiday').prop('checked', false);
            $(this).prop('checked');
        });
    }

    function contactTimeInit() {
        $('#contactable_support24hour').click(function () {
            if ($("#contactable_support24hour").prop('checked')) {
                $("#contactable_time_other").prop('checked', false);
                $('#contactable_time_from').val('');
                $('#contactable_time_to').val('');
            }
        });

        $("#contactable_time_other").click(function(){
            if ($("#contactable_time_other").prop('checked')) {
                $("#contactable_support24hour").prop('checked', false);
            }
        });

        $("#support24hour").click(function(){
            if ($("#support24hour").prop('checked')) {
                $("#available_time_other").prop('checked', false);
                $('#available_time_from').val('');
                $('#available_time_to').val('');
            }
        });

        $("#available_time_other").click(function(){
            if ($("#available_time_other").prop('checked')) {
                $("#support24hour").prop('checked', false);
            }
        });
    }

    function postcodeInit() {
        $("#address-search").click(function(){
            var url = $('#page-data').data('get-address-by-postcode-url');
            var zip = $('#postcode').val();
            if (typeof zip !== 'undefined' && zip !== '') {
                Address.getAddressByZipCode(url, zip, function ($data) {
                    if (!jQuery.isEmptyObject($data)) {
                        $('#address1').val($data['m_posts_jis_cd']);
                        $('#address2').val($data['address2']);
                        $('#address3').val($data['address3']);
                    }
                });
            }
        });

        $("#representative-address-search").click(function(){
            var url = $('#page-data').data('get-address-by-postcode-url');
            var zip = $('#representative_postcode').val();
            if (typeof zip !== 'undefined' && zip !== '') {
                Address.getAddressByZipCode(url, zip, function ($data) {
                    if (!jQuery.isEmptyObject($data)) {
                        $('#representative_address1').val($data['m_posts_jis_cd']);
                        $('#representative_address2').val($data['address2']);
                        $('#representative_address3').val($data['address3']);
                    }
                });
            }
        });
    }

    function mobileMailInit() {
        $("#mobile_mail_none").click(function(){
            if ($("#mobile_mail_none").prop('checked')) {
                $('#mobile_tel_type').attr('disabled', 'disabled');
                $('#mailaddress_mobile').attr('disabled', 'disabled');
                $('#mobile_tel_type_hidden').removeAttr('disabled');
                $('#mailaddress_mobile_hidden').removeAttr('disabled');
            } else {
                $('#mobile_tel_type').removeAttr('disabled');
                $('#mailaddress_mobile').removeAttr('disabled');
                $('#mobile_tel_type_hidden').attr('disabled', 'disabled');
                $('#mailaddress_mobile_hidden').attr('disabled', 'disabled');
            }
        });
    }

    function initCoordinationMethod() {
        $('#coordination_method').on('change', function () {
            if ($(this).val() === "6") {
                $('#coordination_method_message_info').addClass('d-none');
            } else {
                $('#coordination_method_message_info').removeClass('d-none');
            }
        });

    }

    function setupValidation() {
        $('#mobile_tel_type').rules('add', {
            required: "#mobile_mail_none:unchecked"
        });

        $('#contactable_time_from').rules('add', {
            required: "#contactable_time_other:checked"
        });

        $('#contactable_time_to').rules('add', {
            required: "#contactable_time_other:checked"
        });

        $('#available_time_from').rules('add', {
            required: "#available_time_other:checked"
        });

        $('#available_time_to').rules('add', {
            required: "#available_time_other:checked"
        });
    }

    function init() {
        holidayInit();
        contactTimeInit();
        Datetime.initForTimepicker();
        postcodeInit();
        mobileMailInit();
        initCoordinationMethod();
        FormUtil.validate('#updateCorp');
        setupValidation();

    }

    return {
        init: init
    }
}();
jQuery(document).ready(function () {
    AffiliationCategory.init();
});