<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset('assets/img/favicon.ico')}}" type="image/x-icon" rel="icon">

    <!-- Styles -->
    <link href="{{ mix('css/lib/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/jquery-ui-timepicker-addon.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/jquery.multiselect.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/jquery.multiselect.filter.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('style')
</head>
@php
    $logined = Auth::user();
    $class = 'body-wrapper-default';
    if (Route::current()->getName() == 'bill.moneyCorrespond') {
        $class = 'body-wrapper-custom-1';
    } else if (Route::current()->getName() == 'auction.proposal') {
        $class = 'body-wrapper-custom-2';
    }
@endphp
<body>
<div class="progress-block">
    <div class="progress"></div>
</div>
<div id="top">
    {{--Header component--}}
    @include('partials.header')

    {{--Main content component--}}
    <div class="{{ $class }}">

        @if (!empty($logined) && strcmp($logined->auth, 'affiliation') === 0 && !isset($typeProject))
            <div class="kameiten-notice">
                <div class="container">
                    <a href="{{ route('notice_info.near') }}">【近隣施工エリア拡大のおねがい】</a>
                </div>
            </div>
        @endif
        <div class="container">
            @yield('content')
        </div>
    </div>

    {{--Footer component--}}
    @include('partials.footer')
</div>

@include('partials.global_modal')

<!-- Scripts -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/lib/jquery-ui.min.js') }}"></script>
<script src="{{ mix('js/lib/jquery.ui.datepicker-ja.min.js') }}"></script>
<script src="{{ mix('js/pages/global.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
    $( document ).ready(function() {
        if (window.location.pathname != '/login' && $('form').has) {
            $('form').bind("keypress", function(e) {
                if (e.keyCode == 13 && !$(document.activeElement).is('textarea')) {               
                    e.preventDefault();
                    return false;
                }
            });
        }
    });
    $(document).on("keydown", disableF5);
</script>
@yield('script')
</body>
</html>
