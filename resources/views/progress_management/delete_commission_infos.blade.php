@extends('layouts.app')
@section('title', $title)
@section('style')

@endsection
@section('content')
    <div class="progress-management-delete">
        @if(session()->has('error'))
            <div class="box__mess box--error">
                {!! session('error') !!}
            </div>
        @endif
        @if(session()->has('success'))
            <div class="box__mess box--success">
                {!! session('success') !!}
            </div>
        @endif
        <label class="form-category__label mt-2">{{ trans('progress_management.delete_commission_infos.title') }}</label>
        <table class="table custom-border mt-4">
            <thead class="text-center bg-yellow-light">
                <tr>
                    <th class="align-middle p-1">{{ trans('progress_management.delete_commission_infos.file_id') }}</th>
                    <th class="align-middle p-1">{{ trans('progress_management.delete_commission_infos.file_name') }}</th>
                    <th class="align-middle p-1">{{ trans('progress_management.delete_commission_infos.file_upload_date') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="align-middle text-center p-1">{{$file->id}}</td>
                    <td class="align-middle p-1">{{$file->original_file_name}}</td>
                    <td class="align-middle text-center p-1">{{$file->import_date}}</td>
                </tr>
            </tbody>
        </table>

        <form method="post" id="delete_commission_infos" action="{{ URL::route('post.progress.management.delete.commission.infos', $file->id) }}">
            {{ csrf_field() }}
            <div class="form-group row mx-0">
                <div class="col-lg-6 px-0 d-flex align-items-center">
                    <label class="col-form-label font-weight-bold fs-20">{{ __('progress_management.delete_commission_infos.label_title') }}</label>
                </div>
                <div class="col-lg-6 px-0 d-flex align-items-center">
                    <label class="col-form-label">{{ __('progress_management.delete_commission_infos.label_guide') }}</label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2 col-sm-3 d-flex align-items-center">
                    <label class="col-form-label font-weight-bold" for="delete_ids">{{ __('progress_management.delete_commission_infos.label_commission_id') }}</label>
                </div>
                <div class="col-sm-9 col-md-10">
                    <textarea class="form-control" name="delete_ids" rows="3">{{ old('delete_ids') }}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="offset-sm-4 col-sm-4 text-center">
                    <button class="btn btn--gradient-green font-weight-bold col-12 remove-focus-effect" type="button" id="deleteButton">{{ __('progress_management.delete_commission_infos.button_submit') }}</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="{{ mix('js/utilities/st.common.js') }}"></script>
    <script>
        var msg = "@lang('progress_management.delete_commission_infos.confirm_request')";
        var confirm = "@lang('support.confirm')";
        var cancel = "@lang('support.cancel')";
    </script>
    <script src="{{ mix('js/pages/delete_commission_infos.js') }}"></script>
@endsection
