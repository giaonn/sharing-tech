var demandModule = (function () {
    var $agencyInfoSection = $('#agency_info'),
        $normalInfoSection = $('.normal_info'),
        $bidInfoSection = $('#bid_infos'),
        $radioAbsoluteTimeSection = $('.absolute_time'),
        $radioRangeTimeSection = $('.range_time'),
        $maxLimitNumMessageEl = $('#MaxLimitNumMessage'),
        $autoCommissionMessageEl = $('#auto_commission_message'),
        $displayAutoCommissionMessageEl = $('#display_auto_commission_message'),
        $selectionSystemDivSection = $('#selection-system-div'),
        $demandStatusEl = $("#demand_status"),
        $orderFailReasonEl = $('#order_fail_reason'),
        $orderFailDateEl = $('#order_fail_date'),
        $crossSellSourceSiteEl = $('#cross_sell_source_site'),
        $crossSellSourceGenreEl = $('#cross_sell_source_genre'),
        $crossSellSourceCategoryEl = $('#cross_sell_source_category'),
        $attentionEl = $("#attention"),
        $sendCommissionInfoBtn = $('#sendCommissionInfoBtn'),
        $sendIntroduceInfoBtn = $('#sendIntroduceInfoBtn'),
        $sendCommissionInfoEl = $('#sendCommissionInfo'),
        $contactDesiredTimeEl = $("#contact_desired_time"),
        $notSendEl = $('#notSend'),
        $commissionTypeSection = $('.commission_type'),
        $totalCurrentViewSection = $('.total-current-views'),
        $sourceDemandId = $('#source_demand_id'),
        $beforeDemandInfoGenreId = $('#before_demandinfo_genre_id'),
        $demandContentEl = $('#demand-content'),
        $businessTripAmountEl = $('#business-trip-mount'),
        $middleNightCheckbox = $('#nighttime_takeover'),
        $commissionNoteSenderEl = $('.commission_note_sender'),
        $appointerEl = $('.appointers'),
        $visitTimeSection = $('.visit-time-div'),
        $hiddenTimeSection = $('.hidden-time'),
        $demandCommissionInfoSection = $('#partner_commission_info');

    var $siteEl = $('#site_id'),
        $genreEl = $('#genre_id'),
        $categoryEl = $('#category_id'),
        $address1El = $('#address1'),
        $address2El = $('#address2'),
        $address3El = $('#address3'),
        $postcodeEl = $('#postcode'),
        $introduceInfoCountEl = $('#introduce_info_count'),
        $introductionEL = $('#introduction'),
        $selectionSystemEl = $('#selection_system'),
        $quickOrderFailReasonEl = $('#quick-order-fail-reason'),
        $maxIndexEl = $('#max_index'),
        $loadMCorpSection = $('#load_m_corps'),
        $txtAbsoluteTime = $('.txt_absolute_time'),
        $maxLimitNumText = $('#max_limit_num'),
        $visitTimeDiv = $('.visitTimeDiv');

    var $modalPopup = $('#modal-popup'),
        $modalDialog = $('#modal-dialog');

    var $searchAddressByZipButton = $('#search-address-by-zip'),
        $quickOrderFailButton = $('#quick_order_fail'),
        $resetRadioButton = $('#reset-radio'),
        $plus15MinutesButton = $('#plus-15-minus'),
        $copyButton = $('#demand_copy'),
        $crossButton = $('#demand_cross'),
        $destinationCompanyButton = $('#destination-company');

    var defineListSiteIds = [861, 889, 890, 910, 1312, 1313, 1314, 647, 953];
    var listSiteId = [861, 863, 889, 890, 1312, 1313, 1314];
    var crossSellDisabledList = ['861', '863', '889', '890', '1312', '1313', '1314'];

    var localStorage = window.localStorage;

    var initProgress = function () {
        return new progressCommon();
    };

    var progress = initProgress();

    var copyDemand = function (url) {
        $copyButton.on('click', function(e) {
            window.open().location.href = url;
        });
    };

    var crossDemand = function (url) {
        $crossButton.on('click', function(e) {
            window.open().location.href = url;
        });
    };

    var demandObj = {
        writeBrowse: function () {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getWriteBrowseUrl,
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function () {
                    demandObj.countBrowse();
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        countBrowse: function () {
            $.ajax({
                type: 'POST',
                url: apiRoutes.postCountBrowseUrl,
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    $totalCurrentViewSection.html(res.data);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        renderHtml: function(items, isDefaultOption, defaultSelected) {
            if (typeof isDefaultOption == 'undefined' || isDefaultOption == null)
                isDefaultOption = true;
            if(typeof defaultSelected == 'undefined' || defaultSelected == null)
                defaultSelected = '';

            var html = '';

            if (isDefaultOption)
                html += '<option value>--なし--</option>';

            for(var i in items){
                var category = items[i];
                html +=  defaultSelected == i ? `<option selected="selected" value=${i}>${category}</option> `: `<option value=${i}>${category}</option>`;
            }
            return html;
        },
        triggerFile: function (section) {
            section.find('input[type="file"]').trigger('click');
        },
        submitUploadAttachFile: function (btn, url) {
            $(btn).parents('form').attr('action', url).submit();
        },
        deleteAttachedFile: function (btnDelete, url) {
            $(btnDelete).parents('form').attr('action', url + '/' + $(btnDelete).data('attached_id')).submit();
        },
        resetAttachedFileValue: function (btnReset) {
            var parent = $(btnReset).parent().prev('.trigger-section-file');
            var file = parent.find('input[type="file"]');
            file.replaceWith(file.val('')).clone(true);
            parent.find('.reset-file-name').html('No file choosen');
        },
        setDemandStatus: function (isSelected) {
            $demandStatusEl.find('option:selected').removeAttr('selected');

            if (isSelected)
                $demandStatusEl.val($demandStatusEl.find('option').eq(1).val());

        },
        controlSelectionSystem: function (id, arr) {
            if (jQuery.inArray(parseInt(id), arr) !== -1) {
                // move item
                $selectionSystemEl.find('option').each(function (i, opt) {
                    if ($(opt).is(':selected')) {
                        $(opt).removeAttr('selected');
                    }
                    if ($(opt).text() == '入札式+自動') {
                        $(opt).css('display', 'none');
                    }
                });
                demandObj.setDemandStatus(true);
                demandObj.changeModeOrderFailReason();
            } else {
                $selectionSystemEl.find('option').each(function (i, opt) {
                    if ($(opt).css('display') == 'none') {
                        $(opt).removeAttr('style');
                    }
                });
                demandObj.setDemandStatus(false);
                demandObj.changeModeOrderFailReason();
            }
        },
        controlSection: function (section, isShow) {
            if (isShow)
                section.show();
            else
                section.hide();
        },
        controlDisabled: function (el, isDisabled) {
            if (isDisabled)
                el.attr('disabled', true);
            else
                el.removeAttr('disabled');
        },
        addMinutes: function (minutes) {
            if (typeof minutes == 'undefined' || minutes == null)
                minutes = 15;

            var now = new Date();

            now.setMinutes(now.getMinutes() + minutes);
            var hours = (now.getHours()<10?'0':'') + now.getHours();
            var min = (now.getMinutes()<10?'0':'') + now.getMinutes();
            var time = demandObj.dateNowFormat() + ' ' + hours + ':' + min;
            $contactDesiredTimeEl.val(time);

        },
        middleNightCheckbox: function (checkbox) {
            var hours = (new Date()).getHours();
            if (hours >= 21 || hours < 7) {
                $(checkbox).prop('checked', true);
            }
        },
        getUserList: function () {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getUserListUrl,
                data: {},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    var comissionNoteSenderHtml = demandObj.renderHtml(res.data);
                    var apointerHtml = demandObj.renderHtml(res.data, true, apiRoutes.currentUserId);
                    $(document).find('.commission_note_sender').html(comissionNoteSenderHtml);
                    $(document).find('.appointers').html(apointerHtml);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        changeCategorySelection: function () {
            var categoryId = $categoryEl.find('option:selected').val();
            demandObj.changeCategoryCommissionData(categoryId);
        },
        changeCategoryCommissionData: function(categoryId) {

            for (var i = 0; i <= 19; i++) {
                var corpId = $('#CommissionInfo' + i + 'CorpId').val();

                if (corpId != '' && categoryId != '') {
                    if (isNaN(corpId) == false) {
                        $.ajax({
                            type: 'GET',
                            url: apiRoutes.getCommissonChangeUrl,
                            data: {num: i,category_id: categoryId, corp_id: corpId},
                            xhr: function () {
                                return progress.createXHR();
                            },
                            beforeSend: function () {
                                progress.controlProgress(true);
                            },
                            complete: function () {
                                progress.controlProgress(false);
                            },
                            success: function (res) {
                                var data = response.data;
                                var dt = data.split(",");
                                var order_fee_dis = '';
                                var unit_dis = '取次時手数料率';
                                var commission_inp_dis = '<input name="data[CommissionInfo][' + dt[0] + '][commission_fee_rate]" id="commission_fee_rate' + dt[0] + '" style="width:100px" size="20" maxlength="10" type="text"/> ％';
                                if (dt[1] != '' && dt[2] != '') {
                                    if (dt[2] == '0') {
                                        order_fee_dis = dt[1].toString().replace(/(\d)(?=(\d\d\d)+$)/g, '$1,') + '円';
                                        unit_dis = '取次先手数料';
                                        commission_inp_dis = '<input name="data[CommissionInfo][' + dt[0] + '][corp_fee]" id="corp_fee' + dt[0] + '" style="width:100px" size="20" maxlength="10" type="text"/> 円';
                                    } else {
                                        order_fee_dis = dt[1] + '％';
                                        unit_dis = '取次時手数料率';
                                        commission_inp_dis = '<input name="data[CommissionInfo][' + dt[0] + '][commission_fee_rate]" id="commission_fee_rate' + dt[0] + '" style="width:100px" size="20" maxlength="10" type="text"/> ％';
                                    }
                                }
                                // 表示用
                                $('#order_fee_display' + dt[0]).html(order_fee_dis);
                                $('#m_corp_category_note_display' + dt[0]).html(dt[3]);
                                // hidden用
                                $('#order_fee' + dt[0]).val(dt[1]);
                                $('#order_fee_unit' + dt[0]).val(dt[2]);
                                $('#commission_order_fee_unit' + dt[0]).val(dt[2]);
                                $('#m_corp_category_note' + dt[0]).val(dt[3]);
                                // inputタグ用
                                $("#unit_display" + dt[0]).html(unit_dis);
                                $("#commission_inp_display" + dt[0]).html(commission_inp_dis);
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                    }
                }
            }
        },
        changeSiteId: function() {
            var siteId = $siteEl.val();

            $.ajax({
                type: 'GET',
                url: apiRoutes.getGenreListBySiteIdUrl,
                data: {site_id: siteId},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    var genresList = res.data;
                    var genreHtml = demandObj.renderHtml(genresList, true);
                    $genreEl.html(genreHtml);
                    initTime();

                    $visitTimeSection.css({display: 'block'});
                    $hiddenTimeSection.css({display: 'block'});
                    demandObj.controlDisabled($commissionTypeSection, false);

                    if(listSiteId.indexOf(parseInt(siteId)) !== -1) {
                        demandObj.controlDisabled($crossSellSourceSiteEl, false);
                        demandObj.controlDisabled($crossSellSourceGenreEl, false);
                    } else {
                        demandObj.controlDisabled($crossSellSourceSiteEl, true);
                        demandObj.controlDisabled($crossSellSourceGenreEl, true);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });

            demandObj.getCategoryList();
            demandObj.getSiteData(siteId);
            demandObj.crossSellDisabled(siteId);
            demandObj.controlSelectionSystem(siteId, defineListSiteIds);
            $selectionSystemEl.val('');
        },
        crossSellDisabled: function(siteId) {
            if (crossSellDisabledList.indexOf(siteId) != -1){
                demandObj.controlDisabled($crossSellSourceSiteEl, false);
                demandObj.controlDisabled($crossSellSourceGenreEl, false);
                demandObj.controlDisabled($crossSellSourceCategoryEl, false);
                $selectionSystemEl.val(0);
            } else {
                demandObj.controlDisabled($crossSellSourceSiteEl, true);
                demandObj.controlDisabled($crossSellSourceGenreEl, true);
                demandObj.controlDisabled($crossSellSourceCategoryEl, true);
            }
        },
        getCategoryList: function() {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getCategoryListByGenreIdUrl,
                data: {genreId: null},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    var categories = res.data;
                    var categoryHtml = demandObj.renderHtml(categories);
                    $categoryEl.html(categoryHtml);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        getCategory: function (genreId, el) {
            if (typeof genreId == 'undefined' || genreId == '')
                genreId = null;
            $.ajax({
                type: 'GET',
                url: apiRoutes.getCategoryListByGenreIdUrl,
                data: {genreId: genreId},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    var categories = res.data;

                    if(!isRegist){
                        var categoryHtml = demandObj.renderHtml(categories, true, parseInt(categoryId));
                    }else{
                        var categoryHtml = demandObj.renderHtml(categories);
                    }
                    el.html(categoryHtml);
                    el.trigger('change');
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        getInquiryItem: function (genreId) {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getInquiryItemDataUrl,
                data: {genreId: genreId},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    $demandContentEl.val(res.data ? res.data.inquiry_item : '');
                    $attentionEl.html(res.data ? res.data.attention : '');
                    $beforeDemandInfoGenreId.val(genreId);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        changeGenreSelection: function(genreId, renderElm) {
            if (typeof renderElm == 'undefined' || renderElm == '' || renderElm == null)
                renderElm = $categoryEl;

            var genreSelected = $genreEl.find('option:selected');
            var genreId = genreSelected.val();
            var genreName = genreSelected.text();
            var siteId = $siteEl.val();
            var sourceDemandIdVal = $sourceDemandId.val();

            demandObj.getCategory(genreId, renderElm);

            var beforeDemandInfoGenreIdVal = $beforeDemandInfoGenreId.val();

            if(siteId == 861){
                if(beforeDemandInfoGenreIdVal == "" && genreId != ""){
                    demandObj.getInquiryItem(genreId);
                    $beforeDemandInfoGenreId.val(genreId);
                }
            } else if(beforeDemandInfoGenreIdVal == "" && genreId != "") {
                $beforeDemandInfoGenreId.val(genreId);
            }

            if(genreId != ""){
                demandObj.getInquiryItem(genreId);
            }
        },
        resetRadio: function(btnRest){
            btnRest.parents('.group-radio-pet-tomb').find('input[type="radio"]').prop('checked', false);
        },
        enableTextBox : function(radioElm, enableElm, disabledElm) {
            if($(radioElm).is(':checked')){
                var parent = $(radioElm).parents('.py-2');
                parent.find('.' + enableElm).val('').attr('disabled', false);
                parent.find('.' + disabledElm).val('').attr('disabled', true);
            }
        },
        getSelectionSystemList: function(genreId, address1, default_value = false) {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getSelectionSystemListUrl,
                data: {genre_id: genreId, address1: address1},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    var selectionType = res.data.selection_system;
                    var defaultValue = !default_value ? res.data.default_value : default_value;

                    var selectionSystemHtml = demandObj.renderHtml(selectionType, false, defaultValue);
                    $selectionSystemEl.html(selectionSystemHtml);
                    $selectionSystemEl.trigger('change');

                    var siteId = $siteEl.val();
                    var selectSystemValue = $selectionSystemEl.val();

                    if(siteId === 861){
                        if(selectSystemValue.size() > 0){
                            $selectionSystemEl.val(selectSystemValue);
                        }
                    }

                    if(defineListSiteIds.indexOf(parseInt(siteId)) !== -1){
                        $selectionSystemEl.find('option').each(function(index, opt){
                            if ($(opt).is(':selected')) {
                                $(opt).removeAttr('selected');
                            }
                            if ($(opt).text() === '入札式+自動') {
                                $(opt).css('display', 'none');
                            }
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        getSiteDataByContent : function(siteId, contentType) {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getSiteDataUrl,
                data: {site_id: siteId, content: contentType},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    if (contentType == 1) {
                        var url = res.data ? res.data.site_url : '';
                        $('#site_url').html(url).attr('href', 'http://' + url);
                        $('#hidSiteUrl').val('http://' + url);
                    } else if(contentType == 2) {
                        if(res.data){
                            var commissionTypeName = res.data.m_commission_type ? res.data.m_commission_type.commission_type_name : '';
                            $('#commission_type_data').html(commissionTypeName);
                            $('#commission_type_data_hidden').val(commissionTypeName);
                            $('#commission_type_div').val(res.data.m_commission_type.commission_type_div);
                            demandObj.controlSection($commissionTypeSection, true);
                            var displayData = res.data.m_commission_type.commission_type_div;
                            switch (displayData) {
                                case 1:
                                case 2:
                                    demandObj.controlSection($agencyInfoSection, true);
                                    demandObj.controlSection($normalInfoSection, true);

                                    demandObj.selectionSystemAgencyInfoDis(true);
                                    demandObj.clearIntroduceInfo();
                                    break;
                                default:
                                    demandObj.controlSection($agencyInfoSection, true);
                                    demandObj.controlSection($normalInfoSection, true);

                                    demandObj.selectionSystemAgencyInfoDis(true);
                                    demandObj.clearIntroduceInfo();
                                    break;

                            }
                        }
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        getSiteData : function(siteId) {
            if (siteId != "") {
                demandObj.getSiteDataByContent(siteId, 1);
                demandObj.getSiteDataByContent(siteId, 2);
            } else {
                demandObj.controlSection($normalInfoSection, false);
                demandObj.controlSection($commissionTypeSection, false);
            }
        },
        selectionSystemAgencyInfoDis: function(isChanged) {
            if (!isChanged)
                demandObj.controlSection($agencyInfoSection, true);
            demandObj.controlSection($bidInfoSection, true);

            var selectionSystem = $selectionSystemEl.find ('option:selected').val();
            if (selectionSystem == "2" || selectionSystem == "3") {

                if ($('#CommissionInfo0Id').length === 0) {
                    demandObj.controlSection($agencyInfoSection, false);
                } else {
                    demandObj.controlSection($bidInfoSection, false);
                }
            }
        },
        clearIntroduceInfo: function () {
            $introductionEL.text('');
            $introduceInfoCountEl.val(0);
        },
        getBusinessTripAmount: function(genreId, address1) {

            if (address1 != "" && genreId != "") {
                demandObj.travelExpress(genreId, address1);
            }
        },
        existsAutoCommissionCorp: function() {
            var siteId = $siteEl.val(),
                categoryId = $categoryEl.find('option:selected').val(),
                genreId = $genreEl.find('option:selected').val(),
                address1 = $address1El.find('option:selected').val();

            if (siteId != "" && categoryId != "" && genreId != "" && address1 != "") {
                $.ajax({
                    type: 'GET',
                    url: apiRoutes.getExistAutoCommissionCorpUrl,
                    data: {genre_id: genreId, prefecture_code: address1, site_id : siteId, category_id: categoryId},
                    xhr: function () {
                        return progress.createXHR();
                    },
                    beforeSend: function () {
                        progress.controlProgress(true);
                    },
                    complete: function () {
                        progress.controlProgress(false);
                    },
                    success: function (res) {
                        console.log(res);
                        if(res.data == 1)
                        {
                            $autoCommissionMessageEl.html("現在のカテゴリ、都道府県には自動選定対象加盟店があります。<br>自動で選定を実行する場合は案件状況を未選定に設定し、登録ボタンを押してください。");
                            $autoCommissionMessageEl.css({'color': '#CC0000'});
                            $autoCommissionMessageEl.val("1");
                        }
                        else if(res.data == 2)
                        {
                            $autoCommissionMessageEl.html("現在のカテゴリ、都道府県には自動取次対象加盟店があります。<br>自動で取次を実行する場合は案件状況を未選定に設定し、登録ボタンを押してください。");
                            $autoCommissionMessageEl.css({'color': '#CC0000'});
                            $displayAutoCommissionMessageEl.val("1");
                        }
                        else
                        {
                            $autoCommissionMessageEl.html("");
                            $displayAutoCommissionMessageEl.val("0");
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
            else
            {
                $autoCommissionMessageEl.html("");
                $displayAutoCommissionMessageEl.val("0");
            }
        },
        changeModeOrderFailReason: function() {
            var demandStatusVal = parseInt($demandStatusEl.val());
            var genreId = $genreEl.find('option:selected').val();
            var address1 = $address1El.find('option:selected').val();

            if (demandStatusVal == 6 || demandStatusVal == 9) {
                $selectionSystemEl.val('0');
                demandObj.getBusinessTripAmount(genreId, address1);
                demandObj.selectionSystemAgencyInfoDis(false);
            }

            if (demandStatusVal == 4) {
                demandObj.controlDisabled($orderFailReasonEl, false);

                var orderFailDateVal = $orderFailDateEl.val();
                if (orderFailDateVal == '') {
                    $orderFailDateEl.val(demandObj.dateNowFormat());
                }
            } else {
                demandObj.controlDisabled($orderFailReasonEl, true);
                $orderFailDateEl.val('');
            }
        },
        controlContactTimeEntryFields: function() {
            var turnOnOff = function (flg) {
                var option = {
                    controlType: 'select',
                    oneLine: true,
                    timeText: '時間',
                    hourText: '時',
                    minuteText: '分',
                    currentText: '現時刻',
                    closeText: '閉じる',
                };

                if (flg) {
                    $("#DemandInfoContactDesiredTimeFrom, #DemandInfoContactDesiredTimeTo")
                        .prop('readonly', true)
                        .val("").addClass("readonly").datetimepicker( "destroy");
                    $contactDesiredTimeEl.prop('readonly', false).removeClass("readonly").datetimepicker(option);
                } else {
                    $("#DemandInfoContactDesiredTimeFrom, #DemandInfoContactDesiredTimeTo")
                        .prop('readonly', false).removeClass("readonly").datetimepicker(option);
                    $contactDesiredTimeEl.prop('readonly', true).val("").addClass("readonly").datetimepicker( "destroy");
                }
            }
            if (document.getElementById("DemandInfoIsContactTimeRangeFlg0").checked) {
                turnOnOff(true);
                return;
            }
            if (document.getElementById("DemandInfoIsContactTimeRangeFlg1").checked) {
                turnOnOff(false);
                return;
            }
        },
        controlVisitTimeEntryFields: function(num) {
            var turnOnOff = function (flg) {
                var option = {
                    controlType: 'select',
                    oneLine: true,
                    timeText: '時間',
                    hourText: '時',
                    minuteText: '分',
                    currentText: '現時刻',
                    closeText: '閉じる',
                };
                var vistTimeNum = "#VisitTime" + num.toString();
                var selector = vistTimeNum + "VisitTimeFrom," +vistTimeNum + "VisitTimeTo," + vistTimeNum + "VisitAdjustTime";
                if (flg) {
                    $(selector).prop('readonly', true)
                        .val("")
                        .addClass("readonly")
                        .datetimepicker( "destroy");
                    $("#visit_time" + num.toString()).prop('readonly', false)
                        .removeClass("readonly")
                        .datetimepicker(option);
                } else {
                    $(selector).prop('readonly', false)
                        .removeClass("readonly")
                        .datetimepicker(option);
                    $("#visit_time" + num.toString()).prop('readonly', true)
                        .val("")
                        .addClass("readonly")
                        .datetimepicker( "destroy");
                }
            }
            if (document.getElementById("VisitTime" + num.toString() + "IsVisitTimeRangeFlg0").checked) {
                turnOnOff(true);
                return;
            }
            if (document.getElementById("VisitTime" + num.toString() + "IsVisitTimeRangeFlg1").checked) {
                turnOnOff(false);
                return;
            }
        },
        travelExpress : function(genreId, address) {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getBusinessTripAmountUrl,
                data: {genre_id: genreId, address1: address},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    $businessTripAmountEl.val(res.data.business_trip_amount);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        searchAddressByZip: function(zip){
            $.ajax({
                type: 'GET',
                url: apiRoutes.getAddressByZipUrl,
                data: {zip: zip},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    if(!$.isEmptyObject(res)){

                        var genreId = $genreEl.val(),
                            address = $address1El.val();

                        demandObj.travelExpress(genreId, address);
                        $address1El.val(parseInt(res.m_posts_jis_cd));
                        $address1El.trigger('change');
                        $address2El.val(res.address2);
                        $address3El.val(res.address3);

                        if($businessTripAmountEl.val() == '') return;

                        var address1 = $address1El.val();
                        if(genreId !== '' && address1 !== ''){
                            demandObj.travelExpress(genreId, address1);
                        }

                        if($selectionSystemEl.val() == '') return;
                        demandObj.getSelectionSystemList(genreId, address1);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        changeOrderFailReason: function(statusObj){
            var demandStatus = parseInt($(statusObj).val());
            if([6, 9].indexOf(demandStatus) !== -1){
                $selectionSystemEl.val(0);
                var genreId = $genreEl.find('option:selected').val(),
                    address1 = $address1El.val();
                demandObj.travelExpress(genreId, address1);
            }

            if(demandStatus == 6){
                $('#order-fail-reason').attr('disabled', false);
                $('.order-fail-date').val('' + demandObj.dateNowFormat() + '');
            }else{
                $('#order-fail-reason').attr('disabled', true);
                $('.order-fail-date').val('');
            }
        },
        dateNowFormat(){
            var date = new Date();
            var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();
            dd = dd < 10 ? '0' + dd : dd;
            mm = mm < 10 ? '0' + mm : mm;
            return yyyy + '/' + mm + '/' + dd;
        },
        showDialog: function(header, content){
            $modalDialog.find('h3').html(header);
            $modalDialog.find('.modal-body').html(content);
            $modalDialog.modal({show: true});
        },
        quickOrderFail: function(){
            var quickOrderFail = $quickOrderFailReasonEl.val(),
                genreId = $genreEl.find('option:selected').val(),
                categoryId = $categoryEl.val();

            if(quickOrderFail == '' || genreId == '' || categoryId == ''){
                demandObj.showDialog('入力エラー', 'ワンタッチ失注登録理由,ジャンル,カテゴリを選択して下さい。');
                return false;
            }
            $('#hidQuickOrderFail').val('1');
            $(window).off('beforeunload');
            var formDemand = $categoryEl.parents('form');
            //if (formDemand.valid()) {
            formDemand.submit();
            //}
        },
        getCrossSellSiteSelection: function(siteId) {
            if (siteId != "") {
                $.ajax({
                    type: 'GET',
                    url: apiRoutes.getCrossSourceSiteUrl,
                    data: {site_id : siteId},
                    xhr: function () {
                        return progress.createXHR();
                    },
                    beforeSend: function () {
                        progress.controlProgress(true);
                    },
                    complete: function () {
                        progress.controlProgress(false);
                    },
                    success: function (res) {
                        var crossSourceSiteHtml = demandObj.renderHtml(res.data.category_list, true, '');
                        $crossSellSourceGenreEl.html(crossSourceSiteHtml);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
        },
        getCommissionMaxLimit: function(siteId) {
            $.ajax({
                type: 'GET',
                url: apiRoutes.getCommissionMaxLimitUrl,
                data: {m_site_id : siteId},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    $('#auction_selection_limit').val(res.data.auction_selection_limit);
                    $('#manual_selection_limit').val(res.data.manual_selection_limit);
                    demandObj.showLimitText($selectionSystemEl.val(), res.data);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        showLimitText: function(selectionSystem, limit){
            if (typeof limit == 'undefined' || limit == null || limit == '')
                limit = {};

            var text = '確定可能な取次先は' + ([0, 4].indexOf(selectionSystem) !== -1 ? limit.manual_selection_limit : limit.manual_selection_limit) + ' 社です';
            $maxLimitNumText.html(text);
        },
    };
    var detectCommissionMaxLimit = {
        limit: {},
        setLimitData: function(siteId) {
            return $.ajax({
                type: 'GET',
                url: apiRoutes.getCommissionMaxLimitUrl,
                data: {m_site_id : siteId},
                xhr: function () {
                    return progress.createXHR();
                },
                beforeSend: function () {
                    progress.controlProgress(true);
                },
                complete: function () {
                    progress.controlProgress(false);
                },
                success: function (res) {
                    detectCommissionMaxLimit.limit = res.data;
                    $('#auction_selection_limit').val(res.data.auction_selection_limit);
                    $('#manual_selection_limit').val(res.data.manual_selection_limit);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        showLimitNum: function(selectionVal) {
            if(selectionVal == '0' || selectionVal == '4'){
                $maxLimitNumMessageEl.html('確定可能な取次先は' + detectCommissionMaxLimit.limit.manual_selection_limit + '社です');
            }
            if(selectionVal == '2' || selectionVal == '3'){
                $maxLimitNumMessageEl.html('確定可能な取次先は' + detectCommissionMaxLimit.limit.auction_selection_limit + '社です');
            }
        },
        onChangeSelection: function(selectionVal) {
            detectCommissionMaxLimit.showLimitNum(selectionVal);
            detectCommissionMaxLimit.disableVisitTime(selectionVal);
        },
        onChangeSiteID: function (siteId) {
            detectCommissionMaxLimit.setLimitData(siteId);
        },
        disableVisitTime: function(selectionVal) {
            var datetimeOption = {
                controlType: 'select',
                oneLine: true,
                timeText: '時間',
                hourText: '時',
                minuteText: '分',
                currentText: '現時刻',
                closeText: '閉じる'
            };

            var enable = function ($element) {
                if ($element.attr('type') == 'text') {
                    $element.prop('readonly', false).datetimepicker(datetimeOption);
                }
                if ($element.attr('type') == 'radio') {
                    $element.prop('disabled', false);
                }
                $element.removeClass("readonly");
            };
            var disable = function ($element) {
                if ($element.attr('type') == 'text') {
                    $element.prop('readonly', true).datetimepicker("destroy");
                }
                if ($element.attr('type') == 'radio') {
                    $element.prop('disabled', true);
                    $($element.get(0)).prop('checked', true);
                }
                $element.val("").addClass("readonly");
            };

            var $visitTimeRange1 = $("input[type='radio'][name=data\\[VisitTime\\]\\[0\\]\\[is_visit_time_range_flg\\]]");
            var $visitTimeRange2 = $("input[type='radio'][name=data\\[VisitTime\\]\\[1\\]\\[is_visit_time_range_flg\\]]");
            var $visitTimeRange3 = $("input[type='radio'][name=data\\[VisitTime\\]\\[2\\]\\[is_visit_time_range_flg\\]]");
            var $visitTime1 = $("input[name=data\\[VisitTime\\]\\[0\\]\\[visit_time\\]]");
            var $visitTime1From = $("input[name=data\\[VisitTime\\]\\[0\\]\\[visit_time_from\\]]");
            var $visitTime1To = $("input[name=data\\[VisitTime\\]\\[0\\]\\[visit_time_to\\]]");
            var $visitTime1Adjust = $("input[name=data\\[VisitTime\\]\\[0\\]\\[visit_adjust_time\\]]");
            var $visitTime2 = $("input[name=data\\[VisitTime\\]\\[1\\]\\[visit_time\\]]");
            var $visitTime2From = $("input[name=data\\[VisitTime\\]\\[1\\]\\[visit_time_from\\]]");
            var $visitTime2To = $("input[name=data\\[VisitTime\\]\\[1\\]\\[visit_time_to\\]]");
            var $visitTime2Adjust = $("input[name=data\\[VisitTime\\]\\[1\\]\\[visit_adjust_time\\]]");
            var $visitTime3 = $("input[name=data\\[VisitTime\\]\\[2\\]\\[visit_time\\]]");
            var $visitTime3From = $("input[name=data\\[VisitTime\\]\\[2\\]\\[visit_time_from\\]]");
            var $visitTime3To = $("input[name=data\\[VisitTime\\]\\[2\\]\\[visit_time_to\\]]");
            var $visitTime3Adjust = $("input[name=data\\[VisitTime\\]\\[2\\]\\[visit_adjust_time\\]]");

            // 無効
            if ((selectionVal == '2' || selectionVal == '3') && detectCommissionMaxLimit.limit.auction_selection_limit >= 2) {
                disable($visitTimeRange1);
                disable($visitTime1);
                disable($visitTime1From);
                disable($visitTime1To);
                disable($visitTime1Adjust);
                disable($visitTimeRange2);
                disable($visitTime2);
                disable($visitTime2From);
                disable($visitTime2To);
                disable($visitTime2Adjust);
                disable($visitTimeRange3);
                disable($visitTime3);
                disable($visitTime3From);
                disable($visitTime3To);
                disable($visitTime3Adjust);
                // 有効
            }else{
                enable($visitTimeRange1);
                enable($visitTime1);
                enable($visitTimeRange2);
                enable($visitTime2);
                enable($visitTimeRange3);
                enable($visitTime3);
            }
        },
        initialize: function(){
            var siteId = $siteEl.val();

            if (siteId) {
                detectCommissionMaxLimit.setLimitData(siteId, function () {
                    detectCommissionMaxLimit.showLimitNum($selectionSystemEl.val());
                    detectCommissionMaxLimit.disableVisitTime($selectionSystemEl.val());
                });
            }
        }
    };

    var loadModal = function(url_display_commission) {
        $.ajax({
            type: "GET",
            url: url_display_commission,
            xhr: function () {
                return progress.createXHR();
            },
            beforeSend: function (xhr) {
                progress.controlProgress(true);
            },
            complete: function () {
                progress.controlProgress(false);
            },
            success: function (data) {
                $modalPopup.children().children().find('.modal-body').html(data);
                $modalPopup.modal('show');
            },
            error: function () {
                console.log("Error!");
            }
        });
    };


    function loadCommissionInfo(){
        $demandCommissionInfoSection.css({display: 'block'});
        var currentSession = parseInt(localStorage.getItem('currentSession'));
        var currentIndex = parseInt(localStorage.getItem('currentIndex'));

        var mCorpStorage = $.makeArray(JSON.parse(localStorage.getItem('m_corps')));

        var mCorps = [].concat.apply([], mCorpStorage[currentSession][currentIndex]);
        console.log('m corps ', mCorps);
        // $loadMCorpSection.html('');

        var maxIndex = $maxIndexEl.length > 0 ? parseInt($maxIndexEl.data('max')) : 0;

        $.each(mCorps, function (index, data) {
            var tempIndex = parseInt(maxIndex + index + 1);
            var mCorp = JSON.parse(data);
            $loadMCorpSection.append(template(tempIndex, mCorp));
            demandObj.getUserList();
            $('.date').datetimepicker({
                controlType: 'select',
                oneLine: true,
                timeText: '時間',
                hourText: '時',
                minuteText: '分',
                currentText: '現時刻',
                closeText: '閉じる',
                locale: 'ja',
                onClose: function () {
                    $(this).trigger('blur');
                }
            });
        });
    };



    var init = function () {
        if(!hasCommission){
            localStorage.removeItem('m_corps');
            localStorage.removeItem('currentIndex');
            localStorage.removeItem('close_modal');
        }

        $('.m-corps-detail').on('click', function(e){
            e.preventDefault();
            // var urlData = $(this).data('url_data');
            var urlData = $(this).data('url_data') + '?fee_data=' + $(this).parents('.form-table').find('.get-fee-data').text();
            loadModal(urlData);
        });

        detectCommissionMaxLimit.initialize();
        var $demandIdHidden = $('#demand_id');
        var demandId = $demandIdHidden.val();

        // make sure that this is detail page
        if (!isRegist) {
            var genreId = $genreEl.val();
            demandObj.changeGenreSelection(genreId);
        }

        $crossSellSourceSiteEl.on('change', function() {
            var crossSellSourceSite = $crossSellSourceSiteEl.val();
            demandObj.getCrossSellSiteSelection(crossSellSourceSite);

            return false;
        });
        demandObj.middleNightCheckbox($middleNightCheckbox);

        $destinationCompanyButton.on('click', function(e) {
            e.preventDefault();

            localStorage.setItem('currentIndex', (parseInt(localStorage.getItem('currentIndex') || 0) + 1).toString());

            var urlData = $(this).data('url_data').split('?')[0];
            var currentSession = parseInt(localStorage.getItem('currentSession'));
            var mCorpsStorage = $.makeArray(JSON.parse(localStorage.getItem('m_corps')));
            var mCorpsArr = mCorpsStorage.indexOf(currentSession) !== -1 ? mCorpsStorage[currentSession].flatten() : [];
            var m = $.map(mCorpsArr, function (m_corp, index) {
                return JSON.parse(m_corp).corp_id;
            });

            var corpIds = [];
            $.each($('.corp_id'), function () {
                corpIds.push($(this).val());
            });

            var mCorpsId = m.concat(corpIds).join(',');
            var queryString = $.param({
                'data[no]': -1,
                'data[site_id]': $siteEl.val(),
                'data[category_id]': $categoryEl.val(),
                'data[postcode]': $postcodeEl.val(),
                'data[address1]': $address1El.val(),
                'data[address2]': $address2El.val(),
                'data[corp_name]': '',
                'data[commition_info_count]': 0,
                'data[exclude_corp_id]': mCorpsId,
                'data[genre_id]': $genreEl.find('option:selected').val(),
                'data[view]': true
            });

            $(this).data('url_data', [urlData, queryString].join('?'));
            var address1 = $address1El.val(), category = $categoryEl.val(), address2 = $address2El.val();
            if (address1 === '' || address1 === '--なし--' || category === '' || (address1 !== '不明' && address2 === '')) {
                demandObj.showDialog('', '都道府県、市区町村、カテゴリを選択してください。');
                return false;
            }
            loadModal([urlData, queryString].join('?'));

            return false;
        });

        $plus15MinutesButton.on('click', function(e){
            demandObj.addMinutes(15);

            return false;
        });

        $siteEl.on('change', function(e) {
            demandObj.changeSiteId();
            $categoryEl.trigger('change');
            demandObj.getCommissionMaxLimit($(this).val());
            demandObj.controlSection($selectionSystemDivSection, true);
            demandObj.existsAutoCommissionCorp();

            return false;
        });

        $quickOrderFailButton.on('click', function(e){
            demandObj.quickOrderFail();

            return false;
        });

        $demandStatusEl.on('change', function(e){
            demandObj.changeOrderFailReason(this);

            return false;
        });

        $searchAddressByZipButton.on('click', function(e){
            var postcode = encodeURI($postcodeEl.val());
            demandObj.searchAddressByZip(postcode);

            return false;
        });

        $categoryEl.on('change', function(e){
            demandObj.changeCategorySelection();
            demandObj.existsAutoCommissionCorp();

            return false;
        });

        $resetRadioButton.on('click', function(){
            demandObj.resetRadio($(this));

            return false;
        });


        $modalPopup.on('hidden.bs.modal', function () {
            if(localStorage.getItem('close_modal') === 'decide'){
                loadCommissionInfo();
            }
            return false;
        });

        $visitTimeDiv.first().find('input[type="text"]').on('change', function(){
            $visitTimeDiv.not(':first').find('input[type="text"]').val('');
        });

        $visitTimeDiv.not(':first').find('input[type="text"]').on('change', function(){
            $visitTimeDiv.first().find('input[type="text"]').val('');
        });

        $radioAbsoluteTimeSection.on('click', function(e){
            var contactDesiredTimeVal = $contactDesiredTimeEl.val();
            demandObj.enableTextBox(this, 'txt_absolute_time', 'txt_range_time');
            if (contactDesiredTimeVal != ''){
                $contactDesiredTimeEl.val(contactDesiredTimeVal);
            }
        });

        $radioRangeTimeSection.on('click', function(e){
            demandObj.enableTextBox(this, 'txt_range_time', 'txt_absolute_time');
        });

        $address1El.on('change', function (e) {

            var genreId = $genreEl.val(),
                address1 = $(this).val();

            demandObj.getBusinessTripAmount(genreId, address1);
            demandObj.getSelectionSystemList(genreId, address1);
            demandObj.existsAutoCommissionCorp();

            return false;
        });

        $genreEl.on('change', function() {

            var genreId = $(this).val();
            var address1 = $address1El.val();

            demandObj.getBusinessTripAmount(genreId, address1);
            demandObj.changeGenreSelection(genreId);
            demandObj.getSelectionSystemList(genreId, address1);
            demandObj.existsAutoCommissionCorp();

            return false;
        });

        $selectionSystemEl.on('change', function (e) {
            var genreId = $genreEl.val();
            var address1 = $address1El.val();
            demandObj.getBusinessTripAmount(genreId, address1);
            demandObj.selectionSystemAgencyInfoDis(false);
            detectCommissionMaxLimit.onChangeSelection($(this).val());

            return false;
        });

        $sendCommissionInfoBtn.on('click', function () {
            $sendCommissionInfoEl.val(1);

            return false;
        });

        $(document).on('change', '.now_date', function () {
            var url = $('.demand-detail').attr('data-url');
            var key = $(this).attr('data-key');
            var commission_note_sender_id = $('#commission_note_sender' + key + ' option:selected').val();
            if (commission_note_sender_id != "") {
                if ($(".commission_note_send_datetime" + key).val() == "") {
                    $.get( url, function( data ) {
                        $(".commission_note_send_datetime" + key).val(data);
                    });
                }
            } else {
                $(".commission_note_send_datetime" + key).val("");
            }

        });

        $sendIntroduceInfoBtn.on('click', function () {
            $sendCommissionInfoEl.val(1);

            return false;
        });

        $('input[name="demandInfo[is_contact_time_range_flg]]"]:radio').on('change', function () {
            demandObj.controlContactTimeEntryFields();
            return false;
        });
        $('input[name="data[VisitTime][0][is_visit_time_range_flg]"]:radio').on('change', function () {
            demandObj.controlVisitTimeEntryFields(0);
            return false;
        });
        $('input[name="data[VisitTime][1][is_visit_time_range_flg]"]:radio').on('change', function () {
            demandObj.controlVisitTimeEntryFields(1);
            return false;
        });
        $('input[name="data[VisitTime][2][is_visit_time_range_flg]"]:radio').on('change', function () {
            demandObj.controlVisitTimeEntryFields(2);
            return false;
        });

        $("input[id^='commit_flg']").on('change', function() {

            if($(this).is(':checked')){
                $('#corp_claim_flg' + id).prop('disabled', false);
                var maxLimit = 0;
                var selectionSystem = $selectionSystemEl.val();
                if(selectionSystem == '2' && selectionSystem == '3'){
                    maxLimit = $('#auction_selection_limit').val();
                }else{
                    maxLimit = $('#manual_selection_limit').val() || 0;
                }
                var commitedCount = 0;
                for(var i=0; i<30; i++){
                    var del_flg = $("input[id='del_flg"+i+"']").is(':checked');
                    var commit_flg = $("input[id='commit_flg"+i+"']").is(':checked');
                    if($("input[id='CommissionInfo"+i+"CorpId']").val()){
                        if(!del_flg && commit_flg){
                            commitedCount++;
                        }
                    }
                }
                if(maxLimit == commitedCount){
                    for(var i=0; i<30; i++){
                        var del_flg = $("input[id='del_flg"+i+"']").is(':checked');
                        var commit_flg = $("input[id='commit_flg"+i+"']").is(':checked');
                        if($("input[id='CommissionInfo"+i+"CorpId']").val()){
                            if(!del_flg && !commit_flg){
                                $("input[id='lost_flg"+i+"']").prop('checked', true);
                            }
                        }
                    }
                }
            }else{
                $('#corp_claim_flg' + id).prop('checked', false).prop('disabled', true);
            }
            return false;
        });

        $notSendEl.on('change', function() {
            var val = $(this).find(':checked').val();
            if(val == 1) {
                demandObj.controlSection($sendIntroduceInfoBtn, false);
                demandObj.controlSection($sendCommissionInfoBtn, false);
            } else {
                demandObj.controlSection($sendIntroduceInfoBtn, true);
                demandObj.controlSection($sendCommissionInfoBtn, true);
            }
            return false;
        });

        // get list button which delete attached file
        var lstDeleteButton = $('button[id^=delete_demand_file_]');

        // each element. If use onClick event
        $.map(lstDeleteButton, function (index, el) {
            $(el).on('click', function () {
                var id = $(this).val();
                if (id) {
                    // TODO: delete file
                }
            });
        });

        // get list button which clear attached file
        var lstFileClear = $('input[id^=file_clear_]');
        $.map(lstFileClear, function (index, el) {
            $(el).on('click', function () {
                var id = $(this).attr('data-id');
                if (id) {
                    $('#' + id).val('');
                }
            });
        });

    };

    var windowLoad = function () {
        var siteId = $siteEl.find('option:selected').val();
        demandObj.changeModeOrderFailReason();
        demandObj.crossSellDisabled(siteId);
        // demandObj.changeSiteId();
        // $categoryEl.trigger('change');
    };

    if(validateFail) {
        var genreId = $genreEl.val(), address1 = $address1El.val();
        demandObj.getSelectionSystemList(genreId, address1, selectionSystemValue);
    }
    
    return {
        init: init,
        copyDemand: copyDemand,
        crossDemand: crossDemand,
        windowLoad: windowLoad
    }

})();
