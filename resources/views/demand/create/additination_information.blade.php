<div class="form-table">
    <div class="row mx-0 border">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 py-2">
                <div class="form-row align-items-center">
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[nighttime_takeover]', 1, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[nighttime_takeover]']) !!}
                        <label class="custom-control-label" for='demandInfo[nighttime_takeover]'>@lang('demand_detail.night_work')</label>

                        @if ($errors->has('demandInfo.nighttime_takeover'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.nighttime_takeover')}}</label>
                        @endif
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[mail_demand]', 1, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[mail_demand]']) !!}
                        <label class="custom-control-label" for='demandInfo[mail_demand]'>@lang('demand_detail.mail_demand')</label>

                        @if ($errors->has('demandInfo.mail_demand'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.mail_demand')}}</label>
                        @endif
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[cross_sell_implement]', 1, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[cross_sell_implement]']) !!}
                        <label class="custom-control-label" for='demandInfo[cross_sell_implement]'>@lang('demand_detail.cross_cell_acquisition')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[cross_sell_call]', 1, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[cross_sell_call]']) !!}
                        <label class="custom-control-label" for='demandInfo[cross_sell_call]'>@lang('demand_detail.cross_sell_call')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">

                        {!! Form::checkbox('demandInfo[riro_kureka]', 1, false, ['class' => 'custom-control-input', 'disabled' => true, 'id' => 'demandInfo[riro_kureka]']) !!}
                        <label class="custom-control-label"
                               for='demandInfo[riro_kureka]'>@lang('demand_detail.cleka_case')</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[remand]', 1, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[remand]']) !!}
                        <label class="custom-control-label" for='demandInfo[remand]'>@lang('demand_detail.reversed_case')</label>

                        @if ($errors->has('demandInfo.remand'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.remand')}}</label>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 py-2">
                <div class="form-row align-items-center">
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[auction]', 1, false, ['class' => 'custom-control-input', 'disabled' => true]) !!}
                        <label class="custom-control-label" for='demandInfo["auction"]'>@lang('demand_detail.bidding_flow_case')</label>
                    </div>
                    {{--<div class="custom-control custom-checkbox mr-sm-4">--}}
                        {{--{!! Form::checkbox('demandInfo["do_auction"]', 2, false, ['disabled' => true, 'class' => 'custom-control-input']) !!}--}}
                        {{--<label class="custom-control-label"--}}
                               {{--for="customControlAutosizing">再入札</label>--}}
                    {{--</div>--}}


                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[low_accuracy]', 1, false, ['class'=> 'custom-control-input']) !!}
                        <label class="custom-control-label" for="customControlAutosizing">@lang('demand_detail.low_accuracy_projects')</label>

                        @if ($errors->has('demandInfo.low_accuracy'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.low_accuracy')}}</label>
                        @endif
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[corp_change]', 1, false, ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label" for="customControlAutosizing">@lang('demand_detail.change_of_merchant_store_request')</label>

                        @if ($errors->has('demandInfo.corp_change'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.corp_change')}}</label>
                        @endif
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[sms_reorder]', 1, false, ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label" for="customControlAutosizing">@lang('demand_detail.re_order_sms')</label>

                        @if ($errors->has('demandInfo.sms_reorder'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.sms_reorder')}}</label>
                        @endif
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-4">
                        {!! Form::checkbox('demandInfo[calendar_flg]', 1, false, ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label" for="customControlAutosizing">@lang('demand_detail.calendar_flg')</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.special_measures')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[special_measures]', $specialMeasureDropDownList, '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.guide_pet_tombstones')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-group d-flex flex-column flex-lg-row  align-items-lg-center mb-lg-0 group-radio-pet-tomb">
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[pet_tombstone_demand]', 1, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[pet_tombstone_demand_1]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[pet_tombstone_demand_1]">@lang('demand_detail.guided')</label>
                    </div>
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[pet_tombstone_demand]', 2, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[pet_tombstone_demand_2]']) !!}

                        <label class="custom-control-label"
                               for="demandInfo[pet_tombstone_demand_2]">@lang('demand_detail.not_covered')</label>
                    </div>
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[pet_tombstone_demand]', 3, false, ['class' => 'custom-control-input', 'id' => 'demandInfo[pet_tombstone_demand_3]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[pet_tombstone_demand_3]">@lang('demand_detail.release')</label>
                    </div>
                    <button class="btn btn--gradient-default btn--w-normal ml-lg-2 mb-2 mb-lg-0" id="reset-radio">@lang('demand_detail.release')</button>
                    <p class="text-muted mb-2 mb-lg-0 ml-lg-2">@lang('demand_detail.essential_items')</p>

                    @if (Session::has('demand_errors.pet_tombstone_demand'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.pet_tombstone_demand')}}</label>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.order_number')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between flex-column mb-lg-0">
                    {!! Form::text('demandInfo[order_no_marriage]', '', ['class' => 'form-control is-required', 'data-rules' => 'valid-number']) !!}

                    @if (Session::has('demand_errors.check_order_no_marriage_not_empty'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_order_no_marriage_not_empty')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.st_claim')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[st_claim]', $stClaimDropDownList, '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
