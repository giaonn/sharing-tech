var demandValidationModule = (function() {
    var $topRegisButton = $('#top_register'),
        $bottomRegisButton = $('#bottom_regist'),
        $sendCommissionButton = $('#send_commission_info_btn'),
        $demandCorrespondContent = $('#demandCorrespondContent');

    var messages = {
        notEmpty: "必須入力です。",
        validNumber: "半角数字で入力してください。",
        validEmail: "半角、Eメール形式で入力してください。",
        validDate: "日付形式で入力してください。",
        validUrl: "URL形式で入力してください"
    };

    var ruleNames = {
        notEmpty: 'not-empty',
        validNumber: 'valid-number',
        validEmail: 'valid-email',
        validDate: 'valid-date',
        validUrl: 'valid-url'
    };

    var getRequiredFields = function (form) {
        return form.find('.is-required');
    };

    var demandValidate = {
        generateMessageSection: function (msg) {
            var html = '';
            html += '<label class="invalid-feedback d-block">';
            html += msg;
            html += '</label>';
            return html;
        },
        addInvalidClass: function (el) {
            if (!el.hasClass('is-invalid'))
                el.addClass('is-invalid');
        },
        removeInvalidClass: function (el) {
            if (el.hasClass('is-invalid'))
                el.removeClass('is-invalid');
        },
        getMessageSection: function (el) {
            return el.find('label.invalid-feedback.d-block');
        },
        renderMessageSection: function (el, msg) {
            var section = demandValidate.getMessageSection(el);
            if (!section.length)
                el.append(demandValidate.generateMessageSection(msg));
            else
                $(section).html(msg);
        },
        removeMessageSection: function (el) {
            var section = demandValidate.getMessageSection(el);
            if (section.length) {
                $(section).remove();
            }
        },
        getRules: function (el) {
            var rules = [];
            var strRules = el.data('rules');
            if (typeof strRules != 'undefined' && strRules.indexOf(',') != -1)
                rules = strRules.split(',');
            else
                rules[0] = strRules;

            return rules;
        },
        convertToHalfSize: function (val) {
            return val.replace(/[\uFF10-\uFF19]/g, function(m) {
                return String.fromCharCode(m.charCodeAt(0) - 0xfee0);
            });
        },
        checkNumber: function (val) {
            var telNumber = demandValidate.convertToHalfSize(val);
            return (/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(telNumber));
        },
        checkEmail: function (val) {
            return (/^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(val));
        },
        checkDate: function (val) {
            var cvrtDate = new Date(val);
            return !isNaN(cvrtDate);
        },
        checkUrl: function (val) {
            var pattern = new RegExp('^(https?:\/\/)?'+ // protocol
                '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
                '((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
                '(\:\d+)?(\/[-a-z\d%_.~+]*)*'+ // port and path
                '(\?[;&a-z\d%_.~+=-]*)?'+ // query string
                '(\#[-a-z\d_]*)?$','i'); // fragment locater
            return pattern.test(val);
        },
        notEmpty: function (el) {
            var val = el.val();
            var parent = el.parent();
            if (val === '') {
                demandValidate.addInvalidClass(el);
                demandValidate.renderMessageSection($(parent), messages.notEmpty);
            } else {
                demandValidate.removeInvalidClass(el);
                demandValidate.removeMessageSection($(parent));
            }
        },
        validNumber: function (el) {
            var val = el.val();
            var parent = el.parent();
            if (val != '') {
                if (!demandValidate.checkNumber(val)) {
                    demandValidate.addInvalidClass(el);
                    demandValidate.renderMessageSection($(parent), messages.validNumber);
                } else {
                    demandValidate.removeInvalidClass(el);
                    demandValidate.removeMessageSection($(parent));
                }
            }
        },
        validEmail: function (el) {
            var val = el.val();
            var parent = el.parent();
            if (val != '') {
                if (!demandValidate.checkEmail(val)) {
                    demandValidate.addInvalidClass(el);
                    demandValidate.renderMessageSection($(parent), messages.validEmail);
                } else {
                    demandValidate.removeInvalidClass(el);
                    demandValidate.removeMessageSection($(parent));
                }
            }
        },
        validDate: function(el) {
            var val = el.val();
            var parent = el.parent();

            if(val != '') {
                if (!demandValidate.checkDate(val)) {
                    demandValidate.addInvalidClass(el);
                    demandValidate.renderMessageSection($(parent), messages.validDate);
                } else {
                    demandValidate.removeInvalidClass(el);
                    demandValidate.removeMessageSection($(parent));
                }
            } else {
                demandValidate.removeInvalidClass(el);
                demandValidate.removeMessageSection($(parent));
            }
        },
        validUrl: function (el) {
            var val = el.val();
            var parent = el.parent();

            if(val != '') {
                if (!demandValidate.checkUrl(val)) {
                    demandValidate.addInvalidClass(el);
                    demandValidate.renderMessageSection($(parent), messages.validDate);
                } else {
                    demandValidate.removeInvalidClass(el);
                    demandValidate.removeMessageSection($(parent));
                }
            } else {
                demandValidate.removeInvalidClass(el);
                demandValidate.removeMessageSection($(parent));
            }
        },
        checkContentOfCorrespond: function () {
            var val = $demandCorrespondContent.val();
            var parent = $demandCorrespondContent.parent();

            if (val === '') {
                demandValidate.addInvalidClass($demandCorrespondContent);
                demandValidate.renderMessageSection($(parent), messages.notEmpty);
                return false;
            } else {
                return true;
            }
        }
    };

    var validate = function (form) {
        var requireFields = getRequiredFields($(form));

        $.each(requireFields, function (i, el) {
            $(el).on('change', function (e) {
                var rules = demandValidate.getRules($(el));

                if (jQuery.inArray(ruleNames.notEmpty, rules) !== -1) {
                    demandValidate.notEmpty($(el));
                }

                if (jQuery.inArray(ruleNames.validNumber, rules) !== -1) {
                    demandValidate.validNumber($(el));
                }

                if (jQuery.inArray(ruleNames.validEmail, rules) !== -1) {
                    demandValidate.validEmail($(el));
                }

                if (jQuery.inArray(ruleNames.validDate, rules) !== -1) {
                    demandValidate.validDate($(el));
                }

                if (jQuery.inArray(ruleNames.validUrl, rules) !== -1) {
                    demandValidate.validUrl($(el));
                }

                return false;
            }).on('blur', function (e) {
                var rules = demandValidate.getRules($(el));

                if (jQuery.inArray(ruleNames.notEmpty, rules) !== -1) {
                    demandValidate.notEmpty($(el));
                }

                if (jQuery.inArray(ruleNames.validNumber, rules) !== -1) {
                    demandValidate.validNumber($(el));
                }

                if (jQuery.inArray(ruleNames.validEmail, rules) !== -1) {
                    demandValidate.validEmail($(el));
                }

                if (jQuery.inArray(ruleNames.validDate, rules) !== -1) {
                    demandValidate.validDate($(el));
                }

                if (jQuery.inArray(ruleNames.validUrl, rules) !== -1) {
                    demandValidate.validUrl($(el));
                }

                return false;
            });
        });

        $topRegisButton.on('click', function () {
            return demandValidate.checkContentOfCorrespond();
        });
        $bottomRegisButton.on('click', function () {
            return demandValidate.checkContentOfCorrespond();
        });
        $sendCommissionButton.on('click', function () {
            return demandValidate.checkContentOfCorrespond();
        });
    };

    return {
        validate: validate
    }
})();
