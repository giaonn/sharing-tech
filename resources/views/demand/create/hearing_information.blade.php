@php
    $timeInvalid[1] = false;
    $timeTxt[1] = old('visitTime')[1]['visit_time'];
    $timeTxt[2] = old('visitTime')[2]['visit_time'];
    $timeTxt[3] = old('visitTime')[3]['visit_time'];
    $timeTxt[4] = '';
    $orderFailDate = old('demandInfo')['order_fail_date'];
    $orderFailDateInvalid = false;
    $invalidFlowDate = false;
    $invalidDemandDesertTime = '';
    $timeInvalid[2] = false;
    $timeInvalid[3] = false;
    $timeInvalid[4] = false;
    if (old('visitTime')) {
        if (strtotime(old('visitTime')[1]['visit_time']) === false ) {
            $timeTxt[1] = old('visitTime')[1]['visit_time'];
        }
        if (strtotime(old('visitTime')[2]['visit_time']) === false && !empty(old('visitTime')[2]['visit_time'])) {
            $timeInvalid[2] = true;
        }
        if (strtotime(old('visitTime')[3]['visit_time']) === false && !empty(old('visitTime')[3]['visit_time'])) {
            $timeInvalid[3] = true;
        }
    }
    if (old('demandInfo')['order_fail_date'] && strtotime(old('demandInfo')['order_fail_date']) === false ) {
        $orderFailDateInvalid = true;
        $orderFailDate = '';
    }
    if (old('demandInfo')['follow_date'] && strtotime(old('demandInfo')['follow_date']) === false ) {
        $invalidFlowDate = true;
    }
    if (old('demandInfo')['contact_desired_time'] && !empty(old('demandInfo')['contact_desired_time'])) {
        $invalidDemandDesertTime = old('demandInfo')['contact_desired_time'];
    }
@endphp
<div class="form-table">
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.customer_name')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text("demandInfo[customer_name]", '', ['class' => 'form-control is-required', 'data-rules' => 'not-empty', 'maxlength' => 50]) !!}

                    @if ($errors->has('demandInfo.customer_name'))
                    <label class="invalid-feedback d-block">{{ $errors->first('demandInfo.customer_name') }}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.corporate_name')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text("demandInfo[customer_corp_name]", '', ['class' => 'form-control', 'id' => 'customer_corp_name']) !!}

                    @if ($errors->has('demandInfo.customer_corp_name'))
                    <label class="invalid-feedback d-block">{{ $errors->first('demandInfo.customer_corp_name') }}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.customer_tel')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between flex-column mb-lg-0">
                    @php
                        $tel = ($ctiDemand) ? $ctiDemand['customer_tel'] : '';
                        if(!empty(old('demandInfo')['customer_tel'])) {
                            $tel = old('demandInfo')['customer_tel'];
                            if (!is_numeric($tel)) {
                                $tel = '9999999999';
                            }
                        }
                    @endphp
                    <input class="form-control w-50 is-required" data-rules="not-empty,valid-number" maxlength="11" name="demandInfo[customer_tel]" type="text" value="{{ $tel }}">

                    @if(!empty(old('demandInfo')['customer_tel']))
                        <a href="callto:{{ $tel }}"  class="text--orange">{{ $tel }}</a>
                    @endif
                    @if (Session::has('demand_errors.check_customer_tel'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_customer_tel')}}</label>
                    @elseif ($errors->has('demandInfo.customer_tel'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.customer_tel')}}</label>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.contact_first')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex flex-column mb-lg-0">
                    {!! Form::text("demandInfo[tel1]", '', ['class' => 'form-control w-50 is-required', 'data-rules' => 'not-empty,valid-number', 'maxlength' => 11]) !!}
                    @if(!empty(old('demandInfo')['tel1']))
                        @if(!is_numeric(old('demandInfo')['tel1']))
                            <label class="invalid-feedback d-block">{{ trans('demand.validation_error.numeric_error') }}</label>
                        @endif
                        <a href="callto:{{ old('demandInfo')['tel1'] }}"  class="text--orange">{{ old('demandInfo')['tel1'] }}</a>
                    @endif
                    @if (Session::has('demand_errors.check_tel1'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_tel1')}}</label>
                    @elseif ($errors->has('demandInfo.tel1'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.tel1')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.contact_second')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                    {!! Form::text("demandInfo[tel2]", !empty(old('demandInfo')['tel2']) ? old('demandInfo')['tel2'] : '' , ['class' => 'form-control w-50 is-required', 'data-rules' => 'valid-number', 'maxlength' => 11]) !!}

                    @if ($errors->has('demandInfo.tel2'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.tel2')}}</label>
                    @endif
                    <a href="callto:{{ old('demandInfo')['tel2'] }}" class="text--orange ml-2 w-50">
                        @if(!empty(old('demandInfo')['tel2']))
                            {{ old('demandInfo')['tel2'] }}
                        @endif
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.customer_email_address')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between flex-column align-items-center mb-lg-0">
                    {!! Form::text("demandInfo[customer_mailaddress]", '', ['class' => 'form-control is-required', 'data-rules' => 'valid-email' ]) !!}

                    @if ($errors->has('demandInfo.customer_mailaddress'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.customer_mailaddress')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.postal_code')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-group d-flex flex-column flex-lg-row mb-lg-0">
                    {!! Form::text("demandInfo[postcode]", '', ['class' => 'form-control w-auto mb-2 mb-lg-0 is-required', 'id' => 'postcode', 'data-rules' => 'valid-number', 'data-error-container' => '#err-postcode', 'size' => 7, 'maxlength' => 7]) !!}
                    <button class="btn btn--gradient-default ml-lg-2 mb-2 mb-lg-0" id="search-address-by-zip">@lang('demand_detail.address_search')</button>
                    <p class="text-muted mb-2 mb-lg-0 ml-lg-2 py-2">@lang('demand_detail.zip_code_msg')</p>
                </div>
                <div id="err-postcode">
                    @if ($errors->has('demandInfo.postcode'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.postcode')}}</label>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.prefectures')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around flex-column mb-lg-0">
                    {!! Form::select('demandInfo[address1]', $prefectureDiv, '', ['class' => 'form-control is-required', 'id' => 'address1', 'data-rules' => 'not-empty', 'maxlength' => 10]) !!}

                    @if ($errors->has('demandInfo.address1'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.address1')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.municipality')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text('demandInfo[address2]', '', ['class' => 'form-control is-required', 'id' => 'address2', 'data-rules' => 'not-empty', 'maxlength' => 20]) !!}

                    @if ($errors->has('demandInfo.address2'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.address2')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.later_address')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::text('demandInfo[address3]', '', ['class' => 'form-control', 'id' => 'address3', 'maxlength' => 100]) !!}

                    @if ($errors->has('demandInfo.address3'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.address3')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.building_type')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around flex-column align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[construction_class]', $mItemsDropDownList, '', ['class' => 'form-control is-required', 'data-rules' => 'not-empty']) !!}

                    @if ($errors->has('demandInfo.construction_class'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.construction_class')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border normal_info" id="selection-system-div" style="display: @if(!old('visitTime')) none @else block @endif;">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.selection_method')</strong>
                    </label>
                    <span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[selection_system]', $selectionSystemList, '', ['class' => 'form-control', 'id' => 'selection_system']) !!}
                    {!! Form::hidden('demandInfo[selection_system_before]', '', ['id' => 'selection_system_before']) !!}
                </div>

                @if (Session::has('demand_errors.check_selection_system'))
                <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_selection_system')}}</label>
                @elseif (Session::has('demand_errors.check_auction_setting_genre'))
                <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_auction_setting_genre')}}</label>
                @elseif ($errors->has('demandInfo.selection_system'))
                <label class="invalid-feedback d-block">{{$errors->first('demandInfo.selection_system')}}</label>
                @endif
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6 px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.content_of_consultation')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group w-100 mb-lg-0">
                    {!! Form::textarea('demandInfo[contents]', '', ['class' => 'form-control', 'rows' => 10, 'id' => 'demand-content']) !!}

                    @if (Session::has('demand_errors.check_contents_string'))
                    <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contents_string')}}</label>
                    @elseif ($errors->has('demandInfo.contents'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contents')}}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 row m-0 p-0">
            <div class="col-12 col-lg-6  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.sharing')
                            <br>
                            @lang('demand_detail.technology_side_notes')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-6 py-2">
                <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                    <div id="attention" class="clearfix"></div>
                    {{--<textarea class="form-control-plaintext" id="attention" disabled></textarea>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.contact_deadline')</strong>
                    </label>
                    <button class="btn btn--gradient-default d-block" id="plus-15-minus">@lang('demand_detail.15_minutes')</button>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2 visitTimeDiv">
                <div class="form-row align-items-center mb-2" data-group-required="true">
                    <div class="custom-control custom-radio mr-2">
                        {!! Form::radio('demandInfo[is_contact_time_range_flg]', 0, true, ['class' => 'custom-control-input range-time-0 absolute_time','id' => 'demandInfo[is_contact_time_range_flg_0]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[is_contact_time_range_flg_0]">@lang('demand_detail.specify_time')</label>
                    </div>
                    <div class="custom-control custom-radio">
                        {!! Form::radio('demandInfo[is_contact_time_range_flg]', 1, false, ['class' => 'custom-control-input range-time-1 range_time', 'id' => 'demandInfo[is_contact_time_range_flg_1]']) !!}
                        <label class="custom-control-label"
                               for="demandInfo[is_contact_time_range_flg_1]">@lang('demand_detail.time_adjustment_required')</label>
                    </div>
                </div>
                <div class="form-row align-items-center mb-2">
                    <div class="col-12 col-lg-3 mb-2">
                        {!! Form::hidden('demandInfo[contact_desired_time]', '') !!}
                        <input class="form-control datetimepicker txt_absolute_time is-required"
                               id="contact_desired_time" data-rules="valid-date"
                               name="demandInfo[contact_desired_time]" type="text"
                               value="{{ $invalidDemandDesertTime }}">
                        {!! Form::hidden('demandInfo[contact_desired_time_before]', '') !!}
                        @if (Session::has('demand_errors.check_contact_desired_time2'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time2')}}</label>
                        @elseif (Session::has('demand_errors.check_contact_desired_time3'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time3')}}</label>
                        @elseif ($errors->has('demandInfo.contact_desired_time'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contact_desired_time')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-row align-items-center mb-2">
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::hidden('demandInfo[contact_desired_time_from]', '') !!}
                        {!! Form::text('demandInfo[contact_desired_time_from]', '',
                        ['class' => 'form-control datetimepicker txt_range_time is-required', 'disabled' => true, 'data-rules' => 'valid-date']) !!}

                        @if (Session::has('demand_errors.check_contact_desired_time4'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time4')}}</label>
                        @elseif (Session::has('demand_errors.check_require_to'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_require_to')}}</label>
                        @elseif ($errors->has('demandInfo.contact_desired_time_from'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contact_desired_time_from')}}</label>
                        @endif
                    </div>
                    <span class="col-auto mb-2 text-center">〜</span>
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::hidden('demandInfo[contact_desired_time_to]', '') !!}
                        {!! Form::text('demandInfo[contact_desired_time_to]', '', ['class' => 'form-control datetimepicker txt_range_time is-required', 'disabled' => true, 'data-rules' => 'valid-date']) !!}

                        @if (Session::has('demand_errors.check_contact_desired_time5'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time5')}}</label>
                        @elseif (Session::has('demand_errors.check_contact_desired_time6'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.check_contact_desired_time6')}}</label>
                        @elseif ($errors->has('demandInfo.contact_desired_time_to'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.contact_desired_time_to')}}</label>
                        @endif
                    </div>
                </div>
                <p class="text--info">@lang('demand_detail.visit_time_text')</p>
            </div>
        </div>
    </div>

    @php $label = [1 => '①', 2 => '②', 3 => '③'] @endphp
    @for($i = 1; $i < 4; $i++)
        {!! Form::hidden('visitTime[' . $i . '][visit_time_before]', '') !!}
        {!! Form::hidden('visitTime[' . $i . '][commit_flg]', '') !!}
        {!! Form::hidden('visitTime[' . $i . '][id]', '') !!}
        {!! Form::hidden('visitTime[' . $i . '][visit_time_before]', '') !!}
        <div class="row mx-0 border normal_info" style="display: @if(!old('visitTime')) none @else block @endif;">
            <div class="col-12 row m-0 p-0 ">
                <div class="col-12 col-lg-3  px-0">
                    <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                        <label class="m-0">
                            <strong>@lang('demand_detail.desired_visit_date'){{ $label[$i] }}</strong>
                        </label>
                        {{--<span class="badge badge-warning float-lg-right">{{ __('common.have_to') }}</span>--}}
                    </div>
                </div>
                <div class="col-12 col-lg-9 py-2 visitTimeDiv">
                    <div class="form-row align-items-center mb-2">
                        <div class="custom-control custom-radio mr-2">
                            {!! Form::hidden('visitTime['. $i .'][is_visit_time_range_flg]', 0) !!}
                            {!! Form::radio('visitTime['. $i .'][is_visit_time_range_flg]', 0, true, ['class' => 'custom-control-input absolute_time', 'id' => 'visitTime['. $i .'][is_visit_time_range_flg_0]']) !!}
                            <label class="custom-control-label"
                                   for="{{'visitTime['. $i .'][is_visit_time_range_flg_0]'}}">@lang('demand_detail.specify_time')</label>
                        </div>

                        <div class="custom-control custom-radio">
                            {!! Form::radio('visitTime[' . $i . '][is_visit_time_range_flg]', 1, false, ['class' => 'custom-control-input range_time', 'id' => 'visitTime['. $i .'][is_visit_time_range_flg_1]']) !!}
                            <label class="custom-control-label"
                                   for="{{ 'visitTime[' . $i . '][is_visit_time_range_flg_1]' }}">@lang('demand_detail.time_adjustment_required')</label>
                        </div>
                    </div>
                    <div class="form-row align-items-center mb-2">
                        <div class="col-12 col-lg-3 mb-2">
                            {!! Form::hidden('visitTime[' . $i . '][visit_time]', '') !!}
                            {!! Form::text('visitTime['.$i.'][visit_time]', $timeTxt[$i], ['class' => 'form-control datetimepicker txt_absolute_time is-required']) !!}
                            {{--<input class="form-control datetimepicker txt_absolute_time hasDatepicker" name="visitTime[{{ $i }}][visit_time]" type="text" value="{{ $timeTxt[$i] }}" />--}}
                            @if($timeInvalid[$i])
                                <label class="invalid-feedback d-block">{{ trans('demand.validation_error.date_error') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="form-row align-items-center mb-2">
                        <div class="col-5 col-lg-3 mb-2">
                            {!! Form::hidden('visitTime[' . $i . '][visit_time_from]', '') !!}
                            {!! Form::text('visitTime[' . $i . '][visit_time_from]', '', ['class' => 'form-control datetimepicker txt_range_time is-required', 'disabled' => true, 'date-rules' => 'valid-date']) !!}
                        </div>
                        <span class="col-auto mb-2 text-center">〜</span>
                        <div class="col-5 col-lg-3 mb-2">
                            {!! Form::hidden('visitTime[' . $i . '][visit_time_to]', '') !!}
                            {!! Form::text('visitTime[' . $i . '][visit_time_to]', '', ['class' => 'form-control datetimepicker txt_range_time is-required', 'disabled' => true, 'date-rules' => 'valid-date']) !!}
                        </div>
                        <span class="col-auto mb-2">@lang('demand_detail.visit_adjustment') </span>
                        <div class="col-12 col-lg-3 mb-2">
                            {!! Form::hidden('visitTime[' . $i . '][visit_adjust_time]', '') !!}
                            {!! Form::text('visitTime[' . $i . '][visit_adjust_time]', '', ['class' => 'form-control datetimepicker txt_range_time is-required', 'disabled' => true, 'date-rules' => 'valid-date']) !!}
                        </div>
                    </div>
                    @if($i != 1) <p class="text--info">@lang('demand_detail.visit_time_complete')</p>@endif
                </div>
            </div>
        </div>
    @endfor

    <div class="row mx-0 border normal_info" style="{{ old('demandInfo')['category_id'] ? 'display: block;' : 'display: none;' }}">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.business_trip_cost')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-row align-items-center mb-2">
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::text('demandInfo[business_trip_amount]', '', ['class' => 'form-control', 'id' => 'business-trip-mount']) !!}
                    </div>
                    <span class="col-auto mb-2 text-center"> @lang('demand_detail.circle') </span>

                    @if ($errors->has('demandInfo.business_trip_amount'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.business_trip_amount')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border normal_info" style="{{ old('demandInfo')['category_id'] ? 'display: block;' : 'display: none;' }}">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.service_price_offer')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-9 py-2">
                <div class="form-row align-items-center mb-2">
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::text('demandInfo[cost_from]', '', ['class' => 'form-control']) !!}

                        @if ($errors->has('demandInfo.cost_from'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.cost_from')}}</label>
                        @endif
                    </div>
                    <span class="col-auto mb-2 text-center"> @lang('demand_detail.circle') </span>
                    <span class="col-auto mb-2 text-center"> 〜</span>
                    <div class="col-5 col-lg-3 mb-2">
                        {!! Form::text('demandInfo[cost_to]', '', ['class' => 'form-control']) !!}

                        @if ($errors->has('demandInfo.cost_to'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandInfo.cost_to')}}</label>
                        @endif
                    </div>
                    <span class="col-auto mb-2 text-center"> @lang('demand_detail.circle')</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.follow_up_date')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[follow_date]', '', ['class' => 'form-control datetimepicker is-required', 'data-rules' => 'valid-date']) !!}
                    @if (Session::has('demand_errors.follow_date'))
                        <label class="invalid-feedback d-block">{{Session::get('demand_errors.follow_date')}}</label>
                    @elseif($invalidFlowDate == true)
                        <label class="invalid-feedback d-block">{{ trans('demand.validation_error.date_error') }}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.reception_date_time')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::text('demandInfo[receive_datetime]', dateTimeNowFormat(), ['class' => 'form-control datetimepicker is-required', 'data-rules' => 'valid-date']) !!}

                    @if ($errors->has('demandInfo.receive_datetime'))
                    <label class="invalid-feedback d-block">{{$errors->first('demandInfo.receive_datetime')}}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border ">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.priority')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! Form::select('demandInfo[priority]', $priorityDropDownList, '', ['class' => 'form-control', 'disabled' => Auth::user()->auth != 'system']) !!}
                    {!! Form::hidden('demandInfo[priority_before]', '') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border normal_info" style="display: none;">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.follow_up_date_time')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="form-group d-flex justify-content-between align-items-center mb-lg-0">
                    {!! isset($demand) && isset($demand->follow_tel_date_format) ? $demand->follow_tel_date_format : '' !!}
                    {!! Form::hidden('demandInfo[follow_tel_date]', '') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 border normal_info" style="display: none">
        <div class="col-12 row m-0 p-0">
            <div class="col-12 col-lg-3  px-0">
                <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                    <label class="m-0">
                        <strong>@lang('demand_detail.followed')</strong>
                    </label>
                </div>
            </div>
            <div class="col-12 col-lg-3 py-2">
                <div class="custom-control custom-checkbox mr-sm-2">
                    {!! Form::checkbox('demandInfo[follow]', 1, false, ['class' => 'custom-control-input']) !!}
                    <label class="custom-control-label" for="demandInfo[follow]"></label>
                </div>
            </div>
        </div>
    </div>

</div>
