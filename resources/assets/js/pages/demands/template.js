let renderChainHtml = function(values, tag, commissionIndex, hiddenField = 'label_0') {
    let chainHtml = '';
    $.each(values, (index, value) => {
        chainHtml += `<${tag}>
            ${value}
            <input type="hidden" value=${value} name="commissionInfo[${commissionIndex}][mCorpNewYear][${hiddenField}${index + 1}]/>"
        </${tag}>`;
    });
    return chainHtml;
};

let getFeeUnit = function(){
    return 1;
}
let getCommissionString = function(commission, index){
    let commissionString = '取次時手数料率';
    if(commission.order_fee_val !== '' && commission.order_fee_unit !== ''){
        if (commission.order_fee_unit === '0') {
            commissionString = '取次先手数料';
        }
    }
    return `${commissionString}'<input type="hidden" name="commissionInfo[${index}][commission_string]" value="${commissionString}" />`;
};

let getCommissionValue = function(commission, index){
    let commissionInput = '';
    let result = '';
    if (commission.order_fee !== '') {
        commissionInput = `<input type="hidden" value="${commission.order_fee}" name="commissionInfo[${index}][corp_fee]" /> ${commission.order_fee} 円`;
        commissionInput += `<input type="hidden" value="${commission.order_fee + '円'}" name="commissionInfo[${index}][commission_fee_dis]" />`;
    }
    if(commission.order_fee_val !== '' && commission.order_fee_unit === 1){
        commissionInput = `<input type="hidden" name="commissionInfo[${index}][commission_fee_rate]" value=${commission.order_fee_val || 0} /> 
                            ${commission.order_fee_val} %`;
        commissionInput += `<input type="hidden" value="${commission.order_fee_val + '%'}" name="commissionInfo[${index}][commission_fee_dis]" />`;
    }

    if(commission.corp_id !== ''){
        result = commission.corp_commission_type_disp + ' ' + commissionInput;
    }
    result += `<input type="hidden" value=${commission.corp_commission_type_disp} name="commissionInfo[${index}][corp_commission_type_disp]"`;
    return `${result}<input type="hidden" name="commissionInfo[${index}][complete_date]" /> <input type="hidden" name="commissionInfo[${index}][commission_status]"/>`;
};

var loadModal = function(url_data, index) {
    var initProgress = function () {
        return new progressCommon();
    };
    var progress = initProgress();
    var modalMCorpPopup = $('#m-corp-detail');
    var modalMCorpPopupDetail = $('#title-m-corp-detail');
    $.ajax({
        type: "GET",
        url: url_data,
        xhr: function () {
            return progress.createXHR();
        },
        beforeSend: function (xhr) {
            progress.controlProgress(true);
        },
        complete: function () {
            progress.controlProgress(false);
        },
        success: function (data) {
            modalMCorpPopup.children().children().find('.modal-body').html(data);
            modalMCorpPopupDetail.html(`取次先${index}情報`);
            modalMCorpPopup.modal('show');
        },
        error: function () {
            console.log("Error!");
        }
    });
};

function loadMCorpDetail(element, index) {
    var urlData = $(element).data('url_data');
    loadModal(urlData, index);
};

function checkCommissionFlg(element) {
    var id = $(element).data('id');
    if ($(element).is(':checked')) {
        $('#corp_claim_flg' + id).prop('disabled', false);
        var maxLimit = 0;
        var selectionSystem = $('#selection_system').val();
        if (selectionSystem == '2' && selectionSystem == '3') {
            maxLimit = $('#auction_selection_limit').val();
        } else {
            maxLimit = $('#manual_selection_limit').val() || 0;
        }

        var count = 0;
        for (var i = 0; i < 30; i++) {
            var delFlg = $("input[id='del_flg" + i + "']").is(':checked');
            var commitFlg = $("input[id='commit_flg" + i + "']").is(':checked');
            if ($("input[id='CommissionInfo" + i + "CorpId']").val()) {
                if (!delFlg && commitFlg) {
                    count++;
                }
            }
        }

        if (maxLimit == count) {
            for (var i = 0; i < 30; i++) {
                var del_flg = $("input[id='del_flg" + i + "']").is(':checked');
                var commit_flg = $("input[id='commit_flg" + i + "']").is(':checked');
                if ($("input[id='CommissionInfo" + i + "CorpId']").val()) {
                    if (!del_flg && !commit_flg) {
                        $("input[id='lost_flg" + i + "']").prop('checked', true);
                    }
                }
            }
        }
    } else {
        $('#corp_claim_flg' + id).prop('checked', false).prop('disabled', true);
    }
}


function getMCorpDetailUrl(commission){
    var commissionInput = '';
    if (commission.order_fee !== '') {
        commissionInput = `${commission.order_fee} 円`;
    }
    if(commission.order_fee_val !== '' && commission.order_fee_unit === 1){
        commissionInput = `${commission.order_fee_val} %`;
    }
    return  `${commission.corp_commission_type_disp}${commissionInput}`;
}


$('.date').on('click', function(){
    $( '.date' ).datetimepicker({
        controlType: 'select',
        oneLine: true,
        timeText: '時間',
        hourText: '時',
        minuteText: '分',
        currentText: '現時刻',
        closeText: '閉じる',
        locale: 'ja',
        onClose: function () {
            $(this).trigger('blur');
        }
    });
});
let template = function (index, commission = {}) {
    // let currentIndex = index - 1 ;
    let currentIndex = $('.corp_id').length;

    let holidayLabels = renderChainHtml(commission.long_vacations_items, 'th', currentIndex);
    let holidayValues = renderChainHtml(commission.long_vacations, 'td', currentIndex , 'status_0');
    let commissionString = getCommissionString(commission, currentIndex);
    let commissionValue = getCommissionValue(commission, currentIndex);
    let categoryId = $('#category_id').val();
    return `<div class="form-table commission-table">
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 bg-primary-lighter p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--primary p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>取次先<span class="max-index-currentIndex">${currentIndex + 1}</span></strong>
                            </label>
                            <button onClick="loadMCorpDetail(this, ${currentIndex + 1});return false;"
                            data-url_data="/ajax/load_m_corp/${commission.corp_id}/${typeof categoryId !== "undefined" ? categoryId : ''}?fee_data=${getMCorpDetailUrl(commission)}"
                                data-toggle="modal" type="button" class="btn btn-sm btn--gradient-default m-corps-detail">
                                情報参照
                            </button>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-3">
                        <div class="form-group w-100 mb-lg-0">
                            <input class="form-control" id="corp_name${currentIndex}" name="commissionInfo[${currentIndex}][mCorp][corp_name]" type="text" value="${commission.corp_name}">
                        </div>
                    </div>
                </div>

            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>取次用ダイヤル</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="h-100 d-flex justify-content-start align-items-center mb-lg-0">
                            <a href="callto:${commission.commission_dial}" class="text--orange ml-2 w-50 text--underline">${commission.commission_dial} </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0" for='del_flg${currentIndex}'>
                                <strong>削除</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="hidden" value=0 name="commissionInfo[${currentIndex}][del_flg]" />
                            <input class="custom-control-input" id="del_flag${currentIndex}" name="commissionInfo[${currentIndex}][del_flg]" type="checkbox" value=1 />
                            <label class="custom-control-label" for="commissionInfo[${currentIndex}][del_flg]"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>選定者</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                            <select class="form-control appointers" name="commissionInfo[${currentIndex}][appointers]">
                                <option value="">--なし--</option>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0" for='first_commission${currentIndex}'>
                                <strong>初取次チェック</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="hidden" name="commissionInfo[${currentIndex}][first_commission]" value=0 />
                            <input class="custom-control-input" id="first_commission${currentIndex}" name="commissionInfo[${currentIndex}][first_commission]" type="checkbox" value=1 >
                            <label class="custom-control-label" for="commissionInfo[${currentIndex}][first_commission]"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>取次票送信者</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex justify-content-around  mb-lg-0">
                            <select class="form-control commission_note_sender now_date" id="commission_note_sender${currentIndex}" data-key="${currentIndex}" name="commissionInfo[${currentIndex}][commission_note_sender]">
                                <option value="">--なし--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0" for='unit_price_calc_exclude${currentIndex}'>
                                <strong>取次単価対象外</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="hidden" value="${commission.corp_id}" name="commissionInfo[${currentIndex}][corp_id]" id="CommissionInfo${currentIndex}CorpId" class="corp_id"/>
                            <input type="hidden" value=0 name="commissionInfo[${currentIndex}][unit_price_calc_exclude]"/>
                            <input class="custom-control-input" id="unit_price_calc_exclude${currentIndex}" name="commissionInfo[${currentIndex}][unit_price_calc_exclude]" type="checkbox" value="1" >
                            <label class="custom-control-label" for="commissionInfo[${currentIndex}][unit_price_calc_exclude]"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>取次票送信日時</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                            <input class="form-control date commission_note_send_datetime${currentIndex}" name="commissionInfo[${currentIndex}][commission_note_send_datetime]" type="text" value="" id="dp1525500912372">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0" for='commit_flg${currentIndex}'>
                                <strong>確定</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="d-inline-block align-middle custom-control custom-checkbox mr-sm-2">
                            <input type="hidden" name="commissionInfo[${currentIndex}][commit_flg]" value=0 />
                            <input data-id="${currentIndex}" onchange="checkCommissionFlg(this)" class="custom-control-input chk-commit-flg" id="commit_flg${currentIndex}" name="commissionInfo[${currentIndex}][commit_flg]" type="checkbox" value=1 />
                            <label class="custom-control-label" for="commissionInfo[${currentIndex}][commit_flg]"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>メール/FAX送信</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex  mb-lg-0">
                            ${typeof commission.send_mail_fax_othersend !== "undefined" ? '個別送信' : (commission.send_mail_fax == 1 ? ('送信済み　' + commission.send_mail_fax_datetime) || '' : '')}
                            ${commission.send_mail_fax == 1 ? '送信済み　' + commission.send_mail_fax_datetime : ''}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0" for='lost_flg${currentIndex}'>
                                <strong>取次前失注</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="hidden" value = 0 name="commissionInfo[${currentIndex}][lost_flg]"/>
                            <input class="custom-control-input" id="lost_flg${currentIndex}" name="commissionInfo[${currentIndex}][lost_flg]" type="checkbox" value=1 />
                            <label class="custom-control-label" for="commissionInfo[${currentIndex}][lost_flg]"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>${commissionString} </strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex mb-lg-0">
                            <p>${commissionValue}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0" for='corp_claim_flg${currentIndex}'>
                                <strong>加盟店クレーム</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="hidden" value=0 name="commissionInfo[${currentIndex}][corp_claim_flg]"/>
                            <input class="custom-control-input" id="corp_claim_flg${currentIndex}" disabled name="commissionInfo[${currentIndex}][corp_claim_flg]" type="checkbox" value=1 >
                            <label class="custom-control-label" for="commissionInfo[${currentIndex}][corp_claim_flg]"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>単価ランク </strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex  mb-lg-0">
                            <p>${commission.commission_unit_price_rank} 取次単価 ${commission.commission_unit_price_display}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong></strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">

                    </div>
                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>注意事項</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9 py-2">
                        ${commission.attention}

                    </div>
                </div>

            </div>

            <div class="row mx-0 border " style="display: none;">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>紹介不可</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9 py-2">
                        ${commission.attention}
                        <input type="hidden" name="commissionInfo[${currentIndex}][attention]" value="${commission.attention || ''}" />
                        <input type="hidden" value="${commission.introduction_not || ''}" name="commissionInfo[${currentIndex}][introduction_not]"/>
                    </div>
                </div>

            </div>

            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>長期連休状況</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9 py-2">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                ${holidayLabels}
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                ${holidayValues}
                            </tr>
                            <tr>
                                <td>備考</td>
                                <td colspan="7">
                                    <div id='select_long_vacation_note' style="display:inline-block">
                                        ${commission.long_vacation_note || ''}
									</div>
									<input type="hidden" value="${commission.long_vacation_note || ''}" name="commissionInfo[${currentIndex}][mCorpNewYear][long_vacation_note]"/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <input type="hidden" value="${commission.long_vacation_note}" name="commissionInfo[${currentIndex}][mCorpNewYear][note]"/>
            <input type="hidden" value="${commission.order_fee_val}" name="commissionInfo[${currentIndex}][mCorpCategory][order_fee]"/>
            <input type="hidden" value="${commission.order_fee_unit}" name="commissionInfo[${currentIndex}][mCorpCategory][order_fee_unit]"/>
            <input type="hidden" value="${commission.m_corp_category_note}" name="commissionInfo[${currentIndex}][mCorpCategory][note]"/>
            <input type="hidden" value="${commission.fax}" name="commissionInfo[${currentIndex}][mCorp][fax]"/>
            <input type="hidden" value="${commission.mailaddress_pc}" name="commissionInfo[${currentIndex}][mCorp][mailaddress_pc]"/>
            <input type="hidden" value="${commission.coordination_method}" name="commissionInfo[${currentIndex}][mCorp][coordination_method]"/>
            <input type="hidden" value="${commission.contactable_time}" name="commissionInfo[${currentIndex}][mCorp][contactable_time]"/>
            <input type="hidden" value="${commission.holiday}" name="commissionInfo[${currentIndex}][mCorp][holiday]"/>
            <input type="hidden" value="${commission.commission_dial}" name="commissionInfo[${currentIndex}][mCorp][commission_dial]"/>

            <input type="hidden" value="" name="commissionInfo[${currentIndex}][send_mail_fax_datetime]"/>
            <input type="hidden" value="${commission.send_mail_fax || ''}" name="commissionInfo[${currentIndex}][send_mail_fax]"/>
            <input type="hidden" value="${commission.commission_type || ''}" name="commissionInfo[${currentIndex}][commission_type]"/>
            <input type="hidden" value="${commission.commission_unit_price || ''}" name="commissionInfo[${currentIndex}][select_commission_unit_price]"/>
            <input type="hidden" value="${commission.commission_unit_price_rank || ''}" name="commissionInfo[${currentIndex}][select_commission_unit_price_rank]"/>
            <input type="hidden" value="${commission.send_mail_fax_sender || ''}" name="commissionInfo[${currentIndex}][send_mail_fax_sender]"/>
            <input type="hidden" value="${commission.send_mail_fax_othersend || ''}" name="commissionInfo[${currentIndex}][send_mail_fax_othersend]"/>
            <input type="hidden" value="${commission.order_fee_unit || ''}" name="commissionInfo[${currentIndex}][order_fee_unit]"/>
            <input type="hidden" value='${commission.attention}' name="commissionInfo[${currentIndex}][attention]" />
            ${typeof demandInfoId !== "undefined" ? '<input type="hidden" value=' + demandInfoId + ' name="commissionInfo[' + currentIndex + '][demand_id]"/>' : ''}

            <input type="hidden" value="${commission.id || ''}" name="commissionInfo[${currentIndex}][id]"/>
            <input type="hidden" value="${commission.attention}" name="commissionInfo[${currentIndex}][affiliationInfo][attention]" />
        </div>`;
}
