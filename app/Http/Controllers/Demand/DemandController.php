<?php

namespace App\Http\Controllers\Demand;

use App\Http\Controllers\Controller;
use App\Http\Requests\AutoDemandCorrespondsFormRequest;
use App\Http\Requests\Demand\DemandInfoDataRequest;
use App\Services\Auction\AuctionService;

use App\Services\CommissionService;
use App\Services\Demand\BusinessService;
use App\Services\Demand\DemandInfoMailService;
use App\Services\DemandCorrespondService;
use App\Services\Demand\DemandInfoService;
use App\Services\DemandInquiryAnswerService;
use App\Services\MPostService;
use App\Services\MSiteService;
use App\Services\PdfGenerator;
use App\Services\VisitTimeService;
use App\Traits\DemandInfoTrait;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class DemandController extends Controller
{
    use DemandInfoTrait;

    /**
     * @var DemandInfoService
     */
    protected $demandInfoService;
    /**
     * @var DemandInquiryAnswerService
     */
    protected $demandInquiryAnswerService;

    /**
     * @var VisitTimeService
     */
    protected $visitTimeService;
    /**
     * @var DemandCorrespondService
     */
    protected $demandCorrespondService;

    /**
     * @var BusinessService
     */
    protected $businessDemandService;
    /**
     * @var DemandInfoMailService
     */
    protected $demandMailService;

    /**
     * DemandController constructor.
     * @param MSiteService $mSiteService
     * @param CommissionService $commissionService
     * @param MPostService $mPostService
     * @param AuctionService $auctionService
     * @param DemandInfoService $demandInfoService
     * @param DemandInquiryAnswerService $demandInquiryAnswerService
     * @param VisitTimeService $visitTimeService
     * @param BusinessService $businessService
     * @param DemandCorrespondService $correspondService
     * @param DemandInfoMailService $demandInfoMailService
     */
    public function __construct(
        MSiteService $mSiteService,
        CommissionService $commissionService,
        MPostService $mPostService,
        AuctionService $auctionService,
        DemandInfoService $demandInfoService,
        DemandInquiryAnswerService $demandInquiryAnswerService,
        VisitTimeService $visitTimeService,
        BusinessService $businessService,
        DemandCorrespondService $correspondService,
        DemandInfoMailService $demandInfoMailService
    ) {
        parent::__construct();
        $this->mSiteService = $mSiteService;
        $this->commissionService = $commissionService;
        $this->mPostService = $mPostService;
        $this->auctionService = $auctionService;
        $this->demandInfoService = $demandInfoService;
        $this->demandInquiryAnswerService = $demandInquiryAnswerService;
        $this->visitTimeService = $visitTimeService;
        $this->businessDemandService = $businessService;
        $this->demandCorrespondService = $correspondService;
        $this->demandMailService = $demandInfoMailService;
        $this->initRepository();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pdfGenerator = app(PdfGenerator::class);
        $pdfGenerator->commission();
        utilGetDropList("IPHONE");

        return view('home');
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyInput($id = null)
    {
        $demand = $this->findDemandCorrespond($id);
        $userList = $this->mUserRepository->getUser();

        return view(
            'demand.history_input',
            [
            'demand' => $demand ?? (new DemandInfo()),
            'userList' => $userList,
            ]
        );
    }

    /**
     * @param AutoDemandCorrespondsFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @des update history_input
     */
    public function update(AutoDemandCorrespondsFormRequest $request)
    {
        $data = $request->except('_token');
        if ($data['id'] && $data['modified']) {
            $demand = $this->findDemandCorrespond($data['id']);
            if ($data['modified'] != $demand['modified']) {
                return redirect()->back()->with('modified', $data['modified']);
            }
        }
        $rslt = $this->demandCorrespondsRepository->save($data);

        if ($rslt) {
            return redirect()->back()->with('success', Lang::get('demand.message_successfully'));
        }

        return redirect()->back()->with('error', '');
    }

    /**
     * @param null $demandId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function auctionDetail($demandId = null)
    {
        $results = $this->auctionInfoRepo->getListByDemandId($demandId);
        return view('demand.auction_detail', [
            'results' => $results
            ]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function regist(DemandInfoDataRequest $request)
    {
        $data = $request->except('_token', '_method');
        $data['not-send'] = $data['not-send'] ?? 0;
        $data['demandInfo'] = $this->demandInfoService->addDefaultValueForDemand($data['demandInfo']);
        $commissionData = isset($data['commissionInfo']) ? $data['commissionInfo'] : [];
        $data['commissionInfo'] = $this->demandInfoService->makeCommissionInfoData($commissionData);

        foreach($data['commissionInfo'] as $key => $commission){
            if(!isset($commission['lost_flg'])) $commission['lost_flg'] = 0;
            if(!isset($commission['del_flg'])) $commission['del_flg'] = 0;

            if(!isset($commission['first_commission'])) $commission['first_commission'] = 0;

            if(!isset($commission['unit_price_calc_exclude'])) $commission['unit_price_calc_exclude'] = 0;
            if(!isset($commission['commit_flg'])) $commission['commit_flg'] = 0;
            if(!isset($commission['corp_claim_flg'])) $commission['corp_claim_flg'] = 0;

            $data['commissionInfo'][$key] = $commission;
            
        }


        $processData = $this->demandInfoService->processDataWithoutQuickOrder($data);
        if (!$processData) {
            // redirect back to regist demand
            return $this->backToDetail($data);
        } else {
            $data = $processData;
        }
        // DemandInfo Remove the leading and trailing spaces of each item
        $data['demandInfo'] = $this->demandInfoService->replaceSpace($data['demandInfo']);
        //One-touch loss
        $quickOrderFail = false;
        // check do_auction
        $data['demandInfo'] = $this->demandInfoService->checkDemandInfoDoAuction($data['demandInfo']);
        // check auto_selecttion
        $data['demandInfo'] = $this->demandInfoService->checkDoAutoSelection($data['demandInfo']);
        // update demand info if has quick order fail
        $data = $this->demandInfoService->processQuickOrderFail($data);

        $errFlg = false;

        $errFlg = $this->checkCommissionFlgCount($data, $request->get('commissionInfo'), $request->get('send_commission_info'), $errFlg);

        $errFlg = $this->validateAndDebug($data, $errFlg);

        $this->validateAndBackToDetail($data, $errFlg);
        if ($errFlg) {
            session()->flash('error_msg_input', __('demand.input_required'));
            \Log::debug('errFlg');
            return $this->backToDetail($data);
        }
        
        $allData = $this->demandInfoService->processAuctionSelection($data);
        $data = $allData['data'];
        $auctionFlg = $allData['auctionFlg'];
        $auctionNoneFlg = $allData['auctionNoneFlg'];
        $hasStartTimeErr = $allData['hasStartTimeErr'];
        $data = $this->demandInfoService->processDataWithSelectionSystem($data);
        try {
            DB::beginTransaction();
            $errorNo = $this->updateData($data);

            if (empty($errorNo)) {
                DB::commit();
                $this->sendNotify($data);
            } else {
                DB::rollback();
                $errorMessage = __('demand.not_email_and_fax');
                foreach ($errorNo as $val) {
                    $errorMessage .= ' [取次先' . $val . ']';
                }
                session()->flash('error_msg_input', $errorMessage);
                return $this->backToDetail($data);
            }
        } catch (Exception $e) {
             DB::rollback();
            \Log::error($e->getMessage());
            \Log::error('__order fail___' . $quickOrderFail);
            session()->flash('error_msg_input', __('demand.input_required'));
            return $this->backToDetail($data);
        }
        if ($data['send_commission_info'] == 1 && isset($data['commissionInfo'])) {
            \Log::debug('___ start process mail and fax ___');
            $mailAndFaxs = $this->demandInfoService->getMailAndFaxByCorpData($data['commissionInfo']);
            $faxList = $mailAndFaxs['faxList'];
            $mailList = $mailAndFaxs['mailList'];
            $demandInfo = $data['demandInfo'];
            $mailInfo = $this->demandMailService->getMailData($demandInfo['id']);
            $result = $this->demandMailService->sendMail($demandInfo, $mailList, $mailInfo);
            $result = $this->demandMailService->sendFax($demandInfo, $faxList, $mailInfo);
            if (!$result) {
                session()->flash('error_msg_input', __('demand.send_error'));
                \Log::debug('___ fail send email and fax ___');
                return $this->backToDetail($data);
            }
        }
        $this->updateCommissionSendMailFax($data);
        $this->setFlashError($hasStartTimeErr, $auctionNoneFlg, $auctionFlg);
        return redirect()->route('demand.detail', $data['demandInfo']['id'])->with('message', __('demand.msg_success'));
    }


    /**
     * @param $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    private function backToDetail($data)
    {
        $errs = $this->getErrors();

        if (!isset($data['demandInfo']['id']) || empty($data['demandInfo']['id'])) {
            return redirect()->route('demand.get.create')->withInput($data)->withErrors($errs);
        }

        $data = (isset($data['restore_at_error']) && !empty($data['restore_at_error']))
            ? array_replace_recursive($data, $data['restore_at_error'])
            : $data;

        $data['demandCorrespond'] = $data['demandCorrespond'] ?? [];

        if (array_key_exists('id', $data['demandInfo'])) {
            $dispData = $this->demandInfoRepo->getDemandByIdWithRelations($data['demandInfo']['id']);
        } else {
            $data['demandInfo']['same_customer_demand_url'] =
            route('demand.detail', $data['demandInfo']['source_demand_id']);
        }
        $dispData['demandInfo'] = $data['demandInfo'];
        $dispData['visitTime'] = $data['visitTime'];
        $dispData['commissionInfo'] = $data['commissionInfo'];
        $dispData['demandCorrespond'] = $data['demandCorrespond'];

        $answerList = [];
        for ($i = 0; $i < count($dispData['mInquiry']); $i++) {
            $answerList[$i] = $this->mAnswerRepository->dropDownAnswer(
                $dispData['mInquiry'][$i]['demandInquiryAnswer']['inquiry_id']
            );
        }
        $dispData['mAnswer'] = $answerList;

        $dispData['commissionInfo_tmp_corp_id'] = $data['CommissionInfo_tmp_corp_id'] ?? '';
        $siteList = $this->mSite->getListMSitesForDropDown();
        $genreList = $this->mSiteGenresRepository->getGenreBySiteStHide($data['demandInfo']['site_id']);

        $userList = $this->mUserRepository->getListUserForDropDown();
        $categoryList = $this->mCateRepo->getDropListCategory(
            $data['demandInfo']['genre_id'],
            !empty($data['demandInfo']['id'])
        );

        $crossSellSourceSite = [];
        if (isset($data['demandInfo']['cross_sell_source_site'])) {
            $crossSellSourceSite = $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId(
                $data['demandInfo']['cross_sell_source_site']
            );
        }

        $selectionSystemList = $this->demandInfoService->getSelectionSystemList();

        if (isset($data['demandInfo']['id'])) {
            return redirect()->back()->withErrors($errs)->withInput($data)->with('disableLimit', 'disabled');
        }
        return view('demand.detail', compact(
            'dispData',
            'siteList',
            'genreList',
            'userList',
            'categoryList',
            'crossSellSourceSite',
            'selectionSystemList'
        ));
    }

    /**
     * @param null $customerTel
     * @param null $siteTel
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cti($customerTel = null, $siteTel = null)
    {
        if (empty($siteTel)) {
            return redirect()->route('demand.get.create');
        }
        $customerTelSave = '';
        if ($customerTel == '0') {
            $customerTelSave= '0';
            $customerTel= '';
        }

        if ($customerTel == NON_NOTIFICATION) {
            $customerTelSave= NON_NOTIFICATION;
            $customerTel= '';
        }

        $demandInfo = $this->demandInfoService->setPreDemand($customerTel, $siteTel);

        $demandInfo['demand_status'] = 0;
        if ($customerTelSave == NON_NOTIFICATION) {
            $demandInfo['customer_tel'] = NON_NOTIFICATION;
        }

        return redirect()->route('demand.get.create')->with('ctiDemandInfo', $demandInfo);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $data = $this->businessDemandService->getCommonData();

        $vacationLabels = $this->mItemRepository->getMItemList(LONG_HOLIDAYS, date('Y/m/d'));

        $selectionSystemList = $this->demandInfoService->getSelectionSystemList();

        $genresDropDownList = config('constant.defaultOption');
        if (isset($request->old('demandInfo')['site_id'])) {
            $genresDropDownList = array_replace($genresDropDownList, $this->mSiteGenresRepository->getGenreBySiteStHide($request->old('demandInfo')['site_id'])->toArray());
        }

        $categoriesDropDownList = config('constant.defaultOption');
        if (isset($request->old('demandInfo')['genre_id'])) {
            $categoriesDropDownList =
            $this->mCateRepo->getListCategoriesForDropDown($request->old('demandInfo')['genre_id']);
        }

        return view('demand.create', array_replace($data, [
                'vacationLabels' => $vacationLabels,
                'selectionSystemList' => $selectionSystemList,
                'categoriesDropDownList' => $categoriesDropDownList,
                'genresDropDownList' => $genresDropDownList
            ]));
    }

    /**
     * clone a demand
     *
     * @param  null $demandId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function copy($demandId = null)
    {
        if ($demandId == null) {
            return abort(404);
        }

        $demand = $this->demandInfoRepo->getDemandByIdWithRelations($demandId);

        if (!$demand) {
            return abort(404);
        }

        $demand = $this->businessDemandService->unsetData($demand, [
            'id', 'selection_system_before', 'commissionInfos', 'auction', 'riro_kureka', 'demandAttachedFiles'
        ]);

        $demand->demand_status = 1;
        $demand->demand_status_before = $demand->demand_status;

        $selectionSystemList = $this->demandInfoService->getSelectionSystemList($demand);
        $data = $this->businessDemandService->getDataForDetail($demand, $demand->cross_sell_source_site);

        return view('demand.detail', array_replace(['selectionSystemList' => $selectionSystemList, 'copy' => true , 'demand' => $demand], $data));
    }

    /**
     * Remove demand info
     *
     * @param  integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->demandInfoRepo->deleteByDemandId($id);

        return redirect()->route('demandlist.search');
    }

    /**
     * クロスセル専用
     *
     * @param  integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cross($id = null)
    {
        if (empty($id)) {
            return redirect()->route('demand.detail');
        }

        $demand = $this->demandInfoRepo->getDemandByIdWithRelations($id);

        $demand->cross_sell_source_site =
            isset($demand->site_id) ? $demand->site_id : '';

        $site = $this->mSite->getSiteByName(CROSS_SELLING_DEAL);
        $demand->site_id = $site['id'] ?? '';

        $this->businessDemandService->setData($demand);

        $demand = $this->businessDemandService->unsetData($demand, [
            'id', 'selection_system_before', 'commissionInfos', 'IntroduceInfo', 'demandAttachedFiles', 'auction'
        ]);

        $selectionSystemList = $this->demandInfoService->getSelectionSystemList($demand);

        $siteId = null;
        if (!empty($demand->cross_sell_source_site)) {
            $site = $this->mSite->find($demand->cross_sell_source_site);
            $siteId = ($site && $site->cross_site_flg == 1) ? $site->id : null;
        }

        $data = $this->businessDemandService->getDataForDetail($demand, $siteId);

        return view('demand.detail', array_replace(['selectionSystemList' => $selectionSystemList, 'demand' => $demand, 'copy' => true], $data));
    }

    /**
     * Download file attach in demand
     *
     * @param  integer $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function demandFileDownload($id = null)
    {
        if ($id == null) {
            return abort(404);
        }

        $fileAttach = $this->demandAttachedFileRepo->findId($id);
        if (!file_exists($fileAttach->path)) {
            return abort(404);
        }

        return response()->download($fileAttach->path, $fileAttach->name);
    }
}
