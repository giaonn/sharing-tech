<?php

namespace App\Repositories\Eloquent;

use App\Models\ExclusionTime;
use App\Repositories\ExclusionTimeRepositoryInterface;

class ExclusionTimeRepository extends SingleKeyModelRepository implements ExclusionTimeRepositoryInterface
{
    /**
     * @var ExclusionTime
     */
    protected $model;

    const PATTERN = 'パターン';

    /**
     * ExclusionTimeRepository constructor.
     *
     * @param ExclusionTime $model
     */
    public function __construct(ExclusionTime $model)
    {
        $this->model = $model;
    }

    /**
     * get list data ExclusionTime
     *
     * @return array
     */
    public function getList()
    {
        $lists = $this->model->select('exclusion_times.pattern')->orderBy('exclusion_times.pattern', 'asc')
            ->groupBy('pattern')->get()->toarray();
        $results[''] = trans('common.none');
        foreach ($lists as $list) {
            $results[$list['pattern']] =  self::PATTERN.$list['pattern'];
        }
        return $results;
    }

    /**
     * get by pattern
     *
     * @param  integer $pattern
     * @return array
     */
    public function findByPattern($pattern = null)
    {
        $result = $this->model->select('id', 'pattern', 'exclusion_time_from', 'exclusion_time_to', 'exclusion_day')->where('pattern', '=', $pattern)
            ->where(
                function ($query) {
                    $query->whereNotNull('exclusion_time_from')
                        ->orWhereNotNull('exclusion_time_to');
                }
            )->get()->first();
        return $result;
    }

    /**
     * @return \Illuminate\Support\Collection|mixed
     */
    public function getExclusionTime()
    {
        $lists = $this->model->select('id', 'exclusion_time_from', 'exclusion_time_to', 'exclusion_time_to', 'exclusion_day', 'pattern')
            ->orderBy('pattern', 'asc')
            ->get();
        return $lists;
    }

    /**
     * @param integer $id
     * @param array $data
     */
    public function updateExclusion($id, $data)
    {
        $this->model->where('id', $id)->update($data);
    }

    /**
     * @param integer $genreId
     * @param integer $prefectureCd
     * @return \Illuminate\Database\Eloquent\Model|mixed|null|object|static
     */
    public function getData($genreId, $prefectureCd)
    {
        $result = $this->model->with(
            ['auctionGenreAreas' => function ($q) use ($genreId, $prefectureCd) {
                $q->where('genre_id', $genreId)->where('prefecture_cd', $prefectureCd);
            }]
        )->first();

        if (!empty($result)) {
            return $result;
        }

        $result = $this->model->with(
            ['auctionGenres' => function ($q) use ($genreId) {
                $q->where('genre_id', $genreId);
            }]
        )->first();
        return $result;
    }
}
