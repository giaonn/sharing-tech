<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td>@lang('demand_detail.location')</td>
                    <td>
                        {{ $mCorp->address1_jp  }}{{ $mCorp->address2 }}{{ $mCorp->address3 }}
{{--                        {{ $mCorp->address3 != '' && $mCorp->address4 != '' && strpos($mCorp->address3, $mCorp->address4) ? '' : $mCorp->address4 }}--}}
                    </td>
                </tr>

                <tr>
                    <td>@lang('demand_detail.fax_num')</td>
                    <td>
                        {{ $mCorp->fax }}
                    </td>
                </tr>
                <tr>
                    <td>@lang('demand_detail.pc_email')</td>
                    <td>

                        @foreach($mCorp->email_by_array as $mail)
                            <a href="callto:{{ $mail }}">{{ $mail }}</a> <br/>
                        @endforeach
                    </td>
                </tr>

                <tr>
                    <td>@lang('demand_detail.custome_info')</td>
                    <td>
                        {{ $mCorp->text_coordination }}
                    </td>
                </tr>
                <tr>
                    <td>@lang('demand_detail.available_time')</td>
                    <td>
                        @if($mCorp->contactable_support24hour)
                            @lang('demand_detail.24h')
                        @else
                            {{ $mCorp->contactable_time_from . ' - ' . $mCorp->contactable_time_to }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>@lang('demand_detail.holiday')</td>
                    <td>
                        {{ $mCorp->holiday1 }}
                    </td>
                </tr>
                <tr>
                    <td>@lang('demand_detail.order_fee')</td>
                    <td>
                        {!! $feeCommission !!}
                    </td>
                </tr>

                <tr>
                    <td>@lang('demand_detail.order_note')</td>
                    <td>
                        {!! !empty($feeData->note) ? $feeData->note : ''  !!}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
