<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/lib/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ mix('css/lib/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="progress-block">
    <div class="progress"></div>
</div>
<div id="top">
    {{--Header component--}}
    <header>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container px-3 pb-2" id="hearder-logo-app">
                {{--Logo brand--}}
                <a class="navbar-brand" href="{{ url('/') }}">
                    <span class="sr-only">Sharing Tech</span>
                    <img src="{{asset('assets/img/mover.png')}}" alt="Logo" class="img-fluid">
                </a>
            </div>

            <div class="collapse navbar-collapse d-lg-block w-100 mt-lg-2" id="app-navbar-collapse">
                <div class="container">
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav justify-content-lg-start w-100">
                        <!-- Authentication Links -->
                    </ul>
                </div>
            </div>
        </nav>
    </header>


    {{--Main content component--}}
    <div class="body-wrapper-default">
        <div class="container">
            <div class="guideline">
                <h3 class="font-weight-bold text-header mt-3 mb-4 pb-1">利用規約</h3>
                <p>利用者は、以下の利用規約（以下「本規約」といいます）に同意することを条件として、本サービスを利用することができるものとします。</p>

                <h5 class="font-weight-bold text-group">第 1 条（本規約の適用）</h5>
                <ol>
                    <li>本規約は、シェアリングテクノロジー株式会社(以下「当社」といいます)が提供する顧客管理システム「SHARINGPLACE」（以下、「本サービス」という。）を利用することに伴うすべての事項にわたり適用するものとします。</li>
                    <li>本サービスは、本規約に基づき運営されるサービスであり、当社は、本サービスの利用を希望し、かつ本規約に同意した個人、法人、その他の団体（以下、「利用者」という。）に対して本サービスを提供します。</li>
                    <li>本サービスは、当社加盟店規約上の「加盟店向けＷＥＢシステム」の一部を構成します。本サービスの機能のうち、入札機能その他顧客紹介に関する機能を利用する場合には、当社加盟店規約に定める条件で当社の加盟店となることの申込みを行い、当社が審査の上で加盟店登録を承認することが必要です。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 2 条（本規約の改訂）</h5>
                <ol>
                    <li>当社は利用者に事前の通知なく、本規約を変更できるものとします。</li>
                    <li>本規約の変更に伴い、利用者に不利益、損害が発生した場合、当社はその責任を一切負わないものとします。</li>
                    <li>本規約の変更については、別途当社が定める場合を除き、当社のウェブサイト上に掲載した時点から変更後の規約が適用されるものとします。</li>
                    <li>利用者が、本規約の変更後に本サービスを利用した場合には、本規約の変更に同意したものとみなされます。利用者が本規約の変更に同意できない場合には、当社所定の手続きにより本サービスのユーザー登録の抹消を行い、本サービスの利用を中止するものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 3 条（サービスの内容）</h5>
                <ol>
                    <li>1. 本サービスは、利用者に対し顧客管理システム「SHARINGPLACE」を当社が提供するものです。</li>
                    <li>2. 本サービスの具体的な内容については、当社がその裁量により決定及び変更することができるものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 4 条（利用の申込み）</h5>
                <ol>
                    <li>本サービスの利用を希望する者は、当社が定める登録手続きに従って申込をし、当社がこれを承諾することにより、本サービスの利用資格を得られるものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 5 条（システムメンテナンス）</h5>
                <ol>
                    <li>当社はシステム保守を行う場合、あらかじめ利用者に対して通知した上で本サービスの提供を一時中止することができるものとします。</li>
                    <li>但し、第 6 条に定める理由により本サービスの提供ができなくなった場合は、利用者に通知をすること無く、本サービスを中断もしくは中止することができるものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 6 条（障害発生時の処置）</h5>
                <ol>
                    <li>当社は、次の各号のいずれかに該当する場合には、当社の任意の判断で利用者に事前に通知することなく、本サービスの運用の全部または一部の提供を中断することができるものとします。<br>(1)天災、事変、疫病の蔓延その他の不可抗力が発生し、または発生する恐れがある場合<br>(2)電気通信設備の保守上または工事、障害その他やむをえない事由が生じた場合<br>(3)法令等による規制が行なわれた場合<br>(4)前各号のほか、当社が必要と認める場合</li>
                    <li>当社は、システムの停止、電気通信回線の異常、その他システム障害が発生した際には、復旧についての最善の策をとるものとしますが、これらの事由により利用者及び第三者に生じた損害、結果について、一切責任を負わないものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 7 条（ID 及びパスワードの管理）</h5>
                <ol>
                    <li>当社は、利用者に対し、利用者 ID 及びパスワードを発行するものとします。</li>
                    <li>利用者 ID およびパスワードは、利用者本人が管理責任を負うものとします。</li>
                    <li>利用者は、ID およびパスワードの第三者を介した利用、貸与、譲渡、名義変更、売買、質入等いかなる行為をしてはならないものとします。</li>
                    <li>利用者は、利用者 ID またはパスワードを紛失、漏洩し、または盗用された場合は、直ちに当社に届け出を行い、当社の指示に従うものとします。</li>
                    <li>利用者は、利用者 ID 及びパスワードの運用、保管管理について一切の責任を負い、運用、保管管理の過誤によって生じた損害について一切の責任を負うものとします。</li>
                    <li>利用者の ID 及びパスワードを用いて行われた行為は、すべて利用者自身による行為とみなします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 8 条（禁止事項）</h5>
                <ol>
                    <li>利用者は、本サービスの利用に関して以下の行為を行ってはならないものとします。</li>
                    <li>他の利用者のアカウントおよび名称等を不正に使用する行為</li>
                    <li>有害なコンピュータプログラム等を本サービスに関連して使用し若しくは提供する行為</li>
                    <li>第三者又は本サービスに損害を与え、又は与えるおそれのある行為</li>
                    <li>事実に反する情報を提供する行為</li>
                    <li>当社と同種または類似の業務を行う行為</li>
                    <li>本サービスを通じて提供される情報を改ざんする行為</li>
                    <li>第三者の著作権、その他の知的財産権を侵害する行為、又はそのおそれのある行為</li>
                    <li>他の利用者や第三者を誹謗または中傷、名誉を傷つける行為</li>
                    <li>他の利用者や第三者のプライバシーを侵害する行為</li>
                    <li>サーバその他当社のコンピュータに不正にアクセスする行為</li>
                    <li>公序良俗に反する行為</li>
                    <li>本サービスの運営を妨害する行為、あるいは妨害するおそれのある行為</li>
                    <li>諸法令に違反する行為、又は違反するおそれのある行為</li>
                    <li>その他、当社が不適切と判断する行為</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 9 条（免責事項）</h5>
                <ol>
                    <li>当社の責任は、本サービスを利用者が快適に利用できるために、最善の努力を持って本サービスの提供を行う事に限られるものとします。利用者は全て自己責任において本サービスを利用するものとします。また利用者が本サービスを利用する事によって不利益や損害が生じた場合にも当社は一切の責任を負わないものとします。また、当社は、以下のいずれかが発生した場合でも一切の責任を負わないものとします。</li>
                    <li>本サービスの解約、変更、中断、中止もしくは利用停止による損失、損害</li>
                    <li>本サービスに関連して第三者に損害が発生した場合</li>
                    <li>本サービスを通じて保管した情報の紛失、破壊、漏洩その他保管に関する損失、損害</li>
                    <li>本サービスより配信される電子メールの配信遅延、未配信、誤配信、消失、改ざん、文字化け等が発生した場合により生じた損失、損害</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 10 条（機密保持）</h5>
                <ol>
                    <li>当社は本サービスの提供に際し知り得た利用者の情報を第三者に漏洩してはならないものとします。</li>
                    <li>秘密保持義務は、本サービス利用終了後も効力を有するものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 11 条（損害賠償）</h5>
                <ol>
                    <li>当社もしくは利用者が、本規約に違反し、または故意や重大な過失により相手方に損害を与えた場合、両者協議のうえその損害を賠償するものとします。</li>
                    <li>利用者が本サービスの利用によって第三者に損害を与えた場合は、利用者は自己の責任において解決するものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 12 条（合意管轄裁判所）</h5>
                <ol>
                    <li>本サービスに関する訴訟については、名古屋地方裁判所を、第一審の専属管轄裁判所とします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 13 条（解除）</h5>
                <ol>
                    <li>利用者は、本サービスの利用を終了する際に、終了日の 1 ケ月前までに当社に通知するものとします。通知日と終了日の期間が 1 ケ月に満たないときは通知日から 1 ケ月をもって終了日とします。</li>
                    <li>利用者が本規約に違反した場合、当社は、即時に利用者による本サービスの利用を停止し、利用者登録を解除できるものとします。</li>
                </ol>

                <h5 class="font-weight-bold text-group">第 14 条（本サービスの廃止）</h5>
                <ol>
                    <li>当社は、利用者に通知することにより、本サービスを廃止することができるものとします。</li>
                </ol>

                <div class="text-right pt-4">
                    <div class="box p-2">
                        <div class="text-center">
                            <p>更新履歴</p>
                        </div>
                        <ul>
                            <li class="text-left">平成28年10月27日　制定</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Footer component--}}
    @include('partials.footer')
</div>

</body>
</html>
<script type="text/javascript">
    function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
    document.addEventListener('keydown', disableF5);
</script>
