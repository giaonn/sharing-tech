@inject('service', 'App\Services\Auction\AuctionService')

@php
    $auctionMaskingAll = $service->getDivValue("auction_masking" , "all_exclusion");
    $auctionMaskingWithout = $service->getDivValue("auction_masking" , "without");
@endphp

@extends('layouts.app')

@section('content')
    <div class="auction-search">
        {!! Form::open(['url' => route('auction.post.search'), 'id' => 'search-form']) !!}
        {!! Form::submit(__('auction.update_info_new'), ['name' => 'Search', 'id' => 'search-btn', 'class' => 'btn btn--gradient-green font-weight-bold px-4 mt-2']); !!}
        <div class="form-category mt-5 d-none d-xl-block">
            <label class="form-category__label fs-15 font-weight-light">@lang('auction.list_of_biddable_items')</label>
        </div>
        <label class="collapse-label-mobi font-weight-bold p-2 w-100 mt-3 text--white d-block d-xl-none" data-toggle="collapse" data-target="#search-box-mobi" aria-expanded="false" aria-controls="search-box-mobi">@lang('auction.list_of_search_items')<i class="fa fa-chevron-down float-right" aria-hidden="true"></i></label>
        <div id="search-box-mobi" class="search-box row mx-0 collapse show">
            <div class="col-lg-3 px-0 mb-2 mb-md-0">
                <div>
                    <label class="mb-1">@lang('auction.proposal_status')</label>
                </div>
                <div class="d-flex fs-11">
                    <div class="switch-btn {{ $display ? 'activated' : '' }} left p-2">
                        {!! Form::radio("display", '0', $display, ['style'=>'display:none']) !!}@lang('auction.show_all')
                    </div>
                    <div class="switch-btn {{ !$display ? 'activated' : '' }} right py-2 px-3">
                        {!! Form::radio('display', '1', !$display, ['style'=>'display:none']) !!}
                        @lang('auction.excluding_competitors_already_bidding_deals')
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 px-0 px-md-2 mb-2 mb-md-0 width-ui-state-default">
                <div>
                    <label class="mb-1">@lang('auction.area')</label>
                </div>
                {!! Form::select('address1[]', config('jpstate.kanji'), null, ['id'=>'auction-search-1', 'multiple'=>'true', 'hidden'=>'true']); !!}
            </div>
            <div class="col-md-4 col-lg-4 px-0 px-md-2 mb-2 mb-md-0">
                <div><label class="mb-1">@lang('auction.genre')</label></div>
                {!! Form::select('genre_id[]', $listMGeners, null, ['id'=>'auction-search-2', 'multiple'=>'true', 'hidden'=>'true']); !!}
            </div>
            <div class="col-md-3 col-lg-1 px-0">
                <div class="d-none mb-1 d-md-block opacity-0">pseudo</div>
                {!! Form::submit(__('auction.search'), ['name' => 'Search', 'id' => 'search-btn', 'class' => 'btn btn--gradient-orange col-sm-3 col-md-12']); !!}
            </div>
        </div>
        <label class="collapse-label-mobi font-weight-bold p-2 w-100 mt-3 text--white d-block d-xl-none" data-toggle="collapse" data-target="#table-group-mobi" aria-expanded="false" aria-controls="table-group-mobi">@lang('auction.list_of_biddable_items')<i class="fa fa-chevron-down float-right" aria-hidden="true"></i></label>
        <div id="table-group-mobi" class="collapse show">
            <label class="bg-gray-light fs-11 d-block d-xl-none font-weight-bold p-1 pl-1 w-100">@lang('auction.sort')</label>
            <div class="auction-link clearfix sort-key-box fs-11 mt-4 d-inline d-xl-flex flex-xl-row">
                @php
                    $sort = $dataSort['sort'];
                    $order = $dataSort['order'];
                @endphp
                <div class="py-1 px-2 d-none d-xl-block">
                    <i class="fa fa-square" aria-hidden="true"></i> @lang('auction.sort')
                </div>
                <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link">
                    @php
                        $sortInfor = $service->getInforOrderSort($sort, $order, 'demand_infos.auction_deadline_time');
                    @endphp
                    <a href="{{ route('auction.index') }}?sort=demand_infos.auction_deadline_time&order={{ $sortInfor['order_display'] }}" class="text-dark">
                        {{ $sortInfor['icon'] }}@lang('auction.bid_button_order_by_deadline')</a>
                </div>
                <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link">
                    @php
                        $sortInfor = $service->getInforOrderSort($sort, $order, 'demand_infos.visit_time_min');
                    @endphp
                    <a href="{{ route('auction.index') }}?sort=demand_infos.visit_time_min&order={{ $sortInfor['order_display'] }}" class="text-dark">
                        {{ $sortInfor['icon'] }}@lang('auction.by_visit_date_and_time')</a>
                </div>
                <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link">
                    @php
                        $sortInfor = $service->getInforOrderSort($sort, $order, 'demand_infos.contact_desired_time');
                    @endphp
                    <a href="{{ route('auction.index') }}?sort=demand_infos.contact_desired_time&order={{ $sortInfor['order_display'] }}" class="text-dark">
                        {{ $sortInfor['icon'] }}@lang('auction.by_phone_call_date_and_time_order')</a>
                </div>
                <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link d-none d-xl-block">
                    @php
                        $sortInfor = $service->getInforOrderSort($sort, $order, 'demand_infos.receive_datetime');
                    @endphp
                    <a href="{{ route('auction.index') }}?sort=demand_infos.receive_datetime&order={{ $sortInfor['order_display'] }}" class="text-dark">
                        {{ $sortInfor['icon'] }}@lang('auction.by_date_of_reception')</a>
                </div>
                <div class="py-1 px-2 d-none d-xl-block">
                    <span class="text--orange">{{trans('common.asterisk')}} {{ $sortInfor['icon'] }}@lang('auction.default_bid_button_order_by_deadline')</span>
                </div>
            </div>
            <div class="fs-11 mt-3 text--info d-none d-xl-block">
                <span>@lang('common.asterisk') @lang('auction.text_contact')</span>
            </div>
            {!! Form::close() !!}

            @foreach($results as $key => $val)
                {!! Form::hidden('AuctionInfo.[push_time]', isset($val->push_time) ? $val->push_time : ""); !!}
                @php
                    $status = $service->detectAuctionBtn($val);
                    $latComission = $service->findLastCommission($val, 'created');
                @endphp
                @if (!(($status == 1 || $status == 2) && (strtotime($latComission . '+' . $supportMessageTime . ' hour') < strtotime(date('Y-m-d H:i:s')))))
                    <div class="mt-xl-3 table-block">
                        <div class="row p-2 mx-0">
                            <div class="col-xl-6 px-0">
                                <div class="row">
                                    <div class="col-sm-2 py-2">
                                        @if($service->getDivValue('priority', 'normal') != $val->priority)
                                            @if($service->getDivTextJP('priority', $val->priority, true))
                                                <span class="label-1 p-1 border-orange">
                                            {{ $service->getDivTextJP('priority', $val->priority, true) }}
                                        </span>
                                            @endif
                                        @else
                                            @if(!empty(trim($service->getDivTextJP('priority', $val->priority, true))))
                                                <span class="label-1 p-1">
                                            {{ $service->getDivTextJP('priority', $val->priority, true) }}
                                        </span>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-sm-4 py-2 d-none d-xl-block">
                                    <span class="label-2"><i class="fa fa-square" aria-hidden="true"></i>
                                        @lang('auction.proposal_number') {{ $val->id }}
                                    </span>
                                    </div>
                                    <div class="col-sm-6 py-2 d-none d-xl-block">
                                        <span class="label-3"><i class="fa fa-square"
                                                                aria-hidden="true"></i> @lang('auction.site_name'): <a
                                                href="http://{{ $val->site_url }}" target="_blank">{{ $val->site_name }}</a></span>
                                    </div>
                                    @if(!empty($val->auction_fee))
                                        <div class="col-md-6 py-2 font-weight-bold">
                                            <span class="p-1">@lang('auction.bidding_fee'): </span>
                                            <span
                                                class="text-danger">{{ $service->yenFormat2($val->auction_fee) }}</span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="row">
                                    <div class="col-md-6 px-0 py-2 d-none d-xl-block">
                                        <span class="date-lable p-1">@lang('auction.agency_(receipt)_date_and_time')</span>
                                        <span class="date">{{ dateTimeWeek($val->receive_datetime) }}</span>
                                    </div>
                                    <div class="col-md-6 px-0 py-2">
                                        <span class="date-lable p-1">@lang('auction.bid_button_deadline')</span>

                                        <span
                                            class="date">{{ dateTimeWeek($val->auction_deadline_time) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive list d-none d-xl-block">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="text-center">
                                        <th class="align-middle">@lang('auction.date_and_time_of_visit')</th>
                                        <th class="align-middle">@lang('auction.telephone_date_time_desired')</th>
                                        <th class="align-middle">@lang('auction.price_offer')</th>
                                        <th class="align-middle">@lang('auction.travel_expenses')</th>
                                        <th class="align-middle">@lang('auction.customer_name')</th>
                                        <th class="align-middle">@lang('auction.contact')</th>
                                        <th class="align-middle">@lang('auction.address')</th>
                                        <th class="align-middle">@lang('auction.building_type')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>
                                            @php
                                                $isDisplay = false;
                                                $visitTimes = explode('|', $val->prop);
                                                if(count($visitTimes) == 4) {
                                                    $isDisplay = true;
                                                    $targetTime = $visitTimes[0];
                                                }
                                            @endphp
                                            @if($isDisplay)
                                                <p class="font-weight-bold text-danger mb-0">{{ ($visitTimes[1] == 0) ? __('auction.specify_time') : __('auction.time_to_adjust') }}</p>
                                                <p>{{ dateTimeWeek($visitTimes[0]) }}</p>
                                                @if($visitTimes[1] == 1)
                                                    @lang('common.wavy_seal')<br>
                                                    {{ dateTimeWeek($visitTimes[2]) }}
                                                    <br>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @php
                                                $isDisplayContactDesiredTime = false;
                                                if ($val->is_contact_time_range_flg == 0 && strlen($val->contact_desired_time) > 0) {
                                                    $isDisplayContactDesiredTime = true;
                                                    $targetTime = $val->contact_desired_time;
                                                }
                                            @endphp
                                            @if ($isDisplayContactDesiredTime)
                                                <p class="font-weight-bold mb-0 text-danger">@lang('auction.time_specification')</p>
                                                {{ dateTimeWeek($val->contact_desired_time) }}
                                            @endif
                                            @php
                                                $isDisplayContactDesiredTimeTo = false;
                                                if ($val->is_contact_time_range_flg == 1 && strlen($val->contact_desired_time_from) > 0) {
                                                    $isDisplayContactDesiredTimeTo = true;
                                                    $targetTime = $val->contact_desired_time_from;
                                                }
                                            @endphp
                                            @if ($isDisplayContactDesiredTimeTo)
                                                <p class="font-weight-bold mb-0 text-danger">@lang('auction.time_to_adjust')</p>
                                                {{ dateTimeWeek($val->contact_desired_time_from) }}
                                                <br>@lang('common.wavy_seal')<br>
                                                {{ dateTimeWeek($val->contact_desired_time_to) }}
                                            @endif
                                            @if (strlen($val->contact_desired_time) == 0 && strlen($val->contact_desired_time_from) == 0 && count($visitTimes) == 4)
                                                <p class="mb-0">{{ dateTimeWeek($visitTimes[3]) }}</p>
                                                @if (strlen($visitTimes[3]) > 0)
                                                    <p class="mb-0 font-weight-bold text-danger">
                                                        @lang('auction.after_bidding_confirm_the_contents')
                                                        , @lang('auction.upon_your_request_promptly')
                                                        , @lang('auction.call_and_decide_the_visit_time')
                                                        , @lang('auction.please')
                                                    </p>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <p class="mb-0">
                                                @if(empty($val->cost_from) && empty($val->cost_to))
                                                    @lang('auction.none')
                                                @elseif(!empty($val->cost_from) && !empty($val->cost_to))
                                                    {{ $service->yenFormat2($val->cost_from) }}
                                                    {{ trans('common.wavy_seal') }} {{ $service->yenFormat2($val->cost_to) }}
                                                @else
                                                    {{ !empty($val->cost_from)? $service->yenFormat2($val->cost_from) : $service->yenFormat2($val->cost_to) }}
                                                @endif
                                            </p>
                                        </td>
                                        <td>
                                            <p class="mb-0">
                                                @if (empty($val->business_trip_amount))
                                                    @lang('auction.none')
                                                @else
                                                    {{ $service->yenFormat2($val->business_trip_amount) }}
                                                @endif
                                            </p>
                                        </td>
                                        <td>
                                            {{ $val->customer_name }}
                                        </td>
                                        <td>
                                            @if(!$isRoleAffiliation)
                                                <a href="callto:{{ $val->tel1 }}">{{ $val->tel1 }}</a><br>
                                            @else
                                                @php
                                                    $hour = 0;
                                                    $minute = 0;
                                                    if (isset($telDisclosure[$val->priority]['item_hour_date'])) {
                                                        $hour = $telDisclosure[$val->priority]['item_hour_date'];
                                                    }
                                                    if (isset($telDisclosure[$val->priority]['item_minute_date'])) {
                                                        $minute = $telDisclosure[$val->priority]['item_minute_date'];
                                                    }
                                                    if (!empty($hour)) {
                                                        $hour = $hour * 60;
                                                    }
                                                    $num = $hour + $minute;
                                                    $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                    $isCallToPhone = false;
                                                    if ((strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                        $val->auction_masking != $auctionMaskingWithout ||
                                                        ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        !empty($visitTimes[3])
                                                    ) && $status == 1 ) {
                                                        $isCallToPhone = true;
                                                    }
                                                @endphp
                                                @if ($isCallToPhone)
                                                    <a href="callto:{{ $val->tel1 }}">{{ $val->tel1 }}</a><br>
                                                @else
                                                    {{ !empty($val->tel1) ? substr_replace($val->tel1, "******", -6,6) : '' }}
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <p class="mb-0">
                                                @if (!$isRoleAffiliation)
                                                    {{ $service->getDivTextJP('prefecture_div', $val->address1) }}{{ $val->address2 }}{{ $val->address3 }}
                                                    <br>
                                                @else
                                                    @php
                                                        $hour = 0;
                                                        $minute = 0;
                                                        if (isset($addressDisclosure[$val->priority]['item_hour_date'])) {
                                                            $hour = $addressDisclosure[$val->priority]['item_hour_date'];
                                                        }
                                                        if (isset($addressDisclosure[$val->priority]['item_minute_date'])) {
                                                            $minute = $addressDisclosure[$val->priority]['item_minute_date'];}
                                                        if (!empty($hour)) {
                                                            $hour = $hour * 60;
                                                        }
                                                        $num = $hour + $minute;
                                                        $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                        $isDisplayAddressFull = false;
                                                        if( (strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                            $val->auction_masking == $auctionMaskingAll ||
                                                            ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                            ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                            !empty($visitTimes[3])
                                                        ) && $status == 1 ) {
                                                            $isDisplayAddressFull = true;
                                                        }
                                                    @endphp
                                                    @if ($isDisplayAddressFull)
                                                        {{ $service->getDivTextJP('prefecture_div', $val->address1) }}{{ $val->address2 }}{{ $val->address3 }}
                                                        <br>
                                                    @else
                                                        {{ $service->getDivTextJP('prefecture_div', $val->address1) }}{{ $val->address2 }}{{ $service->maskingAddress3($val->address3) }}
                                                        <br>
                                                    @endif
                                                @endif
                                            </p>
                                        </td>
                                        <td>
                                            <p class="mb-0">{{ $service->getDropText($buildingType, $val->construction_class) }}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="bg--gray-light ankenDetail ankenDetail-{{ $val->id }}"
                                            style="display:none" id="ankenDetail-{{ $val->id }}">
                                            <span>@lang('auction.case_detail')</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="display:none" id="ankenDetailText-{{ $val->id }}"
                                            class="ankenDetailText ankenDetailText-{{ $val->id }}"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="text-center">
                                            <div id="btnAnkenDetail-{{ $val->id }}" class="btnAnkenDetail"
                                                style="display:none"></div>
                                            @if ($status == 1)
                                                <div>
                                                    @lang('auction.it_is_own_bidding')
                                                </div>
                                            @elseif ($status == 2)
                                                <div>
                                                    <span
                                                        style="color:#FF0000">@lang('auction.bidding_could_not_be_done')</span>
                                                    @if (strtotime($latComission . '+' . $supportMessageTime . ' hour') >= strtotime(date('Y-m-d H:i:s')))
                                                        <div>
                                                            {{ dateTimeFormat($latComission, 'Y年m月d日 H時i分') }}@lang('auction.other_companies_have_bid_on')
                                                        </div>
                                                    @endif
                                                </div>
                                            @elseif ($status == 3)
                                                <div>
                                                    @lang('auction.text_infor_expiration')<br>
                                                    @lang('auction.if_correspondence_is_possible_please_contact_directly')
                                                </div>
                                            @elseif ($status == 4)
                                                <div>
                                                    @lang('auction.it_is_a_bid_flow_project')
                                                </div>
                                            @elseif ($status == 5)
                                                <button id="support-button-{{$val->auction_info_id}}" type="button"
                                                        class="btn btn--gradient-orange supportButton" data-id="{{ $val->auction_info_id }}"
                                                        data-url="{{ route('auction.support', ['id' => $val->auction_info_id]) }}">@lang('auction.to_bid')
                                                </button>
                                                <button type="button" data-id="{{ $val->auction_info_id }}"
                                                        class="btn btn--gradient-green refusalButton"
                                                        id="page-data-{{ $val->auction_info_id }}"
                                                        data-url-refusal="{{ URL::route('auction.get.refusal', $val->auction_info_id) }}"
                                                        data-url-post-refusal="{{ URL::route('auction.post.refusal', $val->auction_info_id) }}">@lang('auction.to_withdraw')</button>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-mobi d-block d-xl-none">
                            <div class="content-table">
                                <div class="detail_box fs-11 m-2">
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.proposal_number')</div>
                                        <div class="detail_item_value">{{ $val->id }}</div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.site_name')</div>
                                        <div class="detail_item_value">
                                            <a href="http://{{ $val->site_url }}" target="_blank" class="text-success">{{ $val->site_name }}</a>
                                        </div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.agency_(receipt)_date_and_time')</div>
                                        <div class="detail_item_value">{{ dateTimeWeek($val->receive_datetime) }}</div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.date_and_time_of_visit')</div>
                                        <div class="detail_item_value">
                                            @php
                                                $isDisplay = false;
                                                $visitTimes = explode('|', $val->prop);
                                                if(count($visitTimes) == 4) {
                                                    $isDisplay = true;
                                                    $targetTime = $visitTimes[0];
                                                }
                                            @endphp
                                            @if($isDisplay)
                                                <p class="font-weight-bold text-danger mb-0">{{ ($visitTimes[1] == 0) ? __('auction.specify_time') : __('auction.time_to_adjust') }}</p>
                                                <p>{{ dateTimeWeek($visitTimes[0]) }}</p>
                                                @if($visitTimes[1] == 1)
                                                    @lang('common.wavy_seal')<br>
                                                    {{ dateTimeWeek($visitTimes[2]) }}
                                                    <br>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.telephone_date_time_desired')</div>
                                        <div class="detail_item_value">
                                            @php
                                                $isDisplayContactDesiredTime = false;
                                                if ($val->is_contact_time_range_flg == 0 && strlen($val->contact_desired_time) > 0) {
                                                    $isDisplayContactDesiredTime = true;
                                                    $targetTime = $val->contact_desired_time;
                                                }
                                            @endphp
                                            @if ($isDisplayContactDesiredTime)
                                                <p class="font-weight-bold mb-0 text-danger">@lang('auction.time_specification')</p>
                                                {{ dateTimeWeek($val->contact_desired_time) }}
                                            @endif
                                            @php
                                                $isDisplayContactDesiredTimeTo = false;
                                                if ($val->is_contact_time_range_flg == 1 && strlen($val->contact_desired_time_from) > 0) {
                                                    $isDisplayContactDesiredTimeTo = true;
                                                    $targetTime = $val->contact_desired_time_from;
                                                }
                                            @endphp
                                            @if ($isDisplayContactDesiredTimeTo)
                                                <p class="font-weight-bold mb-0 text-danger">@lang('auction.time_to_adjust')</p>
                                                {{ dateTimeWeek($val->contact_desired_time_from) }}
                                                <br>@lang('common.wavy_seal')<br>
                                                {{ dateTimeWeek($val->contact_desired_time_to) }}
                                            @endif
                                            @if (strlen($val->contact_desired_time) == 0 && strlen($val->contact_desired_time_from) == 0 && count($visitTimes) == 4)
                                                <p class="mb-0">{{ dateTimeWeek($visitTimes[3]) }}</p>
                                                @if (strlen($visitTimes[3]) > 0)
                                                    <p class="mb-0 font-weight-bold text-danger">
                                                        @lang('auction.after_bidding_confirm_the_contents')
                                                        , @lang('auction.upon_your_request_promptly')
                                                        , @lang('auction.call_and_decide_the_visit_time')
                                                        , @lang('auction.please')
                                                    </p>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.customer_name')</div>
                                        <div class="detail_item_value">{{ $val->customer_name }}</div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.contact')</div>
                                        <div class="detail_item_value">
                                            @if(!$isRoleAffiliation)
                                                <a href="callto:{{ $val->tel1 }}">{{ $val->tel1 }}</a><br>
                                            @else
                                                @php
                                                    $hour = 0;
                                                    $minute = 0;
                                                    if (isset($telDisclosure[$val->priority]['item_hour_date'])) {
                                                        $hour = $telDisclosure[$val->priority]['item_hour_date'];
                                                    }
                                                    if (isset($telDisclosure[$val->priority]['item_minute_date'])) {
                                                        $minute = $telDisclosure[$val->priority]['item_minute_date'];
                                                    }
                                                    if (!empty($hour)) {
                                                        $hour = $hour * 60;
                                                    }
                                                    $num = $hour + $minute;
                                                    $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                    $isCallToPhone = false;
                                                    if ((strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                        $val->auction_masking != $auctionMaskingWithout ||
                                                        ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        !empty($visitTimes[3])
                                                    ) && $status == 1 ) {
                                                        $isCallToPhone = true;
                                                    }
                                                @endphp
                                                @if ($isCallToPhone)
                                                    <a href="callto:{{ $val->tel1 }}">{{ $val->tel1 }}</a><br>
                                                @else
                                                    {{ !empty($val->tel1) ? substr_replace($val->tel1, "******", -6,6) : '' }}
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix detail_row border-bottom mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.address')</div>
                                        <div class="detail_item_value">
                                            <p class="mb-0">
                                                @if (!$isRoleAffiliation)
                                                    {{ $service->getDivTextJP('prefecture_div', $val->address1) }}{{ $val->address2 }}{{ $val->address3 }}
                                                    <br>
                                                @else
                                                    @php
                                                        $hour = 0;
                                                        $minute = 0;
                                                        if (isset($addressDisclosure[$val->priority]['item_hour_date'])) {
                                                            $hour = $addressDisclosure[$val->priority]['item_hour_date'];
                                                        }
                                                        if (isset($addressDisclosure[$val->priority]['item_minute_date'])) {
                                                            $minute = $addressDisclosure[$val->priority]['item_minute_date'];}
                                                        if (!empty($hour)) {
                                                            $hour = $hour * 60;
                                                        }
                                                        $num = $hour + $minute;
                                                        $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                        $isDisplayAddressFull = false;
                                                        if( (strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                            $val->auction_masking == $auctionMaskingAll ||
                                                            ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                            ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                            !empty($visitTimes[3])
                                                        ) && $status == 1 ) {
                                                            $isDisplayAddressFull = true;
                                                        }
                                                    @endphp
                                                    @if ($isDisplayAddressFull)
                                                        {{ $service->getDivTextJP('prefecture_div', $val->address1) }}{{ $val->address2 }}{{ $val->address3 }}
                                                        <br>
                                                    @else
                                                        {{ $service->getDivTextJP('prefecture_div', $val->address1) }}{{ $val->address2 }}{{ $service->maskingAddress3($val->address3) }}
                                                        <br>
                                                    @endif
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix detail_row mb-2">
                                        <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.building_type')</div>
                                        <div class="detail_item_value">
                                            <p class="mb-0">{{ $service->getDropText($buildingType, $val->construction_class) }}</p>
                                        </div>
                                    </div>
                                    
                                    <div class="bg--gray-light px-4 py-2">
                                        <div id="btnAnkenDetail-{{ $val->id }}" class="btnAnkenDetail" style="display:none"></div>
                                        @if ($status == 1)
                                        <div class="bg--gray-light">
                                            <div class="text-center">
                                                <button type="button" class="btn btn--gradient-default col-12 confirm-deal-details-btn" data-url="{{ route('auction.proposal.json', ['demandId' => $val->id]) }}">@lang('auction.confirm_deal_details')
                                                </button>
                                            </div>
                                            <div class="bg-white font-weight-bold text-center p-1">
                                                @lang('auction.it_is_own_bidding')
                                            </div>
                                        </div>
                                        @elseif ($status == 2)
                                        <div class="bg--gray-light">
                                            <div class="text-center mb-1">
                                                <button type="button" class="btn btn--gradient-default col-12 confirm-deal-details-btn" data-url="{{ route('auction.proposal.json', ['demandId' => $val->id]) }}">@lang('auction.confirm_deal_details')
                                                </button>
                                            </div>
                                            <div class="bg-white font-weight-bold text-center p-1">
                                                <span
                                                    style="color:#FF0000">@lang('auction.bidding_could_not_be_done')</span>
                                                @if (strtotime($latComission . '+' . $supportMessageTime . ' hour') >= strtotime(date('Y-m-d H:i:s')))
                                                    <div>
                                                        {{ dateTimeFormat($latComission, 'Y年m月d日 H時i分') }}@lang('auction.other_companies_have_bid_on')
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        @elseif ($status == 3)
                                        <div class="bg--gray-light">
                                            <div class="text-center mb-1">
                                                <button type="button" class="btn btn--gradient-default col-12 confirm-deal-details-btn" data-url="{{ route('auction.proposal.json', ['demandId' => $val->id]) }}">@lang('auction.confirm_deal_details')
                                                </button>
                                            </div>
                                            <div class="bg-white font-weight-bold text-center p-1">
                                                @lang('auction.text_infor_expiration')<br>
                                                @lang('auction.if_correspondence_is_possible_please_contact_directly')
                                            </div>
                                        </div>
                                        @elseif ($status == 4)
                                        <div class="bg--gray-light">
                                            <div class="text-center mb-1">
                                                <button type="button" class="btn btn--gradient-default col-12 confirm-deal-details-btn" data-url="{{ route('auction.proposal.json', ['demandId' => $val->id]) }}">@lang('auction.confirm_deal_details')
                                                </button>
                                            </div>
                                            <div class="bg-white font-weight-bold text-center p-1">
                                                @lang('auction.it_is_a_bid_flow_project')
                                            </div>
                                        </div>
                                        @elseif ($status == 5)
                                        <div class="bg--gray-light">
                                            <button id="support-button-{{$val->auction_info_id}}" type="button"
                                                    class="btn btn--gradient-orange supportButton" data-id="{{ $val->auction_info_id }}"
                                                    data-url="{{ route('auction.support', ['id' => $val->auction_info_id]) }}">@lang('auction.to_bid')
                                            </button>
                                            <button type="button" data-id="{{ $val->auction_info_id }}"
                                                    class="btn btn--gradient-green refusalButton"
                                                    id="page-data-{{ $val->auction_info_id }}"
                                                    data-url-refusal="{{ URL::route('auction.get.refusal', $val->auction_info_id) }}"
                                                    data-url-post-refusal="{{ URL::route('auction.post.refusal', $val->auction_info_id) }}">@lang('auction.to_withdraw')</button>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        @if ($isRoleAffiliation)
            <div class="kameiten-account">
                <div class="collapse-link">
                    <input type="hidden" value="0" id="affFirstClick"/>
                    <p class="font-weight-bold border-top pt-4 pb-xl-4" id="txt-lbl" data-toggle="collapse"
                       href="#collapse-content">@lang('auction.show_bid_completed_case')≫</p>
                </div>
                <div id="collapse-content" class="collapse">
                    {!! Form::open(['url' => route('auction.delete'), 'id' => 'auction-post-search-form']) !!}
                    @php
                        $sort = $dataSort['sort'];
                        $order = $dataSort['order'];
                    @endphp
                    <label class="collapse-label-mobi font-weight-bold p-2 w-100 text--white d-block d-xl-none" data-toggle="collapse" data-target="#table-group-kameiten-mobi" aria-expanded="false" aria-controls="table-group-kameiten-mobi">@lang('auction.latest_bid_deals_list')<i class="fa fa-chevron-down float-right" aria-hidden="true"></i></label>
                    <div id="table-group-kameiten-mobi" class="collapse show">
                        <div class="form-category d-none d-xl-block">
                            <label
                                class="form-category__label font-weight-light">@lang('auction.latest_bid_deals_list')</label>
                        </div>
                        <label class="bg-gray-light fs-11 d-block d-xl-none font-weight-bold p-1 pl-1 w-100">@lang('auction.attachment_file')</label>
                        <div class="file-upload mb-2 fs-8 d-block d-xl-none">
                            <span><i class="fa fa-file-o fa-lg" aria-hidden="true"
                                    title=""></i>: @lang('auction.with_attached_file')</span>
                        </div>
                        <label class="bg-gray-light fs-11 d-block d-xl-none font-weight-bold p-1 pl-1 w-100">@lang('auction.sort')</label>
                        <div class="clearfix sort-key-box fs-11 mt-4 d-inline d-xl-flex flex-xl-row">
                            <div class="py-1 px-2 d-none d-xl-block"><i class="fa fa-square" aria-hidden="true"></i> @lang('auction.sort')</div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'demand_id');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=demand_id&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.by_proposal_number')</a>
                            </div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'visit_time');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=visit_time&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.by_visit_date_and_time')</a>
                            </div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'contact_desired_time');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=contact_desired_time&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.by_phone_call_date_and_time_order')</a>
                            </div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link d-none d-xl-block">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'genre_name');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=genre_name&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.genre_order')</a>
                            </div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link d-none d-xl-block">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'customer_name');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=customer_name&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.by_customer_name')</a>
                            </div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link d-none d-xl-block">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'tel1');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=tel1&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.contact_sort')</a>
                            </div>
                            <div class="py-1 px-2 float-left mb-1 mb-xl-0 sort-link d-none d-xl-block">
                                @php
                                    $sortInfor = $service->getInforOrderSort($sort, $order, 'address');
                                @endphp
                                <a href="{{ route('auction.index') }}?sort=address&order={{ $sortInfor['order_display'] }}" class="text-dark">
                                    {{ $sortInfor['icon'] }}@lang('auction.sort_by_address')</a>
                            </div>
                        </div>
                        <div class="file-upload py-3 d-none d-xl-block">
                            <span><i class="fa fa-file-o fa-lg" aria-hidden="true"
                                    title=""></i>: @lang('auction.with_attached_file')</span>
                        </div>
                        <div class="table-responsive d-none d-xl-block">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="align-middle">@lang('auction.Delete')</th>
                                        <th class="align-middle">@lang('auction.proposal_number')</th>
                                        <th class="align-middle">@lang('auction.date_and_time_of_visit')</th>
                                        <th class="align-middle">@lang('auction.telephone_date_time_desired')</th>
                                        <th class="align-middle">@lang('auction.genre')</th>
                                        <th class="align-middle">@lang('auction.customer_name')</th>
                                        <th class="align-middle">@lang('auction.contact')</th>
                                        <th class="align-middle">@lang('auction.address')</th>
                                        <th class="align-middle">@lang('auction.building_type')</th>
                                        <th class="align-middle">@lang('auction.detail')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($auctionAlreadyData as $key => $val)
                                        @php
                                            $targetTime = '';
                                        @endphp
                                        <tr>
                                            <td>
                                                {!! Form::checkbox("id[]", $val->id, false) !!}
                                            </td>
                                            <td>{{ $val->demand_id }}</td>
                                            <td>
                                                @if ($val->is_visit_time_range_flg == 0)
                                                    {!! dateTimeWeek($val->visit_time, '%Y/%m/%d(%a)<br>%H:%M') !!}
                                                @endif
                                                @if ($val->is_visit_time_range_flg == 1 && strlen($val->visit_time_from) > 0)
                                                    {!! dateTimeWeek($val->visit_time_from, '%Y/%m/%d(%a)<br>%H:%M') !!}
                                                    <br>
                                                    <center>@lang('common.wavy_seal')</center>
                                                    {!! dateTimeWeek($val->visit_time_to, '%Y/%m/%d(%a)<br>%H:%M') !!}
                                                @endif
                                                @php
                                                    if (empty($targetTime)) {
                                                        $targetTime = ($val->is_visit_time_range_flg == 0) ? $val->visit_time : $val->visit_time_from;
                                                    }
                                                @endphp
                                            </td>
                                            <td>
                                                @if ($val->is_contact_time_range_flg == 0)
                                                    {!! dateTimeWeek($val->contact_desired_time, '%Y/%m/%d(%a)<br>%H:%M') !!}
                                                @endif
                                                @if ($val->is_contact_time_range_flg == 1 && strlen($val->contact_desired_time_from) > 0)
                                                    {!! dateTimeWeek($val->contact_desired_time_from, '%Y/%m/%d(%a)<br>%H:%M') !!}
                                                    <br>
                                                    <center>@lang('common.wavy_seal')</center>
                                                    {!! dateTimeWeek($val->contact_desired_time_to, '%Y/%m/%d(%a)<br>%H:%M') !!}
                                                @endif
                                                @php
                                                    if (empty($targetTime)) {
                                                        $targetTime = ($val->is_contact_time_range_flg == 0) ? $val->contact_desired_time : $val->contact_desired_time_from;
                                                    }
                                                @endphp
                                            </td>
                                            <td>{{ $val->genre_name }}</td>
                                            <td>{{ $val->customer_name }}</td>
                                            <td>
                                                @php
                                                    $hour = 0;
                                                    $minute = 0;
                                                    if (isset($telDisclosure[$val->priority]['item_hour_date'])) {
                                                        $hour = $telDisclosure[$val->priority]['item_hour_date'];
                                                    }
                                                    if (isset($telDisclosure[$val->priority]['item_minute_date'])) {
                                                        $minute = $telDisclosure[$val->priority]['item_minute_date'];
                                                    }
                                                    if (!empty($hour)) {
                                                        $hour = $hour * 60;
                                                    }
                                                    $num = $hour + $minute;
                                                    $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                    $isDisplayTelFull = false;
                                                    if(strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                        $val->auction_masking != $auctionMaskingWithout ||
                                                        ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        isset($val->visit_adjust_time)
                                                    ) {
                                                        $isDisplayTelFull = true;
                                                    }
                                                @endphp
                                                @if ($isDisplayTelFull)
                                                    <a href="callto:{{ $val->tel1 }}">{{ $val->tel1 }}</a><br>
                                                @else
                                                    {{ !empty($val->tel1) ? substr_replace($val->tel1, "******", -6,6) : '' }}
                                                @endif
                                            </td>
                                            <td>
                                                @php
                                                    $hour = 0;
                                                    $minute = 0;
                                                    if (isset($addressDisclosure[$val->priority]['item_hour_date'])) {
                                                        $hour = $addressDisclosure[$val->priority]['item_hour_date'];
                                                    }
                                                    if (isset($addressDisclosure[$val->priority]['item_minute_date'])) {
                                                        $minute = $addressDisclosure[$val->priority]['item_minute_date'];
                                                    }
                                                    if (!empty($hour)) {
                                                        $hour = $hour * 60;
                                                    }
                                                    $num = $hour + $minute;
                                                    $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                    $isDisplayAddressFull = false;
                                                    $auctionMaskingAll = $service->getDivValue("auction_masking" , "all_exclusion");
                                                    $auctionMaskingWithout = $service->getDivValue("auction_masking" , "without");
                                                    if(strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                        $val->auction_masking == $auctionMaskingAll ||
                                                        ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        isset($val->visit_adjust_time)
                                                    ) {
                                                        $isDisplayAddressFull = true;
                                                    }
                                                @endphp
                                                {{ $service->getDivTextJP('prefecture_div', $val->address1) }}
                                                {{ $val->address2 }}
                                                @if ($isDisplayAddressFull)
                                                    {{ $val->address3 }}
                                                @else
                                                    {{ $service->maskingAddress3($val->address3) }}
                                                @endif
                                            </td>
                                            <td>{{ $service->getDropText($buildingType, $val->construction_class) }}</td>
                                            <td>
                                                <button
                                                    class="btn btn--gradient-default font-weight-bold border--btn-gray"><a
                                                        href="commission/detail/{{ $val->id }}"
                                                        target="_blank">@lang('auction.detailed_confirmation_report')</a>
                                                </button>
                                                @if (!empty($val->demand_attached_files_demand_id))
                                                    <img src="/img/file_ico.jpg" alt="@lang('auction.with_attached_file')" title="@lang('auction.with_attached_file')"
                                                        style="width: 15px">
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if (count($auctionAlreadyData) > 0)
                                {{ $auctionAlreadyData->links('auction.component.pagination') }}
                            @endif
                        </div> 
                        <div class="table-mobi d-block d-xl-none">
                            @foreach($auctionAlreadyData as $key => $val)
                                @php
                                    $targetTime = '';
                                @endphp
                                <div class="content-table">
                                    <div class="commission_info_title mt-4">
                                        <span>@lang('auction.date_and_time_of_visit') －</span>
                                        {!! Form::checkbox("id[]", $val->id, false, ['id' => 'delete-' . $key, 'class' => 'checkbox-delete float-right']) !!}
                                        {{Form::label('delete-'.$key, __('auction.Delete'), ['class' => 'mb-0 mr-2 float-right'])}}
                                    </div>
                                    <div class="detail_box fs-11 m-2">
                                        <div class="clearfix detail_row border-bottom mb-2">
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.proposal_number')</div>
                                            <div class="detail_item_value">{{ $val->demand_id }}</div>
                                        </div>
                                        <div class="clearfix detail_row border-bottom mb-2">
                                            <p class="mb-0 text-danger font-weight-bold">時間指定</p>
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.telephone_date_time_desired')</div>
                                            <div class="detail_item_value">
                                                @if ($val->is_contact_time_range_flg == 0)
                                                    {!! dateTimeWeek($val->contact_desired_time, '%Y/%m/%d(%a) &nbsp %H:%M') !!}
                                                @endif
                                                @if ($val->is_contact_time_range_flg == 1 && strlen($val->contact_desired_time_from) > 0)
                                                    {!! dateTimeWeek($val->contact_desired_time_from, '%Y/%m/%d(%a) &nbsp %H:%M') !!}
                                                    &nbsp
                                                    <center>@lang('common.wavy_seal')</center>
                                                    {!! dateTimeWeek($val->contact_desired_time_to, '%Y/%m/%d(%a)&nbsp %H:%M') !!}
                                                @endif
                                                @php
                                                    if (empty($targetTime)) {
                                                        $targetTime = ($val->is_contact_time_range_flg == 0) ? $val->contact_desired_time : $val->contact_desired_time_from;
                                                    }
                                                @endphp
                                            </div>
                                        </div>
                                        <div class="clearfix detail_row border-bottom mb-2">
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.genre')</div>
                                            <div class="detail_item_value">{{ $val->genre_name }}</div>
                                        </div>
                                        <div class="clearfix detail_row border-bottom mb-2">
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.customer_name')</div>
                                            <div class="detail_item_value">{{ $val->customer_name }}</div>
                                        </div>
                                        <div class="clearfix detail_row border-bottom mb-2">
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.contact')</div>
                                            <div class="detail_item_value">
                                                @php
                                                    $hour = 0;
                                                    $minute = 0;
                                                    if (isset($telDisclosure[$val->priority]['item_hour_date'])) {
                                                        $hour = $telDisclosure[$val->priority]['item_hour_date'];
                                                    }
                                                    if (isset($telDisclosure[$val->priority]['item_minute_date'])) {
                                                        $minute = $telDisclosure[$val->priority]['item_minute_date'];
                                                    }
                                                    if (!empty($hour)) {
                                                        $hour = $hour * 60;
                                                    }
                                                    $num = $hour + $minute;
                                                    $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                    $isDisplayTelFull = false;
                                                    if(strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                        $val->auction_masking != $auctionMaskingWithout ||
                                                        ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        isset($val->visit_adjust_time)
                                                    ) {
                                                        $isDisplayTelFull = true;
                                                    }
                                                @endphp
                                                @if ($isDisplayTelFull)
                                                    <a href="callto:{{ $val->tel1 }}">{{ $val->tel1 }}</a><br>
                                                @else
                                                    {{ !empty($val->tel1) ? substr_replace($val->tel1, "******", -6,6) : '' }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="clearfix detail_row border-bottom mb-2">
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.address')</div>
                                            <div class="detail_item_value">
                                                @php
                                                    $hour = 0;
                                                    $minute = 0;
                                                    if (isset($addressDisclosure[$val->priority]['item_hour_date'])) {
                                                        $hour = $addressDisclosure[$val->priority]['item_hour_date'];
                                                    }
                                                    if (isset($addressDisclosure[$val->priority]['item_minute_date'])) {
                                                        $minute = $addressDisclosure[$val->priority]['item_minute_date'];
                                                    }
                                                    if (!empty($hour)) {
                                                        $hour = $hour * 60;
                                                    }
                                                    $num = $hour + $minute;
                                                    $targetDate = date("Y-m-d H:i:s", strtotime($targetTime . "-" . $num . " minute"));
                                                    $isDisplayAddressFull = false;
                                                    $auctionMaskingAll = $service->getDivValue("auction_masking" , "all_exclusion");
                                                    $auctionMaskingWithout = $service->getDivValue("auction_masking" , "without");
                                                    if(strtotime($targetDate) <= strtotime(date("Y-m-d H:i:s")) ||
                                                        $val->auction_masking == $auctionMaskingAll ||
                                                        ($val->contact_desired_time != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        ($val->contact_desired_time_from != '' && $val->auction_masking == $auctionMaskingWithout) ||
                                                        isset($val->visit_adjust_time)
                                                    ) {
                                                        $isDisplayAddressFull = true;
                                                    }
                                                @endphp
                                                {{ $service->getDivTextJP('prefecture_div', $val->address1) }}
                                                {{ $val->address2 }}
                                                @if ($isDisplayAddressFull)
                                                    {{ $val->address3 }}
                                                @else
                                                    {{ $service->maskingAddress3($val->address3) }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="clearfix detail_row mb-2">
                                            <div class="detail_item_title font-weight-bold border-left-orange pl-1">@lang('auction.building_type')</div>
                                            <div class="detail_item_value">{{ $service->getDropText($buildingType, $val->construction_class) }}</div>
                                        </div>
                                        <div class="button_block text-center py-3">
                                            <a href="commission/detail/{{ $val->id }}" target="_blank" class="btn btn--gradient-orange text-white">@lang('auction.detailed_confirmation_report')</a>
                                                @if (!empty($val->demand_attached_files_demand_id))
                                                    <img src="/img/file_ico.jpg" alt="@lang('auction.with_attached_file')" title="@lang('auction.with_attached_file')"
                                                        style="width: 15px">
                                                @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="text-center py-5">
                            {!! Form::submit(__('auction.delete_checked_items'),['id' => 'delete-auction', 'class' => 'btn btn--gradient-default auction-search-border font-weight-bold col-md-3', 'name' => 'delete']); !!}
                        </div>
                    </div>
                    {{ Form::close() }}

                    <div id="table-calendar" class="justify-content-center flex-column flex-md-row p-3" hidden>
                        <span><i class="fa fa-chevron-left fa-4x getCalendar" aria-hidden="true"
                                data-next="0"></i></span>
                        <span><i class="fa fa-chevron-right fa-4x getCalendar" aria-hidden="true"
                                data-next="1"></i></span>
                    </div> 
                </div>
            </div>
        @endif
    </div>
    <div id="temp-refusal" data-id="1"></div>
    @component('auction.refusal')@endcomponent
    <div class="modal fade" id="seikatu_info_dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body">
                    <p style="text-align:center;font-size:18px;font-weight: bold;">@lang('auction.what_is_a_living_ambulance_case')
                        <br/></p>
                    <p style="text-align:left;margin-left:40px;">
                        @lang('auction.company_info')<br>
                        @lang('auction.rushing_support_service')<br>
                        @lang('auction.unlike_company')<br>
                        @lang('auction.customers')<br><br>
                        @lang('auction.free')<br>
                        @lang('auction.text_respond')<br><br>
                        @lang('auction.correspondence')<br>
                        @lang('auction.i_will')<br><br>
                        @lang('auction.copy_of_the_receipt')<br>
                        @lang('auction.we_spend_it')<br>
                        @lang('auction.there_are_some_conditions_in_the_commission_rate_as_well')<br>
                        @lang('auction.please_contact_us_if_you_are_interested')<br>
                    </p>
                    <p style="margin-bottom:20px;padding:0;text-align:center;color:#ffffff;">
                        <button type="button" class="btn_green_s w200 over"
                                onclick="seikatu_info_dialog_close()">@lang('auction.close')</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @include('auction.component.modal-list-event')
    <!-- Modal -->
    <div class="modal fade" id="supportModal" tabindex="-1" role="dialog" aria-labelledby="refusalModalLabel" data-backdrop="static" data-keyboard="false"
         aria-hidden="true">
        <div class="modal-dialog fix-w-610" role="document">
            <div class="modal-content fix-w-645">
                <div class="modal-body">
                    <div class="auction-support-content">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="auctionDetailModal" tabindex="-1" role="dialog" aria-labelledby="auctionDetailModal" data-keyboard="false"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark text-white">
                    <h5 class="mb-0">@lang('auction.deal_details')</h5>
                </div>
                <div class="modal-body">
                    <div class="auction-support-content">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn--gradient-default col-12">@lang('auction.close')</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ mix('js/lib/jquery.multiselect.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.multiselect.filter.js') }}"></script>
    <script src="{{ mix('js/utilities/st.common.js') }}"></script>
    
    <script src="{{ mix('js/lib/jquery-ui-timepicker-addon.js') }}"></script>
    <script src="{{ mix('js/pages/helpers/datetime.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.validate.min.js') }}"></script>
    <script src="{{ mix('js/lib/localization/jquery.validate.messages_ja.js') }}"></script>
    <script src="{{ mix('js/lib/additional-methods.min.js') }}"></script>
    <script src="{{ mix('js/utilities/form.validate.js') }}"></script>
    <script src="{{ mix('js/pages/auction.refusal.js') }}"></script>
    <script src="{{ mix('js/pages/support.js') }}"></script>

    <script type="text/javascript">
        var d = new Date();
        var currentMonth = d.getMonth() + 1;
        var currentYear = d.getFullYear();
        var cals_id = ["cell-cal1", "cell-cal2", "cell-cal3"];
        var urlGetCalenderView = "{{ route('ajax.get.calender.view') }}";
        var currentDate = new Date();
        var calendarEventData = {!! (!isset($calendarEventData)) ? '[]' : $calendarEventData !!};
        var un_select = "@lang('auction.un_select')";
        var check_all = "@lang('auction.check_all')";
        var un_check_all = "@lang('auction.un_check_all')";
        var urlProposalJson = "{{ route('auction.proposal.json', ['demandId' => '']) }}";
        var isRoleAffiliation = '{{ $isRoleAffiliation ? true : false }}';
        var urlUpdateJbrStatus = "{{route('auction.support.updateJbrStatus')}}";
        var urlComplete = "{{route('auction.handle.complete')}}";
        var screen_complete = "{{config('constant.refusal.complete')}}";
        var urlPostData = "{{route('auction.handle.support')}}";
    </script>
    <script src="{{ mix('js/pages/auction_search.js') }}"></script>
@endsection
