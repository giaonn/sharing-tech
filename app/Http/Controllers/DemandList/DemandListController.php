<?php

namespace App\Http\Controllers\DemandList;

use App\Http\Controllers\Controller;
use App\Http\Requests\Demand\DemandAttachedFileRequest;
use App\Repositories\Eloquent\MItemRepository;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Models\DemandInfo;
use App\Services\Demand\BusinessService;
use App\Services\UploadFile\UploadFile;
use Aws\CloudFront\Exception\Exception;
use Illuminate\Http\Request;
use App\Services\ExportService;
use App\Http\Requests\DemandListRequest;
use App\Services\Demand\DemandInfoService;
use App\Services\MItemService;
use DB;
use Auth;
use App\Services\DemandService;
use App\Services\DemandListDetailService;

class DemandListController extends Controller
{
    /**
     * @var boolean
     */
    public $defaultDisplay = false;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var DemandInfoRepositoryInterface
     */
    public $demandinfoRepository;
    /**
     * @var DemandInfoService
     */
    public $demandInfoService;
    /**
     * @var MItemService
     */
    public $mItemService;
    /**
     * @const NON_NOTIFICATION
     */
    const NON_NOTIFICATION = '非通知';
    /**
     * @var DemandService
     */
    protected $demandService;
    /**
     * @var DemandListDetailService
     */
    protected $demandListDetailService;
    /**
     * @var ExportService
     */
    protected $exportService;
    /**
     *
     * @var $siteIdEnable
     */
    public static $siteIdEnable = [861, 863, 889, 890, 1312, 1313, 1314];
    /**
     * @var array
     */
    public $additionalSearchParams = [];

    /**
     * DemandListController constructor.
     * @param Request $request
     * @param DemandInfoRepositoryInterface $demandinfoRepository
     * @param DemandInfoService $demandInfoService
     * @param MItemService $mItemService
     * @param DemandService $demandService
     * @param ExportService $exportService
     * @param DemandListDetailService $demandListDetailService
     */
    public function __construct(
        Request $request,
        DemandInfoRepositoryInterface $demandinfoRepository,
        DemandInfoService $demandInfoService,
        MItemService $mItemService,
        BusinessService $demandService,
        ExportService $exportService,
        DemandListDetailService $demandListDetailService
    ) {
        parent::__construct();
        $this->request = $request;
        $this->demandinfoRepository = $demandinfoRepository;
        $this->demandInfoService = $demandInfoService;
        $this->mItemService = $mItemService;
        $this->demandService = $demandService;
        $this->exportService = $exportService;
        $this->demandListDetailService = $demandListDetailService;
    }

    /**
     * index function
     *
     * @param  Request $request
     * @return view
     */
    public function index(Request $request)
    {
        $this->defaultDisplay = true;
        $data = $request->query();
        if ($request->isMethod('get')) {
            if (!empty($data['cti'])) {
                $sessionKeyForAffiliationSearch = self::$sessionKeyForAffiliationSearch;
                $sessionKeyForDemandSearch = self::$sessionKeyForDemandSearch;
                $this->demandInfoService->checkAffiliationResults($request, $data, $sessionKeyForAffiliationSearch, $sessionKeyForDemandSearch);
            } elseif (!empty($data['customer_tel'])) {
                $request->session()->put(self::$sessionKeyForDemandSearch, $data);
                return redirect()->route('demandlist.search');
            }
        }
        return view(
            'demand_list.index',
            [
            "defaultDisplay" => $this->defaultDisplay,
            'siteLists' => $this->demandListDetailService->mSiteRepo->getList(),
            'itemLists' => $this->mItemService->prepareDataList($this->demandListDetailService->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS))
            ]
        );
    }

    /**
     * search demand function
     * @param null $id
     * @param bool $bCheck
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($id = null, $bCheck = false)
    {
        $data = [];

        if (!empty($id)) {
            $listCorp = $this->getMCorp($id);
            if (!empty($listCorp)) {
                $this->request->merge(
                    [
                        'corp_name' => $listCorp['corp_name'],
                        'corp_name_kana' => $listCorp['corp_name_kana']
                    ]
                );
            }
            $data = $this->demandListPost();
        }
        try {
            if ($this->request->isMethod('get')) {
                $data = $this->demandListGet();
            }
        } catch (Exception $e) {
            abort('404');
        }
        $this->setParameterSession(); //set parameter session
        // update b check
        $data = $this->unifyBCheck($data, $bCheck);
        $demandInfos = $this->demandinfoRepository->getDemandInfo($data);
        return view('demand_list.index', [
            "defaultDisplay" => $this->defaultDisplay,
            "demandInfos" => $demandInfos,
            "auth" => Auth::user()->auth,
            'siteLists' => $this->demandListDetailService->mSiteRepo->getList(),
            'itemLists' => $this->mItemService->prepareDataList($this->demandListDetailService->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS)),
            'conditions' => $data
            ]);
    }

    /**
     * save session new data $sessionKeyForDemandParameter
     */
    private function setParameterSession()
    {
        $this->request->session()->forget(self::$sessionKeyForDemandParameter);
        $this->request->session()->put(self::$sessionKeyForDemandParameter, $this->request->get('named'));
    }

    /**
     * @param DemandListRequest $demandrequest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchDemand(DemandListRequest $demandrequest)
    {
        try {
            if ($demandrequest->isMethod('post')) {
                $dataRequest = $demandrequest->all();
                if (!empty($dataRequest['data']['corp_id'])) {
                    $listCorp = $this->getMCorp($dataRequest['data']['corp_id']);
                    if (!empty($listCorp)) {
                        $dataRequest['data']['corp_name'] = $listCorp['corp_name'];
                        $dataRequest['data']['corp_name_kana'] = $listCorp['corp_name_kana'];
                        $this->request->merge($dataRequest);
                    }
                }
                $data = $this->demandListPost();
            }
        } catch (Exception $e) {
            abort('404');
        }
        if ($demandrequest->input('submit') != '' && $demandrequest->input('submit') == 'csv' && (Auth::user()->auth == 'system'
            || Auth::user()->auth == 'admin' || Auth::user()->auth == 'accounting_admin')
        ) {
            $dataList = $this->demandInfoService->convertDataCsv($this->demandinfoRepository->getDataCsv($data));
            $fileName = trans('demandlist.csv_demand_name') . '_' . Auth::user()->user_id;
            $fieldList = DemandInfo::csvFormat();
            return $this->exportService->exportCsv($fileName, $fieldList['default'], $dataList);
        } else {
            $demandInfos = $this->demandinfoRepository->getDemandInfo($data);
            if ($demandrequest->ajax()) {
                return view(
                    'demand_list.demand_list_table',
                    [
                        "defaultDisplay" => $this->defaultDisplay,
                        "demandInfos" => $demandInfos,
                        "auth" => Auth::user()->auth,
                        'siteLists' => $this->demandListDetailService->mSiteRepo->getList(),
                        'itemLists' => $this->mItemService->prepareDataList($this->demandListDetailService->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS)),
                        'conditions' => $data
                    ]
                );
            }
            //Sanitize::clean before show
            return view(
                'demand_list.index',
                [
                "defaultDisplay" => $this->defaultDisplay,
                "demandInfos" => $demandInfos,
                "auth" => Auth::user()->auth,
                'siteLists' => $this->demandListDetailService->mSiteRepo->getList(),
                'itemLists' => $this->mItemService->prepareDataList($this->demandListDetailService->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS)),
                'conditions' => $data
                ]
            );
        }
    }

    /**
     * get corp by id function
     *
     * @param  integer $id
     * @return array
     */
    private function getMCorp($id = null)
    {
        $this->additionalSearchParams['corp_id'] = $id;
        $result = $this->demandListDetailService->mCorpRepo->getFirstById($id, true);
        if (!empty($result)) {
            $this->additionalSearchParams['corp_name'] = $result['corp_name'];
            $this->additionalSearchParams['corp_name_kana'] = $result['corp_name_kana'];
        }
        return $result;
    }

    /**
     * set again data request post function
     *
     * @return array
     */
    private function demandListPost()
    {

        $dataRequest = $this->request->all();
        $data = $dataRequest;
        if (isset($dataRequest['data'])) {
            $data = $dataRequest['data'];
            if (isset($data['b_check']) && $data['b_check'] == 'on') {
                $data['demand_status'] = [3];
            }
        }
        if (isset($dataRequest['sortName']) && isset($dataRequest['orderBy'])) {
            $data['sort'] = $dataRequest['sortName'];
            $data['direction'] = $dataRequest['orderBy'];
        }
        unset($data['csv_out']);
        $this->request->session()->forget(self::$sessionKeyForDemandSearch);
        $this->request->session()->put(self::$sessionKeyForDemandSearch, $data);

        return $data;
    }

    /**
     * set again data request get function
     *
     * @return array
     */
    private function demandListGet()
    {
        $data = $this->request->session()->get(self::$sessionKeyForDemandSearch);
        if (!empty($data)) {
            $data += ['page' => $this->request->input('page')];
        }
        return $data;
    }

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @param null    $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetail($id)
    {
        $demand = $this->demandinfoRepository->getDemandByIdWithRelations($id);
        if (!$demand) {
            return redirect()->route('demand.get.create');
        }

        $customerTel = $this->demandinfoRepository->checkIdenticallyCustomer($demand->customer_tel);
        $userDropDownList = $this->demandListDetailService->mUserRepo->getListUserForDropDown();
        $genresDropDownList = $this->demandListDetailService->mSiteGenresRepo->getGenreBySiteStHide($demand->site_id);

        $categoriesDropDownList = $this->demandListDetailService->mCategoryRepo->getListCategoriesForDropDown($demand->genre_id);
        $mSiteDropDownList = $this->demandListDetailService->mSiteRepo->getListMSitesForDropDown();
        $mSiteGenresDropDownList = $this->demandListDetailService->mSiteGenresRepo->getMSiteGenresDropDownBySiteId($demand->site_id);
        $mItemsDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(BUILDING_TYPE);
        $prefectureDiv = $this->demandInfoService->translatePrefecture();
        $selectionSystemList = $this->demandInfoService->getSelectionSystemList($demand);

        $priorityDropDownList = $this->demandInfoService->getPriorityTranslate();
        $stClaimDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(REQUEST_ST);
        $jbrWorkContentDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(JBR_WORK_CONTENTS);
        $specialMeasureDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(PROJECT_SPECIAL_MEASURES);
        $demandStatusDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(PROPOSAL_STATUS);
        $orderFailReasonDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(REASON_FOR_LOST_NOTE);
        $acceptanceStatusDropDownList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(ACCEPTANCE_STATUS);
        $quickOrderFailReasonList = $this->demandListDetailService->mItemRepo->getMItemListByItemCategory(REASON_MISSING_CONTACT);
        $vacationLabels = $this->demandListDetailService->mItemRepo->getMItemList(LONG_HOLIDAYS, date('Y/m/d'));

        $rankList = $this->demandService->getGenreByRank($demand->site_id);
        $enableSiteId = in_array($demand->site_id, [861, 863, 889, 890, 1312, 1313, 1314]);
        return view('demand.detail', [
            'demand' => $demand,
            'userDropDownList' => $userDropDownList,
            'genresDropDownList' => $genresDropDownList,
            'categoriesDropDownList' => $categoriesDropDownList,
            'mSiteDropDownList' => $mSiteDropDownList,
            'mSiteGenresDropDownList' => $mSiteGenresDropDownList,
            'mItemsDropDownList' => $mItemsDropDownList,
            'prefectureDiv' => $prefectureDiv,
            'selectionSystemList' => $selectionSystemList,
            'priorityDropDownList' => $priorityDropDownList,
            'stClaimDropDownList' => $stClaimDropDownList,
            'jbrWorkContentDropDownList' => $jbrWorkContentDropDownList,
            'specialMeasureDropDownList' => $specialMeasureDropDownList,
            'demandStatusDropDownList' => $demandStatusDropDownList,
            'orderFailReasonDropDownList' => $orderFailReasonDropDownList,
            'acceptanceStatusDropDownList' => $acceptanceStatusDropDownList,
            'quickOrderFailReasonDropDownList' => $quickOrderFailReasonList,
            'vacationLabels' => $vacationLabels,
            'rankList' => $rankList,
            'customerTel' => $customerTel,
            'enableSiteId' => $enableSiteId
        ]);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function deleteAttachedFile($id)
    {
        $demandAttachedFile = $this->demandListDetailService->demandAttachedFileRepo->findId($id);
        $demandAttachedFile->delete();

        return redirect()->back()->withMessage(__('demand.delete_file_success'));
    }

    /**
     * @param DemandAttachedFileRequest $request
     * @param null                      $demandId
     * @return $this
     * @throws \Exception
     * @throws \Throwable
     */
    public function uploadAttachedFile(DemandAttachedFileRequest $request, $demandId = null)
    {
        if (!$request->hasFile('demand_attached_file')) {
            return redirect()->back()->with('error_msg_input',__('demand.the_file_field_is_required'));
        }

        $uploadFiles = $request->file('demand_attached_file');

        foreach ($uploadFiles as $key => $file) {
            if(!$request->hasFile('demand_attached_file['. $key .']')) continue;
            DB::transaction(
                function () use ($demandId, $request, $file, $key) {
                    try {
                        $currentDate = date('Y-m-d H:i:s');
                        $userId = Auth::user()->id;
                        $pathUpload = storage_path('upload/' . $demandId);

                        $nextIndex = $this->demandListDetailService->demandAttachedFileRepo->getNextIndex();
                        $fileUpload = new UploadFile($file, $pathUpload, $nextIndex);

                        $demandAttachedFile = $this->demandListDetailService->demandAttachedFileRepo->create(
                            [
                            'id' => $nextIndex,
                            'demand_id' => $demandId,
                            'path' => '',
                            'name' => $fileUpload->getOriginalName(),
                            'content_type' => $fileUpload->getMiMeType(),
                            'create_date' => $currentDate,
                            'create_user_id' => $userId,
                            'update_date' => $currentDate,
                            'update_user_id' => $userId
                            ]
                        );
                        $demandAttachedFile->path = $pathUpload . '/' . $fileUpload->upload();
                        $demandAttachedFile->save();
                    } catch (Exception $e) {
                        return redirect()->withMessage('has something wrong with upload file ' . $key . '');
                    }
                }
            );
        }
        return redirect()->back()->withMessage(__('demand.upload_file_success'));
    }

    /**
     * @param $data
     * @param $bCheck
     * @return array
     */
    private function unifyBCheck($data, $bCheck)
    {
        if ($bCheck || (isset($data['b_check']) && $data['b_check'])) {
            $bCheck = true;
        } else {
            $bCheck = false;
        }
        $data['b_check'] = $bCheck;
        $this->additionalSearchParams['b_check'] = $bCheck;
        if (isset($data['corp_id']) && $data['corp_id']) {
            $this->getMCorp($data['corp_id']);
        }
        if ($data['b_check']) {
            $demandStatus = strval(getDivValue('demand_status', 'agency_before'));
            $this->additionalSearchParams['demand_status'] = [$demandStatus];
        }
        $this->request->merge($this->additionalSearchParams);
        $data = array_merge($data, $this->additionalSearchParams);
        $this->request->session()->forget(self::$sessionKeyForDemandSearch);
        $this->request->session()->put(self::$sessionKeyForDemandSearch, $data);
        return $data;
    }
}
