<?php

namespace App\Http\Controllers\Commission;

use App\Http\Controllers\Controller;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Services\CommissionPrintService;
use Illuminate\Support\Facades\Response;

class CommissionPrintController extends Controller
{
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepo;

    /**
     * @var CommissionPrintService
     */
    protected $commissionPrintService;

    /**
     * CommissionPrintController constructor.
     *
     * @param CommissionInfoRepositoryInterface $commissionInfoRepository
     * @param CommissionPrintService            $commissionPrintService
     */
    public function __construct(
        CommissionInfoRepositoryInterface $commissionInfoRepository,
        CommissionPrintService $commissionPrintService
    ) {
        parent::__construct();
        $this->commissionInfoRepo = $commissionInfoRepository;
        $this->commissionPrintService = $commissionPrintService;
    }

    /**
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param $demandId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($demandId)
    {
        $results = $this->commissionInfoRepo->getCommInfoWithCorpByDemandId(
            $demandId,
            [
            "id",
            "corp_id",
            ],
            ["id", "official_corp_name"]
        );

        return view("commission_print.detail", ["results" => $results]);
    }

    /**
     * @param $commissionId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function exportWord($commissionId)
    {
        $makeFile = "";
        $fileName = $this->commissionPrintService->exportWord($commissionId, $makeFile);
        $headers = [
            'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ];

        return Response::download($makeFile, $fileName, $headers);
    }
}
