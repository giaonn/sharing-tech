@extends('layouts.app')
@section('style')
    <link href="{{ mix('css/lib/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="demand-list">
        <form id="demand_info" class="form-horizontal fieldset-custom" method="POST"
              action="{{ route('demandlist.post.search') }}">
            {{ csrf_field() }}
            <fieldset>
                <legend class="fs-13">{{ trans('demandlist.search_condition') }}</legend>
                <div class="form-container box--bg-gray box--border-gray p-2">
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0 mb-2 mb-sm-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.company_name') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[corp_name]', isset($conditions['corp_name']) ? $conditions['corp_name'] : null, ['id' => 'corp_name', 'class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.company_name_furigana') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[corp_name_kana]', isset($conditions['corp_name_kana']) ? $conditions['corp_name_kana'] : null, ['id' => 'corp_name_kana', 'class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0 mb-2 mb-sm-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.customer_phone_number') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[customer_tel]', isset($conditions['customer_tel']) ? $conditions['customer_tel'] : null, ['id' => 'customer_tel', 'class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.customer_name') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[customer_name]', null, ['id' => 'customer_name', 'class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0 mb-2 mb-sm-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.opportunity_id') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[id]', null, ['id' => 'id', 'class' => 'form-control', 'data-rule-number' => 'true'])}}
                                    @if ($errors->has('data.id'))
                                        <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.id')}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.proposal_status') }}</label>
                                </div>
                                <div class="col-sm-7 px-0">
                                    {{Form::select('data[demand_status][]',$itemLists, isset($conditions['demand_status']) ? $conditions['demand_status'] : null,['id'=>'demand_status', 'multiple'=>'multiple', 'class'=>'multiple_check_filter', 'style' => 'display:none'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0 mb-2 mb-sm-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.site_name') }}</label>
                                </div>
                                <div class="col-sm-7 px-0">
                                    {{Form::select('data[site_id][]',$siteLists, isset($conditions['site_id']) ? $conditions['site_id'] : null,['id'=>'site_id', 'multiple'=>'multiple', 'class'=>'multiple_check_filter w-100' , 'style' => 'display:none'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.JBR_reception_like_no') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[jbr_order_no]', null, ['id' => 'jbr_order_no', 'class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0 mb-2 mb-sm-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.contact_deadline_date_and_time') }}</label>
                                </div>
                                <div class="col-sm-8 px-0 pr-sm-3">
                                    <div class="row mx-0">
                                        <div class="col-sm-5 px-0 mb-1 mb-sm-0">
                                            <input name="data[from_contact_desired_time]"
                                                   class="form-control datetimepicker w-100"
                                                   type="text"
                                                   id="from_contact_desired_time"
                                                   value="{{ isset($conditions['from_contact_desired_time']) ? $conditions['from_contact_desired_time'] : '' }}"
                                                   data-rule-lessThanTime="#to_contact_desired_time">
                                            @if ($errors->has('data.from_contact_desired_time'))
                                                <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.from_contact_desired_time')}}</p>
                                            @endif
                                        </div>
                                        <div class="col-sm-1 d-none d-sm-block px-0 fs-20 text-center">&#x223C;</div>
                                        <div class="col-sm-5 px-0">
                                            <input name="data[to_contact_desired_time]"
                                                   class="form-control datetimepicker w-100"
                                                   type="text"
                                                   id="to_contact_desired_time"
                                                   value="{{ isset($conditions['to_contact_desired_time']) ? $conditions['to_contact_desired_time'] : '' }}">
                                            @if ($errors->has('data.to_contact_desired_time'))
                                                <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.to_contact_desired_time')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0" for="b_check">{{ trans('demandlist.b_check') }}</label>
                                </div>
                                <div class="col-sm-7 custom-checkbox custom-control d-flex align-items-center">
                                    {{Form::input('checkbox', 'data[b_check]', null, ['id' => 'b_check', 'class' => 'custom-control-input ignore', (isset($conditions['b_check']) && $conditions['b_check']=='on') ? 'checked' : ''])}}
                                    <label class="custom-control-label custome-label" for="b_check"><span
                                                class="d-none">a</span></label>
                                    <input class="d-none d-lg-inline-block opacity-0 form-control col-1" tabindex="-1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0 mb-2 mb-sm-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.reception_date_and_time') }}</label>
                                </div>
                                <div class="col-sm-8 px-0 pr-sm-3">
                                    <div class="row mx-0">
                                        <div class="col-sm-5 px-0 mb-1 mb-sm-0">
                                            <input name="data[from_receive_datetime]"
                                                   class="form-control datetimepicker w-100"
                                                   type="text"
                                                   id="from_receive_datetime"
                                                   value="{{ isset($conditions['from_receive_datetime']) ? $conditions['from_receive_datetime'] : '' }}"
                                                   data-rule-lessThanTime="#to_receive_datetime">
                                            @if ($errors->has('data.from_receive_datetime'))
                                                <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.from_receive_datetime')}}</p>
                                            @endif
                                        </div>
                                        <div class="col-sm-1 d-none d-sm-block px-0 fs-20 text-center">&#x223C;</div>
                                        <div class="col-sm-5 px-0">
                                            <input name="data[to_receive_datetime]"
                                                   class="form-control datetimepicker w-100"
                                                   type="text" id="to_receive_datetime"
                                                   value="{{ isset($conditions['to_receive_datetime']) ? $conditions['to_receive_datetime'] : '' }}">
                                            @if ($errors->has('data.to_receive_datetime'))
                                                <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.to_receive_datetime')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0">{{ trans('demandlist.company_id') }}</label>
                                </div>
                                <div class="col-sm-6 col-md-5 px-0">
                                    {{Form::input('text', 'data[corp_id]', isset($conditions['corp_id']) ? $conditions['corp_id'] : null, ['id' => 'corp_id', 'class' => 'form-control', 'data-rule-number' => 'true'])}}
                                </div>
                            </div>
                            <div class="row mx-0">
                                <div class="offset-sm-4 col-sm-7 px-0 text--info fs-11">{{ trans('demandlist.ignore_company_name') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0 mb-2">
                        <div class="col-lg-6 px-0">
                            <div class="row mx-0">
                                <div class="col-sm-4 px-0 d-flex align-items-center">
                                    <label class="mb-0"
                                           for="calendar_flg">{{ trans('demandlist.calendar_policy') }}</label>
                                </div>
                                <div class="col-sm-7 custom-checkbox custom-control d-flex align-items-center">
                                    {{Form::input('checkbox', 'data[calendar_flg]', null, ['id' => 'calendar_flg', 'class' => 'custom-control-input intlcal_get_error_message ignore', (isset($conditions['calendar_flg']) && $conditions['calendar_flg']=='on') ? 'checked' : ''])}}
                                    <label class="custom-control-label custome-label" for="calendar_flg"><span
                                                class="d-none">a</span></label>
                                    <input class="d-none d-lg-inline-block opacity-0 form-control col-1" tabindex="-1">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-0"></div>
                    </div>
                    <div class="row mx-0 mb-2 d-flex flex-column flex-sm-row">
                        <input type="button"
                               class="btn btn--gradient-orange remove-effect-btn col-lg-2 col-sm-3 mb-1 mb-sm-0"
                               onclick="location.href='{{ route('demand.get.create') }}';"
                               value="{{ trans('demandlist.sign_up') }}">
                        <button name="submit" id="search"
                                class="btn btn--gradient-orange  remove-effect-btn col-lg-2 col-sm-3 mx-sm-2 mb-1 mb-sm-0"
                                type="submit"
                                value="search">{{ trans('demandlist.search') }}</button>

                        @if (!$defaultDisplay && ((isset($conditions['b_check']) && $conditions['b_check'] != 1 && $conditions['b_check'] != 'on') || !isset($conditions['b_check'])))
                            @if($auth=='system' || $auth=='admin' || $auth=='accounting_admin')
                                <button name="submit"
                                        class="btn btn--gradient-orange remove-effect-btn col-lg-2 col-sm-3"
                                        type="submit"
                                        value="csv">{{ trans('demandlist.CSV_output') }}</button>
                            @endif
                        @endif
                    </div>
                </div>
            </fieldset>
            @if (!$defaultDisplay)
                @if(isset($demandInfos) && count($demandInfos) > 0)
                    <p class="mt-2 mb-0">{{ trans('antisocial_follow.total_number').$demandInfos->total().trans('antisocial_follow.matter') }}</p>
                @else
                    <p class="mt-2 mb-0">{{ trans('demandlist.not_data') }}</p>
                @endif
                @if(isset($demandInfos) && count($demandInfos) > 0)
                    <div class="custom-scroll-x demand_info_table" data-url="{{ route('demandlist.search') }}">
                        <table class="table custom-border add-pseudo-scroll-bar mb-2" id='table-demandlist'
                               data-url='{{ Route::current()->parameter('id') }}'>
                            <thead>
                            <tr class="text-center bg-yellow-light">
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.opportunity_id') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-idUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.id"
                                           data-order-by="asc"></i>
                                        <i class="triangle-down down sort-idDown" aria-hidden="true"
                                           data-sort-name="demand_infos.id"
                                           data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.urgent') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-immediatelyUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.immediately" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-immediatelyDown" aria-hidden="true"
                                           data-sort-name="demand_infos.immediately" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.demand_status') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-demandstatusUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.demand_status" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-demandstatusDown" aria-hidden="true"
                                           data-sort-name="demand_infos.demand_status" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.customer_name') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-customernameUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.customer_name" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-customernameDown" aria-hidden="true"
                                           data-sort-name="demand_infos.customer_name" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.corporate_name') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-customercorpnameUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.customer_corp_name" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-customercorpnameDown" aria-hidden="true"
                                           data-sort-name="demand_infos.customer_corp_name" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.site_name') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-sitenameUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.site_id"
                                           data-order-by="asc"></i>
                                        <i class="triangle-down down sort-sitenameDowm" aria-hidden="true"
                                           data-sort-name="demand_infos.site_id"
                                           data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.category') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-categorynameUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.category_id" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-categorynameDown" aria-hidden="true"
                                           data-sort-name="demand_infos.category_id" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.JBR_reception_like_no') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-jbrordernoUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.jbr_order_no" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-jbrordernoDown" aria-hidden="true"
                                           data-sort-name="demand_infos.jbr_order_no" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.power_receiving') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-receivedatetimeUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.receive_datetime" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-receivedatetimeDown" aria-hidden="true"
                                           data-sort-name="demand_infos.receive_datetime" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.contact_deadline_date_and_time') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-contactdesiredtimeUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.contact_desired_time" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-contactdesiredtimeDown" aria-hidden="true"
                                           data-sort-name="demand_infos.contact_desired_time" data-order-by="desc"></i>
                                    </div>
                                </th>
                                <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.selection_method') }}
                                    <div class="sortInner">
                                        <i class="triangle-up up sort-selectionsystemUp mx-1" aria-hidden="true"
                                           data-sort-name="demand_infos.selection_system" data-order-by="asc"></i>
                                        <i class="triangle-down down sort-selectionsystemDown" aria-hidden="true"
                                           data-sort-name="demand_infos.selection_system" data-order-by="desc"></i>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            @foreach($demandInfos as $demandInfo)
                                <tr @if(isset($demandInfo->DemandInfo__CommissionRank) && $demandInfo->DemandInfo__CommissionRank == 1) class="bg-condition" @endif>
                                    <td class="p-1 align-middle fix-w-100 text-wrap text-center">
                                        <a class="highlight-link" target="_blank"
                                           href="{{ route('demand.detail', ['id' => $demandInfo->id]) }}">
                                            {{ $demandInfo->id }}
                                        </a>
                                    </td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">
                                        @if (!empty($demandInfo->immediately))
                                            {{ trans('demandlist'.'.'.getDivTextJP('checkbox_div', $demandInfo->immediately)) }}
                                        @endif
                                    </td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ getDropText(\App\Repositories\Eloquent\MItemRepository::PROPOSAL_STATUS, $demandInfo->demand_status) }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->customer_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->customer_corp_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->site_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->category_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->jbr_order_no }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap text-center">{{ dateTimeFormat($demandInfo->receive_datetime) }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap text-center">{!! getContactDesiredTime($demandInfo) !!}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">
                                        @php
                                            if ($demandInfo->selection_system == 2) {
                                                echo trans('demandlist.bidding_ceremony_manual');
                                            }elseif ($demandInfo->selection_system == 3){
                                                echo trans('demandlist.bidding_ceremony_automatic');
                                            }elseif ($demandInfo->selection_system == 4) {
                                                echo trans('demandlist.automatic');
                                            }else {
                                                echo trans('demandlist.manual');
                                            }
                                        @endphp
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        @if($demandInfos->lastPage() > 1)
                            {{ $demandInfos->links('pagination.nextprevajax') }}
                        @endif
                    </div>
                    <div class="pseudo-scroll-bar" data-display="false">
                        <div class="scroll-bar"></div>
                    </div>
                @endif
            @endif
        </form>
    </div>
@endsection
@section('script')
    <script src="{{ mix('js/lib/jquery.multiselect.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.multiselect.filter.js') }}"></script>
    <script src="{{ mix('js/lib/jquery-ui-timepicker-addon.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.validate.min.js') }}"></script>
    <script src="{{ mix('js/lib/localization/jquery.validate.messages_ja.js') }}"></script>
    <script src="{{ mix('js/lib/additional-methods.min.js') }}"></script>
    <script src="{{ mix('js/utilities/form.validate.js') }}"></script>
    <script src="{{ mix('js/utilities/st.common.js') }}"></script>
    <script src="{{ mix('js/lib/custom.js') }}"></script>
    <script src="{{ mix('js/pages/demandinfo.js') }}"></script>
    <script>
        FormUtil.validate('#demand_info');
    </script>
@endsection
